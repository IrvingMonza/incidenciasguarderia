-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2019 a las 21:16:48
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `incidenciasguaderia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_incidencias`
--

CREATE TABLE `catalogo_incidencias` (
  `Id_catalogo_incidencias` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `FK_tipos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catalogo_incidencias`
--

INSERT INTO `catalogo_incidencias` (`Id_catalogo_incidencias`, `Nombre`, `FK_tipos`) VALUES
(1, 'GOLPEO', 1),
(2, 'EMPUJAR', 1),
(3, 'PRIMEROS PASOS', 2),
(4, 'PRIMER PALABRA', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_tipos`
--

CREATE TABLE `catalogo_tipos` (
  `Id_catalogo_tipos` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catalogo_tipos`
--

INSERT INTO `catalogo_tipos` (`Id_catalogo_tipos`, `Nombre`) VALUES
(1, 'CONDUCTA'),
(2, 'LOGRO'),
(3, 'INCIDENCIA'),
(4, 'CITA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `Id_citas` int(11) NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `FechaBase` datetime DEFAULT NULL,
  `FechaAtencion` datetime DEFAULT NULL,
  `EstadoCita` varchar(45) NOT NULL,
  `FK_incidencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermeria`
--

CREATE TABLE `enfermeria` (
  `Id_Enfermeria` int(11) NOT NULL,
  `FechaIngreso` datetime DEFAULT NULL,
  `Sintomas` varchar(45) DEFAULT NULL,
  `Tratamiento` varchar(45) DEFAULT NULL,
  `FK_incidencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias`
--

CREATE TABLE `incidencias` (
  `Id_Incidencia` int(11) NOT NULL,
  `FechaBase` datetime NOT NULL,
  `Observacion` varchar(250) NOT NULL,
  `EstadoIncidencia` varchar(25) NOT NULL,
  `FK_ninio` int(11) NOT NULL,
  `FK_docente` int(15) NOT NULL,
  `FK_tecnico` int(15) NOT NULL,
  `FechaAtencion` datetime DEFAULT NULL,
  `FK_incidencias` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `incidencias`
--

INSERT INTO `incidencias` (`Id_Incidencia`, `FechaBase`, `Observacion`, `EstadoIncidencia`, `FK_ninio`, `FK_docente`, `FK_tecnico`, `FechaAtencion`, `FK_incidencias`) VALUES
(1, '2019-03-27 13:25:32', 'caida jugando', 'pendiente', 1, 1, 2, '2019-03-27 12:29:30', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ninios`
--

CREATE TABLE `ninios` (
  `Id_ninio` int(11) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `ApellidoPaterno` varchar(15) NOT NULL,
  `ApellidoMaterno` varchar(15) NOT NULL,
  `Edad` int(5) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Genero` varchar(10) NOT NULL,
  `FK_salon` int(10) NOT NULL,
  `FK_Tutores` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ninios`
--

INSERT INTO `ninios` (`Id_ninio`, `Nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `Edad`, `FechaNacimiento`, `Genero`, `FK_salon`, `FK_Tutores`) VALUES
(1, 'ANTONIO', 'MARTINEZ', 'DORANTE', 3, '2015-03-19', 'MASCULINO', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salones`
--

CREATE TABLE `salones` (
  `Id_salon` int(10) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `CapacidadMaxima` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `salones`
--

INSERT INTO `salones` (`Id_salon`, `Nombre`, `CapacidadMaxima`) VALUES
(1, 'LACTANTES A', 24),
(2, 'LACTANTES B', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores`
--

CREATE TABLE `trabajadores` (
  `Id_trabajadores` int(15) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `ApellidoPaterno` varchar(15) NOT NULL,
  `ApellidoMaterno` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Genero` varchar(10) NOT NULL,
  `TipoTrabajador` varchar(25) NOT NULL,
  `Contrasena` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajadores`
--

INSERT INTO `trabajadores` (`Id_trabajadores`, `Nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `email`, `Genero`, `TipoTrabajador`, `Contrasena`) VALUES
(1, 'EVA RUTH', 'PASTOR', 'MEZA', 'docente@gmail.com', 'FEMENINO', 'DOCENTE', '12345'),
(2, 'EVA YULIETE', 'STEVES', 'SANCHEZ', 'tecnico@gmail.com', 'FEMENINO', 'TECNICO', '12345'),
(3, 'GIOVANELLI', 'DORANTE', 'SUASTE', 'administrador@gmail.com', 'MASCULINO', 'ADMINISTRADOR', '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutores`
--

CREATE TABLE `tutores` (
  `Id_Tutores` int(15) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `ApellidoPaterno` varchar(45) DEFAULT NULL,
  `ApellidoMaterno` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `Contrasena` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tutores`
--

INSERT INTO `tutores` (`Id_Tutores`, `Nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `email`, `Contrasena`) VALUES
(1, 'LUIS ENRIQUE', 'PEREZ', 'GALINDO', 'luis@gmail.com', 'perez'),
(2, 'ALEJANDRO', 'MENDOZA', 'ORTIZ', 'alejandro@gmail.com', 'mendoza');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catalogo_incidencias`
--
ALTER TABLE `catalogo_incidencias`
  ADD PRIMARY KEY (`Id_catalogo_incidencias`),
  ADD KEY `fk_catalogo_incidencias_catalogo_tipos1_idx` (`FK_tipos`);

--
-- Indices de la tabla `catalogo_tipos`
--
ALTER TABLE `catalogo_tipos`
  ADD PRIMARY KEY (`Id_catalogo_tipos`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`Id_citas`),
  ADD KEY `fk_citas_incidencias1_idx` (`FK_incidencia`);

--
-- Indices de la tabla `enfermeria`
--
ALTER TABLE `enfermeria`
  ADD PRIMARY KEY (`Id_Enfermeria`),
  ADD KEY `fk_enfermeria_incidencias1_idx` (`FK_incidencia`);

--
-- Indices de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  ADD PRIMARY KEY (`Id_Incidencia`),
  ADD KEY `fk_incidencias_ninios_idx` (`FK_ninio`),
  ADD KEY `fk_incidencias_trabajadores1_idx` (`FK_docente`),
  ADD KEY `fk_incidencias_trabajadores2_idx` (`FK_tecnico`),
  ADD KEY `fk_incidencias_catalogo_incidencias1_idx` (`FK_incidencias`);

--
-- Indices de la tabla `ninios`
--
ALTER TABLE `ninios`
  ADD PRIMARY KEY (`Id_ninio`),
  ADD KEY `fk_ninios_salones1_idx` (`FK_salon`),
  ADD KEY `fk_ninios_tutores1_idx` (`FK_Tutores`);

--
-- Indices de la tabla `salones`
--
ALTER TABLE `salones`
  ADD PRIMARY KEY (`Id_salon`);

--
-- Indices de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD PRIMARY KEY (`Id_trabajadores`);

--
-- Indices de la tabla `tutores`
--
ALTER TABLE `tutores`
  ADD PRIMARY KEY (`Id_Tutores`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catalogo_incidencias`
--
ALTER TABLE `catalogo_incidencias`
  MODIFY `Id_catalogo_incidencias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `catalogo_tipos`
--
ALTER TABLE `catalogo_tipos`
  MODIFY `Id_catalogo_tipos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `Id_citas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `enfermeria`
--
ALTER TABLE `enfermeria`
  MODIFY `Id_Enfermeria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  MODIFY `Id_Incidencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ninios`
--
ALTER TABLE `ninios`
  MODIFY `Id_ninio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `salones`
--
ALTER TABLE `salones`
  MODIFY `Id_salon` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  MODIFY `Id_trabajadores` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tutores`
--
ALTER TABLE `tutores`
  MODIFY `Id_Tutores` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `catalogo_incidencias`
--
ALTER TABLE `catalogo_incidencias`
  ADD CONSTRAINT `fk_catalogo_incidencias_catalogo_tipos1` FOREIGN KEY (`FK_tipos`) REFERENCES `catalogo_tipos` (`Id_catalogo_tipos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `fk_citas_incidencias1` FOREIGN KEY (`FK_incidencia`) REFERENCES `incidencias` (`Id_Incidencia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `enfermeria`
--
ALTER TABLE `enfermeria`
  ADD CONSTRAINT `fk_enfermeria_incidencias1` FOREIGN KEY (`FK_incidencia`) REFERENCES `incidencias` (`Id_Incidencia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `incidencias`
--
ALTER TABLE `incidencias`
  ADD CONSTRAINT `fk_incidencias_catalogo_incidencias1` FOREIGN KEY (`FK_incidencias`) REFERENCES `catalogo_incidencias` (`Id_catalogo_incidencias`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_incidencias_ninios` FOREIGN KEY (`FK_ninio`) REFERENCES `ninios` (`Id_ninio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_incidencias_trabajadores1` FOREIGN KEY (`FK_docente`) REFERENCES `trabajadores` (`Id_trabajadores`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_incidencias_trabajadores2` FOREIGN KEY (`FK_tecnico`) REFERENCES `trabajadores` (`Id_trabajadores`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ninios`
--
ALTER TABLE `ninios`
  ADD CONSTRAINT `fk_ninios_salones1` FOREIGN KEY (`FK_salon`) REFERENCES `salones` (`Id_salon`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ninios_tutores1` FOREIGN KEY (`FK_Tutores`) REFERENCES `tutores` (`Id_Tutores`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
