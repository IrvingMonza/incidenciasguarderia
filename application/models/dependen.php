<?php
class dependen extends CI_Model
{
    public function getNombress($fieldSalaGrupo)
    {       
        $this->db->where('salon', $fieldSalaGrupo);
        $this->db->order_by('Nombre', 'asc');
        $nombres = $this->db->get('ninios'); 

        if ($nombres->num_rows() > 0) {
            return $nombres->result();
        }
    }

    function getNombre($FK_salones) 
    {       
        $this->db->where('FK_salon', $FK_salones);
        $query = $this->db->get('ninios');       
        $output = '<option value="">SELECCIONA UN NOMBRE</option>';

        foreach ($query->result() as $row) {
            $output .= '<option value="'.$row->Id_ninio.'">'.$row->Nombre_ninio." ".$row->ApellidoPaterno_ninio." ".$row->ApellidoMaterno_ninio.' </option>';
        }
        return $output;
    }


    function getNombreincidencia($FK_tiposs) 
    {
        $this->db->where('FK_tipos', $FK_tiposs);
        $query = $this->db->get('catalogo_incidencias');       
        $output = '<option value="">SELECCIONE UNA INCIDENCIA</option>';
        
        foreach ($query->result() as $row) {
            $output .= '<option value="'.$row->Id_catalogo_incidencias.'">'.$row->Nombre_incidencia.' </option>';
        }
        return $output;
    }    



}