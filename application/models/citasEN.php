<?php
class citasEN extends CI_Model
{ 
  public function __construct()
  {
     parent::__construct();
  } 

  public function datos_citas()
  {
    $this->db->select("cit.FechaBaseCita, cat.Nombre_incidencia, ic.Observacion, cit.FechaAtencionCita, cit.Descripcion, cit.EstadoCita,cit.Id_citas, catt.Nombre_tipo_inc");
    $this->db->select("CONCAT(nin.Nombre_ninio,' ',nin.ApellidoPaterno_ninio,' ',nin.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where('ic.Cita', 'ENFERMERA/NUTRIOLOGO');
    $this->db->from('citas cit');
    $this->db->join('incidencias ic', 'ic.Id_incidencia = cit.FK_incidencia');
    $this->db->join('ninios nin', 'nin.Id_ninio = ic.FK_ninio');
    $this->db->join('catalogo_incidencias cat', 'cat.Id_catalogo_incidencias = ic.FK_incidencias');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = ic.FK_tecnico','left');
    $this->db->join('catalogo_tipos catt', 'catt.Id_catalogo_tipos = cat.FK_tipos');
    return  $this->db->get()->result();
  }

  public function salon()
  {
   $this->db->select('Id_salon, Nombre');
   $this->db->from('salones');
   return  $this->db->get()->result();
  }

  public function tipo_salon()
  {
   $this->db->select('Nombre');
   $this->db->from('catalogo_tipos');
   return  $this->db->get()->result();
  }

  public function modificar($idcita)
  {
    $this->db->select('*');
    $this->db->where('Id_citas',$idcita);
    return $this->db->get('citas')->result();
  }

  public function CRUD_Citas_Atendidas()
  {
    $this->db->select("cit.FechaBaseCita, cat.Nombre_incidencia, ic.Observacion, cit.FechaAtencionCita, cit.Descripcion, cit.EstadoCita,cit.Id_citas, catt.Nombre_tipo_inc");
    $this->db->select("CONCAT(nin.Nombre_ninio,' ',nin.ApellidoPaterno_ninio,' ',nin.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where('EstadoCita', 'ATENDIDO');
    $this->db->where('ic.Cita', 'ENFERMERA/NUTRIOLOGO');
    $this->db->from('citas cit');
    $this->db->join('incidencias ic', 'ic.Id_incidencia = cit.FK_incidencia');
    $this->db->join('ninios nin', 'nin.Id_ninio = ic.FK_ninio');
    $this->db->join('catalogo_incidencias cat', 'cat.Id_catalogo_incidencias = ic.FK_incidencias');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = ic.FK_tecnico','left');
    $this->db->join('catalogo_tipos catt', 'catt.Id_catalogo_tipos = cat.FK_tipos');
    return  $this->db->get()->result();
  }
  public function CRUD_Citas_Pendientes()
  {
    $this->db->select("cit.FechaBaseCita, cat.Nombre_incidencia, ic.Observacion, cit.FechaAtencionCita, cit.Descripcion, cit.EstadoCita,cit.Id_citas, catt.Nombre_tipo_inc");
    $this->db->select("CONCAT(nin.Nombre_ninio,' ',nin.ApellidoPaterno_ninio,' ',nin.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where('EstadoCita', 'PENDIENTE');
    $this->db->where('ic.Cita', 'ENFERMERA/NUTRIOLOGO');
    $this->db->from('citas cit');
    $this->db->join('incidencias ic', 'ic.Id_incidencia = cit.FK_incidencia');
    $this->db->join('ninios nin', 'nin.Id_ninio = ic.FK_ninio');
    $this->db->join('catalogo_incidencias cat', 'cat.Id_catalogo_incidencias = ic.FK_incidencias');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = ic.FK_tecnico','left');
    $this->db->join('catalogo_tipos catt', 'catt.Id_catalogo_tipos = cat.FK_tipos');
    return  $this->db->get()->result();
  }

}