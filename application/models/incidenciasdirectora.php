<?php
class incidenciasdirectora extends CI_Model
{ 

  public function __construct()
  {
    parent::__construct();
  } 

  public function dir_incidencias()
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,cta.FechaBaseCita, i.EntregaIncidencia, i.FirmaTutor, cta.EstadoCita, i.EstadoRevisadoIncidencia");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
     // $this->db->where("EstadoIncidencia","ATENDIDA");
    //$this->db->where("");
    $this->db->from('incidencias i','left');
    $this->db->where('i.FechaBase <=', date('Y-m-d', strtotime('1 day')));
    $this->db->where('i.FechaBase >=', date('Y-m-d', strtotime('-1 month')));
    // $this->db->from('citas cta');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico','left');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    $this->db->join('citas cta', 'cta.FK_Incidencia = i.Id_incidencia','left');  
    return $this->db->get()->result();
  }

  public function dir_incidencias_pendientes()
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,cta.FechaBaseCita, i.EntregaIncidencia, i.FirmaTutor, cta.EstadoCita, i.EstadoRevisadoIncidencia");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where("EstadoRevisadoIncidencia","PENDIENTE");
    $this->db->where('i.FechaBase <=', date('Y-m-d', strtotime('1 day')));
    $this->db->where('i.FechaBase >=', date('Y-m-d', strtotime('-1 month')));
    $this->db->from('incidencias i');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico','left');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    $this->db->join('citas cta', 'cta.FK_Incidencia = i.Id_incidencia','left');
    return $this->db->get()->result();
  }

  public function dir_incidencias_atendidas()
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,cta.FechaBaseCita, i.EntregaIncidencia, i.FirmaTutor, cta.EstadoCita, i.EstadoRevisadoIncidencia");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where("EstadoIncidencia","ATENDIDA");
    $this->db->from('incidencias i');
    $this->db->where('i.FechaBase >=', date('Y-m-d 00:00:00')); // para que muestre las de dia de hoy
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    $this->db->join('citas cta', 'cta.FK_Incidencia = i.Id_incidencia','left');
    return $this->db->get()->result();
  }
      
  public function datos_citas()
  {
    $this->db->select("cit.FechaBaseCita, cat.Nombre_incidencia, ic.Observacion, cit.FechaAtencionCita, cit.Descripcion, cit.EstadoCita,cit.Id_citas, catt.Nombre_tipo_inc");
    $this->db->select("CONCAT(nin.Nombre_ninio,' ',nin.ApellidoPaterno_ninio,' ',nin.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    // $this->db->where('EstadoCita', 'ATENDIDO');
    $this->db->from('citas cit');
    $this->db->join('incidencias ic', 'ic.Id_incidencia = cit.FK_incidencia');
    $this->db->join('ninios nin', 'nin.Id_ninio = ic.FK_ninio');
    $this->db->join('catalogo_incidencias cat', 'cat.Id_catalogo_incidencias = ic.FK_incidencias');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = ic.FK_tecnico','left');
    $this->db->join('catalogo_tipos catt', 'catt.Id_catalogo_tipos = cat.FK_tipos');
    return  $this->db->get()->result();
  }

  public function datos_citas_atendidas()
  {
    $this->db->select("cit.FechaBaseCita, cat.Nombre_incidencia, ic.Observacion, cit.FechaAtencionCita, cit.Descripcion, cit.EstadoCita,cit.Id_citas, catt.Nombre_tipo_inc");
    $this->db->select("CONCAT(nin.Nombre_ninio,' ',nin.ApellidoPaterno_ninio,' ',nin.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where('EstadoCita', 'ATENDIDO');
    $this->db->from('citas cit');
    $this->db->join('incidencias ic', 'ic.Id_incidencia = cit.FK_incidencia');
    $this->db->join('ninios nin', 'nin.Id_ninio = ic.FK_ninio');
    $this->db->join('catalogo_incidencias cat', 'cat.Id_catalogo_incidencias = ic.FK_incidencias');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = ic.FK_tecnico','left');
    $this->db->join('catalogo_tipos catt', 'catt.Id_catalogo_tipos = cat.FK_tipos');
    return  $this->db->get()->result();
  }

  public function datos_citas_pendientes()
  {
    $this->db->select("cit.FechaBaseCita, cat.Nombre_incidencia, ic.Observacion, cit.FechaAtencionCita, cit.Descripcion, cit.EstadoCita,cit.Id_citas, catt.Nombre_tipo_inc");
    $this->db->select("CONCAT(nin.Nombre_ninio,' ',nin.ApellidoPaterno_ninio,' ',nin.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where('EstadoCita', 'PENDIENTE');
    $this->db->from('citas cit');
    $this->db->join('incidencias ic', 'ic.Id_incidencia = cit.FK_incidencia');
    $this->db->join('ninios nin', 'nin.Id_ninio = ic.FK_ninio');
    $this->db->join('catalogo_incidencias cat', 'cat.Id_catalogo_incidencias = ic.FK_incidencias');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = ic.FK_tecnico','left');
    $this->db->join('catalogo_tipos catt', 'catt.Id_catalogo_tipos = cat.FK_tipos');
    return  $this->db->get()->result();
  }

  public function datos_incidencias()
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,c.FechaBaseCita");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where("EstadoIncidencia","ATENDIDA");
    $this->db->from('incidencias i');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico','left');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    $this->db->join('citas c', 'c.FK_incidencia = i.Id_incidencia','left');
    return $this->db->get()->result();
  }
    public function datos_incidencias_pdf($grupo)
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where("EstadoRevisadoIncidencia","ATENDIDA");
    $this->db->where('n.FK_salon',$grupo);
         $this->db->where('i.FechaBase >=', date('Y-m-d 00:00:00'));
    $this->db->from('incidencias i');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico','left');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    
    return $this->db->get()->result();
  }
    

  public function salon()
  {
    $this->db->select('Id_salon, Nombre_salon');
    $this->db->from('salones');
    return  $this->db->get()->result();
  }
    public function datos_salones()
   {
        $this->db->select('Id_salon, Nombre_salon');
        $this->db->from('salones');
	    return  $this->db->get()->result();
   }

  public function tipo_salon()
  {
    $this->db->select('*');
    $this->db->from('catalogo_tipos');
    return  $this->db->get()->result();
  }
    
  public function enviarincidencia($id)
  {
    $this->db->select('i.Id_incidencia,i.Observacion,n.FK_tutores,ci.Nombre_incidencia,ct.Nombre_tipo_inc, t.email');
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(t.Nombre_tutor,' ',t.ApellidoPaterno_tutor,' ',t.ApellidoMaterno_tutor) AS Nombre_tutor ");
    $this->db->where('Id_incidencia', $id);
    $this->db->from('incidencias i');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('tutores t', 't.Id_tutores = n.FK_tutores');
    $this->db->join('Catalogo_incidencias ci','ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('Catalogo_tipos ct','ct.Id_catalogo_tipos = ci.FK_tipos');   
    return $this->db->get()->result();
  }

  public function revisarincidencia($id)
  {
    $this->db->select('i.Id_incidencia,i.Observacion');
    $this->db->where('Id_incidencia', $id);
    $this->db->from('incidencias i');   
    return $this->db->get()->result();
  }

  public function modificar($id)
  {
    
      $this->db->select('i.Id_incidencia,i.Observacion,n.FK_salon,n.Id_ninio,i.FK_ninio,ci.FK_tipos,i.Cita, ci.Id_catalogo_incidencias');
      $this->db->where('Id_incidencia', $id);
     $this->db->from('incidencias i');
   $this->db->join('ninios n','n.Id_ninio = i.FK_ninio');
   $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
   $hth1= $this->db->get()->result();
      
       $this->db->select('Id_ninio');
      $this->db->select("CONCAT(Nombre_ninio,' ',ApellidoPaterno_ninio,' ',ApellidoMaterno_ninio) AS Nombre_ninio");
      $this->db->from('ninios');
      $this->db->where('FK_salon', $hth1[0]->FK_salon);
      $hth2= $this->db->get()->result();
      
      $this->db->select('Id_catalogo_incidencias, Nombre_incidencia');
      //$this->db->select('');
      $this->db->from('catalogo_incidencias');
      $this->db->where('FK_tipos', $hth1[0]->FK_tipos);
      $hth3=$this->db->get()->result();
      return array ($hth1, $hth2, $hth3);
  }

  public function datos_control($FK_salon, $fecha1, $fecha2)
  {
    $this->db->select("n.Id_ninio, CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio) AS NOMBRE_NIÑO");
    $this->db->select('(SELECT count(Id_incidencia) FROM incidencias i INNER JOIN catalogo_incidencias ci on i.FK_Incidencias = ci.Id_catalogo_incidencias WHERE i.FK_ninio = n.Id_ninio AND ci.FK_tipos = 2 AND i.FechaBase BETWEEN "'.$fecha1.' 00:00:00" AND "'.$fecha2.' 00:00:00") as logro');
    $this->db->select('(SELECT count(Id_incidencia) FROM incidencias i INNER JOIN catalogo_incidencias ci on i.FK_Incidencias = ci.Id_catalogo_incidencias WHERE i.FK_ninio = n.Id_ninio AND ci.FK_tipos = 1 AND i.FechaBase BETWEEN "'.$fecha1.' 00:00:00" AND "'.$fecha2.' 00:00:00") as conducta');
    $this->db->select('(SELECT count(Id_incidencia) FROM incidencias i INNER JOIN catalogo_incidencias ci on i.FK_Incidencias = ci.Id_catalogo_incidencias WHERE i.FK_ninio = n.Id_ninio AND ci.FK_tipos = 3 AND i.FechaBase BETWEEN "'.$fecha1.' 00:00:00" AND "'.$fecha2.' 00:00:00") as incidencia');
    $this->db->select('(SELECT count(Id_incidencia) FROM incidencias i INNER JOIN catalogo_incidencias ci on i.FK_Incidencias = ci.Id_catalogo_incidencias WHERE i.FK_ninio = n.Id_ninio AND ci.FK_tipos = 4 AND i.FechaBase BETWEEN "'.$fecha1.' 00:00:00" AND "'.$fecha2.' 00:00:00") as cita');
    $this->db->from('ninios n');
    $this->db->where('FK_salon',$FK_salon);
    return $this->db->get()->result();
  }
}