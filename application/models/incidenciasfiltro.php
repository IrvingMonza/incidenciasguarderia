<?php
class incidenciasfiltro extends CI_Model
{ 

  public function __construct()
  {
   parent::__construct();
  } 

  public function datos_incidencias()
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,c.FechaBaseCita, i.FirmaTutor,c.EstadoCita");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where("EstadoRevisadoIncidencia","ATENDIDA");
    $this->db->where('i.FechaBase <=', date('Y-m-d',strtotime('1 day')));
    $this->db->where('i.FechaBase >=', date('Y-m-d', strtotime('-1 month')));
    $this->db->from('incidencias i');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico','left');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    $this->db->join('citas c', 'c.FK_incidencia = i.Id_incidencia','left');
    return $this->db->get()->result();
  }
    
    public function datos_salones()
   {
        $this->db->select('Id_salon, Nombre_salon');
        $this->db->from('salones');
	    return  $this->db->get()->result();
   }

  public function salon()
  {
    $this->db->select('Id_salon, Nombre_salon');
    $this->db->from('salones');
    return  $this->db->get()->result();
  }

  public function tipo_salon()
  {
    $this->db->select('*');
    $this->db->from('catalogo_tipos');
    return  $this->db->get()->result();
  }
    
    public function datos_incidencias_pdf($grupo)
   {
        $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,i.EntregaIncidencia");
        $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
        $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
        $this->db->select("CONCAT(tt.Nombre_trabajador,' ',tt.ApellidoPaterno_trabajador,' ',tt.ApellidoMaterno_trabajador) AS Nombre_tecnico");
        $this->db->select("CONCAT(t.Nombre_tutor,' ',t.ApellidoPaterno_tutor,' ',t.ApellidoMaterno_tutor) AS Nombre_tutor");
        $this->db->where('i.EstadoRevisadoIncidencia','ATENDIDA');
        $this->db->where('n.FK_salon',$grupo);
        $this->db->where('i.FechaBase >=', date('Y-m-d 00:00:00'));
        $this->db->from('incidencias i');
        $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
        $this->db->join('tutores t', 't.Id_Tutores = n.FK_Tutores');
        $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
        $this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico', 'left');
        $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
        $this->db->join('salones s', 's.Id_salon = n.FK_salon');
        $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
        return $this->db->get()->result();
   }
}