<?php
class incidencias extends CI_Model 
{ 


  public function __construct() 
  {
    parent::__construct();
  } 


  public function datos_incidencias()
  {
    $this->db->select("i.FechaBase, i.Observacion, i.EstadoIncidencia, i.FechaAtencion, ci.Nombre_incidencia,s.Nombre_salon, i.Id_incidencia,ct.Nombre_tipo_inc,i.Cita,");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(td.Nombre_trabajador,' ',td.ApellidoPaterno_trabajador,' ',td.ApellidoMaterno_trabajador) AS Nombre_docente ");
    $this->db->where("i.EstadoIncidencia !=","ATENDIDA");
    $this->db->from('incidencias i');
    $this->db->join('ninios n', 'n.Id_ninio = i.FK_ninio');
    $this->db->join('trabajadores td', 'td.Id_trabajadores = i.FK_docente');
    //$this->db->join('trabajadores tt', 'tt.Id_trabajadores = i.FK_tecnico');
    $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
    $this->db->join('salones s', 's.Id_salon = n.FK_salon');
    $this->db->join('Catalogo_tipos ct', 'ct.Id_catalogo_tipos = ci.FK_tipos');
    return $this->db->get()->result();
  }

  public function salon()
  {
    $this->db->select('Id_salon, Nombre_salon');
    $this->db->from('salones');
	  return  $this->db->get()->result();
  }

  public function tipo_salon()
  {
   $this->db->select('*');
    $this->db->from('catalogo_tipos');
	 return  $this->db->get()->result();
  }

  public function modificar($id)
  {
   $this->db->select('i.Id_incidencia,i.Observacion,n.FK_salon,n.Id_ninio,i.FK_ninio,ci.FK_tipos,i.Cita, ci.Id_catalogo_incidencias');
   $this->db->where('Id_incidencia', $id);
   $this->db->from('incidencias i');
   $this->db->join('ninios n','n.Id_ninio = i.FK_ninio');
   $this->db->join('Catalogo_incidencias ci', 'ci.Id_catalogo_incidencias = i.FK_incidencias');
   $hth1= $this->db->get()->result();
      
      $this->db->select('Id_ninio');
      $this->db->select("CONCAT(Nombre_ninio,' ',ApellidoPaterno_ninio,' ',ApellidoMaterno_ninio) AS Nombre_ninio");
      $this->db->from('ninios');
      $this->db->where('FK_salon', $hth1[0]->FK_salon);
      $hth2= $this->db->get()->result();
      
      $this->db->select('Id_catalogo_incidencias, Nombre_incidencia');
      //$this->db->select('');
      $this->db->from('catalogo_incidencias');
      $this->db->where('FK_tipos', $hth1[0]->FK_tipos);
      $hth3=$this->db->get()->result();
      return array ($hth1, $hth2, $hth3);
      
  }

  public function datosfirma($id)
  {
    $this->db->select("p.Id_tutores, i.Id_incidencia");
    $this->db->select("CONCAT(n.Nombre_ninio,' ',n.ApellidoPaterno_ninio,' ',n.ApellidoMaterno_ninio) AS Nombre_ninio ");
    $this->db->select("CONCAT(p.Nombre_tutor,' ',p.ApellidoPaterno_tutor,' ',p.ApellidoMaterno_tutor) AS Nombre_tutor ");
    $this->db->where('Id_incidencia', $id);
    $this->db->from('incidencias i');
    $this->db->join('ninios n','n.Id_ninio = i.FK_ninio');
    $this->db->join('tutores p','p.Id_tutores = n.FK_tutores');
    $firma1 = $this->db->get()->result();
    
    $this->db->select("a.Id_autorizados as Idautorizado, a1.Id_autorizados as Idautorizado1, a2.Id_autorizados as Idautorizado2");
    $this->db->select("CONCAT(a.Nombre_autorizado,' ',a.ApellidoPaterno_autorizado,' ',a.ApellidoMaterno_autorizado) AS Nombre_autorizado ");
    $this->db->select("CONCAT(a1.Nombre_autorizado,' ',a1.ApellidoPaterno_autorizado,' ',a1.ApellidoMaterno_autorizado) AS Nombre_autorizado1 ");
    $this->db->select("CONCAT(a2.Nombre_autorizado,' ',a2.ApellidoPaterno_autorizado,' ',a2.ApellidoMaterno_autorizado) AS Nombre_autorizado2 ");
    $this->db->where('Id_incidencia', $id);
    $this->db->from('incidencias i');
    $this->db->join('ninios n','n.Id_ninio = i.FK_ninio');
    $this->db->join('autorizados a','a.Id_autorizados = n.autorizado1');
    $this->db->join('autorizados a1','a1.Id_autorizados = n.autorizado2');
    $this->db->join('autorizados a2','a2.Id_autorizados = n.autorizado3');
      $firma2 = $this->db->get()->result();
      
      return array ($firma1, $firma2);
  }
  
  public function citaAlta($id)
  {
    $this->db->select('i.Id_incidencia,i.Observacion,i.FK_tecnico,c.FechaBaseCita');
    $this->db->select("CONCAT(t.Nombre_trabajador,' ',t.ApellidoPaterno_trabajador,' ',t.ApellidoMaterno_trabajador) AS Nombre_tecnico ");
    $this->db->where('Id_incidencia', $id);
    $this->db->from('incidencias i');
    $this->db->join('trabajadores t','t.Id_trabajadores = i.FK_tecnico');
    $this->db->join('citas c','c.FK_incidencia = i.Id_incidencia','left');   
    return $this->db->get()->result();
  }
}