<?php    
if($this->session->userdata('tipo_trabajador')!="DOCENTE"){//echo "solo el admin tiene acceso aqui xD";
echo "<script>
window.location= '".site_url()."roles/roll'
</script>";
}
?>
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css"> -->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/bootstrap.css" >
<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
<script src="<?php echo base_url();?>assets/js/431bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="<?php echo base_url();?>assets/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
<!--<body style="background:url('<?php echo base_url();?>/Imagenes/bebess.jpg')  center center;">-->

<nav class="navbar navbar-expand-sm" style="background-color: #04774D;">

  <!-- Links  -->
  <ul class="navbar-nav">
   <li class="nav-item">
     <a class="nav-link" href="<?php echo base_url(); ?>Docente/" style="color: #FFFFFF">Inicio</a> 
   </li>
   <li class="nav-item">
     <a class="nav-link" href="<?php echo base_url(); ?>Docente/CRUD_Incidencias" style="color: #FFFFFF">Incidencias</a>
   </li> 
 </ul>
</nav>
<br>
<h3>
  <center>
    SECCIÓN DE INCIDENCIAS  
  </center></h3>
  <?php if ($this->session->flashdata('alerta') == null): ?>

  <?php endif ?>
  <?php if ($this->session->flashdata('alerta') != null): ?>
    <div class="alert alert-success"><h5><?php  echo $this->session->flashdata('alerta'); ?> </h5></div>
  <?php endif ?>
  <div id="tablaaa">
    <table id="table_id" class="table table-striped table-bordered" style="width:100%" style="background-color:red;" > 
      <thead style="background-color:white;">
        <tr>
          <th><button type="button" title="Agregar Incidencia" data-toggle="modal" data-target="#agregar"><img src="<?php echo base_url();?>/Imagenes/add.png" width="26"></button></th>
          <th>FECHA DE INCIDENCIA</th>
          <th>SALÓN</th>
          <th>NOMBRE DEL NIÑO</th>
          <th>TIPO DE INCIDENCIA</th>
          <th>INCIDENCIA</th>
          <th>OBSERVACIÓN</th>
          <th>CITA</th>
          <th>NOMBRE DE QUIEN REGISTRA</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody style="background-color:white;">
       <?php foreach ($datos as $key => $value): ?>
        <tr>
         <td></td>
         <td><span id="fechabas"><?php echo $value->FechaBase; ?></span></td>
         <td><?php echo $value->Nombre_salon; ?></td>
         <td><?php echo $value->Nombre_ninio; ?></td>
         <?php     if($value->Nombre_tipo_inc=="CONDUCTA"){ ?>
          <td style="background-color:#FF4646;" style="color: red"> <?php echo $value->Nombre_tipo_inc;?> </td>
        <?php  } ?>
        <?php if($value->Nombre_tipo_inc=="LOGRO"){ ?>
          <td style="background-color:#2DFF41;"> <?php echo $value->Nombre_tipo_inc;?> </td>
        <?php  } ?>
        <?php if($value->Nombre_tipo_inc=="INCIDENCIA"){ ?>
          <td style="background-color:#FFF639;"> <?php echo $value->Nombre_tipo_inc;?> </td>
        <?php  } ?>
        <?php if($value->Nombre_tipo_inc=="CITA"){ ?>
         <td style="background-color:#58ABFF"> <?php echo $value->Nombre_tipo_inc;?> </td>
       <?php  } ?>
       <td><?php echo $value->Nombre_incidencia; ?></td>
       <td><?php echo $value->Observacion; ?></td>
       <td><?php echo $value->Cita; ?></td>
       <td><?php echo $value->Nombre_docente; ?></td>       
       <td>
        <button type="button" title="Editar Incidencia"  id="modif" onclick="modificar(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/editar.png" width="26"></button>
      </td>
    </tr>
  <?php endforeach ?> 
</tbody>
</table>
</div>

<!-- M O D A L    P A R A     M O D I F I C A R    U N    R E G I S T R O -->  
<?php echo form_open('Docente/modifinc'); ?>
<div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#04774D">
                <font color="white">
                    <h4>EDITAR INCIDENCIA</h4>
                </font>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="form-group row">
                    <label for="pass" class="col-md-4 col-form-label text-md-right">SALÓN :</label>
                    <div class="col-md-6">
                        <select class="form-control" name="salon" id="salonM" class="salonM" required>
                            <option >SELECCIONE UN SALÓN</option>
                            <?php foreach ($salon as $key => $value): ?>
                            <option value="<?php echo $value->Id_salon; ?>">
                                <?php echo $value->Nombre_salon; ?>
                            </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                
                 <div class="form-group row">
                     <label for="pass" class="col-md-4 col-form-label text-md-right">NOMBRE DEL NIÑO :</label>
                     <div class="col-md-6">
                         <select class="form-control" name="nombreninosM" id="nombreninosM" required>
                             <option value="">SELECCIONE UN NOMBRE</option>
                         </select>
                     </div>
                 </div>
                 
                 <div class="form-group row">
                    <label for="pass" class="col-md-4 col-form-label text-md-right">TIPO INCIDENCIA :</label>
                    <div class="col-md-6">
                        <select class="form-control" name="tipoincidenciaM" id="tipoincidenciaM" required>
                            <option value="">SELECCIONE UN TIPO DE INCIDENCIA</option>
                            <?php foreach ($tipos as $key => $value): ?>
                            <option value="<?php echo $value->Id_catalogo_tipos; ?>">
                                <?php echo $value->Nombre_tipo_inc; ?>
                            </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>  
                
                <div class="form-group row">
                    <label for="pass" class="col-md-4 col-form-label text-md-right">INCIDENCIA :</label>
                    <div class="col-md-6">
                        <select class="form-control" name="incidenciaM" id="incidenciaM" required>
                            <option value="">SELECCIONE UNA INCIDENCIA</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="pass" class="col-md-4 col-form-label text-md-right">CITA :</label>
                    <div class="col-md-6">
                        <select class="form-control" name="citaM" id="citaM" required>
                            <option value="">ELIGE UNA OPCIÓN</option>
                            <option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
                            <option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIÓLOGO</option>
                            <option value="SIN CITA">SIN CITA</option>
                        </select>
                    </div>
                </div>       
            
                <div class="form-group row">
                    <input type="text" name="nom" id="nom" hidden>
                    <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
                    <div class="col-md-6">
                        <textarea rows="4" cols="50" name="obsr" id="obsr" class="form-control" aria-describedby="emailHelp" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Modificar</button>
            </div>
        </div>
    </div>
</div>
</form>
<!-- M O D A L    P A R A     E L I M I N A R    U N    R E G I S T R O -->
<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background:#04774D">
        <a style="color:#FFFFFF">ELiMINAR!</a>

        <button type="button"  class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Seguro que desea eliminar el registro?
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- M O D A L    P A R A     A G R E G A R    U N    R E G I S T R O -->
<div id="agregar" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header" style="background-color:#04774D">
      <font color="white" ><h4>AGREGAR INCIDENCIA</h4> </font>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <?php echo form_open('Docente/guardarincidencia'); ?>
    <div class="modal-body">

     <div class="form-group row">
       <label for="pass" class="col-md-4 col-form-label text-md-right" >SALÓN :</label>
       <div class="col-md-6">
        <select class="form-control" name="salon" id="salon" required>
          <option value="">SELECCIONE UN SALÓN</option>
          <?php foreach ($salon as $key => $value): ?>
           <option value="<?php echo $value->Id_salon; ?>">
             <?php echo $value->Nombre_salon; ?>
           </option>
         <?php endforeach ?>
       </select>
     </div>
   </div>

   <div class="form-group row">
    <label for="pass" class="col-md-4 col-form-label text-md-right" >NOMBRE DEL NIÑO :</label>
    <div class="col-md-6">
      <select class="form-control" name="nombrenino" id="nombrenino" required>
       <option value="">SELECCIONA UN NOMBRE</option>
     </select>
   </div>
 </div>

 <div class="form-group row">
  <label for="pass" class="col-md-4 col-form-label text-md-right" >TIPO INCIDENCIA :</label>
  <div class="col-md-6">
    <select class="form-control" name="tipoincidencia" id="tipoincidencia" required>
      <option value="">SELECCIONE UN TIPO DE INCIDENCIA</option>
      <?php foreach ($tipos as $key => $value): ?>
       <option value="<?php echo $value->Id_catalogo_tipos; ?>">
         <?php echo $value->Nombre_tipo_inc; ?>
       </option>
     <?php endforeach ?>
   </select>
 </div>
</div>

<div class="form-group row">
  <label for="pass" class="col-md-4 col-form-label text-md-right" >INCIDENCIA :</label>
  <div class="col-md-6">
   <select name="incidencia" class="form-control" id="incidencia" required>
     <option value="">SELECCIONE UNA INCIDENCIA</option>
   </select>
 </div>
</div>
<div class="modal-body">
  <div class="form-group row">
    <input type="text" name="id" id="id" hidden>
    <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
    <div class="col-md-6">
      <textarea rows="4" cols="50" name="obsr" id="obsr" class="form-control" aria-describedby="emailHelp" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" required></textarea>
    </div>
</div>

<div class="form-group row" id="Cita">
  <label for="pass" class="col-md-4 col-form-label text-md-right" >CITA :</label>
  <div class="col-md-6">
    <select class="form-control" name="cita" id="cita">
      <option value="">ELIGE UNA OPCIÓN</option>
      <option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
      <option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIÓLOGO</option>
      <option value="SIN CITA">SIN CITA</option>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="pass" class="col-md-4 col-form-label text-md-right" >NOMBRE DEL DOCENTE :</label>
  <div class="col-md-6">
    <input id="nombredocente" type="text" class="form-control" placeholder="Ingresa " name="nombredocente" value="<?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?>" disabled >
    <input name="Iddocente" type="hidden" value="<?php echo $this->session->userdata('Id_trabajadores') ?>">
  </div>
</div>
</div>


<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Agregar</button>
  <button type="button" class="btn btn-default"data-dismiss="modal">Cancelar</button>
</div>

</form>
</div>
</div>
</div>


</body>

<script>
    $(document).ready(function() {
        $('#table_id').DataTable({
            responsive: true,
            language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
        $('#nombrenino').select2();
        $('#nombreninosM').select2();
    });

</script>

<script>
  $(document).ready(function(){
    $(document).on('click', '.modificar', function(){
      var id=$(this).val();
      var first=$('#fechabas').text();

      
      $('#modificar').modal('show');
      $('#nom').val(first);
      
    });
  });
</script> 

<script type="text/javascript">

/*  function eliminar(hi){

//alert(hi); 
var confirmacion = confirm('Seguro que desea eliminar un registro?');
if(confirmacion===true){
    $.ajax({
    url:"<?php echo site_url(); ?>Docente/eliminarincidencia",
    method:"POST",
    data:{'id':hi},
    dataType:'json',
    success:function(data)
    {
     if(data == true)
     {
                          //alert("Se elimino exitosamente");
                          location.reload();

                        }else{alert("hubo un error al eliminar");}
                      },
                      error: function(error) {
                        alert("hubo un error al eliminar");
                    }
                }),
                error: function(error) {
                    alert("hubo un error al eliminar");
                }
            };

        }
    */

    function modificar(p) {
        //alert(p);
        $.ajax({
        url: "<?php echo site_url(); ?>Docente/modificarincidencia",
        method: "POST",
        data: {
        'idinc': p
        },
        dataType: 'json',
        success: function(data) {
        console.log(data);

        $('#nom').val(data[0][0].Id_incidencia);
        $('#obsr').val(data[0][0].Observacion);
        $('#salonM').val(data[0][0].FK_salon);
        $('#tipoincidenciaM').val(data[0][0].FK_tipos);
        $('#citaM').val(data[0][0].Cita);

        $( "#nombreninosM" ).html('');
        data[1].forEach(function(e) {

        $( "#nombreninosM" ).append( "<option value='"+e.Id_ninio+"'>"+e.Nombre_ninio+"</option>" );
        });
        $( "#nombreninosM" ).val(data[0][0].Id_ninio);

        $( "#incidenciaM" ).html('');
        data[2].forEach(function(e) {
        $( "#incidenciaM" ).append( "<option value='"+e.Id_catalogo_incidencias+"'>"+e.Nombre_incidencia+"</option>" );
        });
        $('#incidenciaM').val(data[0][0].Id_catalogo_incidencias);

        $('#modificar').modal('show');
        },
        error: function(error) {
        console.log(error);
        }


        })

    }
</script>
<!-- consulta para mandar el nombre de los ninos  A G R E G A R  I N C I D E N C I A-->
<script type="text/javascript">
    $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#salon").change(function() {
            console.log("change option");
            $("#salon option:selected").each(function() {
                FK_salon = $('#salon').val();
                // if(FK_salon!=''){
                $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreninos",
                    method: "POST",
                    data: {
                        FK_salon: FK_salon
                    },
                    success: function(data) {
                        $('#nombrenino').html(data);

                       // $('#nombreninos').html(data);
                       // console.log(data);
                    }
                })

            });
        });
    });
</script>
<!-- consulta para mandar el nombre de los ninos  M O D I F I C A R  I N C I D E N C I A-->
<script type="text/javascript">
    $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#salonM").change(function() {
            console.log("change option");
            $("#salonM option:selected").each(function() {
                FK_salon = $('#salonM').val();
                // if(FK_salon!=''){
                $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreninos",
                    method: "POST",
                    data: {
                        FK_salon: FK_salon
                    },
                    success: function(data) {
                        $('#nombreninosM').html(data);
                       // $('#nombreninos').html(data);
                       // console.log(data);
                    }
                })
            });
        });
    });
</script>
<!-- hace que la incidencia sea dependiente del select tipo de incidencia,  A G R E G A R  I N C I D E N C I A  -->
<script type="text/javascript">
    $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#tipoincidencia").change(function() {
            console.log("change option");
            $("#tipoincidencia option:selected").each(function() {
                FK_tipos = $('#tipoincidencia').val();
                // if(FK_salon!=''){

                $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreincidencias",
                    method: "POST",
                    data: {
                        FK_tipos: FK_tipos
                    },
                    success: function(data) {
                        $('#incidencia').html(data);
                        console.log(FK_tipos);
                    }


                })

            });
        });
    });
</script>

<!-- hace que la incidencia sea dependiente del select tipo de incidencia,  E D I T A R   I N C I D E N C I A  -->
<script type="text/javascript">
    $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#tipoincidenciaM").change(function() {
            console.log("change option");
            $("#tipoincidenciaM option:selected").each(function() {
                FK_tipos = $('#tipoincidenciaM').val();
                // if(FK_salon!=''){

                $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreincidencias",
                    method: "POST",
                    data: {
                        FK_tipos: FK_tipos
                    },
                    success: function(data) {
                        $('#incidenciaM').html(data);
                        console.log(FK_tipos);
                    }


                })

            });

          });
         });
        </script>

        <script type="text/javascript">   
          $(document).ready(function() {   
            //console.log("sdfgh");                    
            $("#tipoincidencia").change(function() {
              //console.log("change option");
              $("#tipoincidencia option:selected").each(function() {
                FK_tipos = $('#tipoincidencia').val();
                //console.log(FK_tipos);
                if (FK_tipos == '2') {
                  $("#Cita").hide();
                } else {
                  $("#Cita").show();
                } 
              });
            });
          });
        </script>
            
