<?php    
if($this->session->userdata('tipo_trabajador')!="FILTRO"){//echo "solo el admin tiene acceso aqui xD";
echo "<script>
window.location= '".site_url()."roles/roll'
</script>";

//header("location:".base_url());

}
?>

<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
<!-- <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
    
    .margen {
  margin-left: 20px;

}
</style>

<nav class="navbar navbar-expand-sm" style="background-color: #04774D;">

    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Filtro/" style="color: #FFFFFF">Inicio</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Filtro/CRUD_Incidencias/" style="color: #FFFFFF">Incidencias</a>
        </li>
        <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Filtro/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
    </li>
    </ul>
</nav>
<br>
<center><h4>Información al usuario de la atención a los niños(as)</h4></center>

<div id="tablaaa" class="table-responsive">
    <table id="table_id" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>SALÓN</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
           <?php foreach ($datosalones as $key => $value): ?>
            <tr>
                <td><?php echo $value->Nombre_salon; ?></td>
                <td><button type="button" title="Generar pdf del salón <?php echo $value->Nombre_salon; ?> "  id="pdff"><a target="_blank" href="<?php echo base_url('Filtro/pdf/'.$value->Id_salon);?>"><img src="<?php echo base_url();?>/Imagenes/pdf.png" width="26"></a></button></td>
            </tr>
            
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<script>$(document).ready( function () {
  $('#table_id').DataTable({
            responsive: true,
            language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
} );
</script>