<!-- se genera Tres columnas iguales -->
<div class="row">
    <div class="col-sm-1" style="background-color:white;"></div>
    <div class="col-sm-10" style="background-color:white">
        <!-- inica Segunda colomna -->
        <h1 class="text-center p-2"> BIENVENIDO <br>
            <a><?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno') ?>, </a> ENCARDADO DE FILTRO.
        </h1>
        <h style=”text-align: justify;”>En el servicio de guardería se atiende el desarrollo integral del niño y la niña, a través del cuidado y fortalecimiento de su salud, además se brinda una sana alimentación y un programa educativo-formativo acorde a su edad y nivel de desarrollo. </h>
        <br><br>
    </div><!-- Termina Segunda columna -->
    <div class="col-sm-1" style="background-color:white;"></div> <!-- Tercera columna -->
</div> <!-- Termina el codigo de Tres columnas iguales -->
