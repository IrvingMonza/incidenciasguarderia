<?php    
if($this->session->userdata('tipo_trabajador')!="FILTRO"){//echo "solo el admin tiene acceso aqui xD";
echo "<script>
window.location= '".site_url()."roles/roll'
</script>";

//header("location:".base_url());

}
?>

<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
<!-- <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}

.margen {
  margin-left: 20px;

}
</style>

<nav class="navbar navbar-expand-sm" style="background-color: #04774D;">

    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Filtro/" style="color: #FFFFFF">Inicio</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Filtro/CRUD_Incidencias/" style="color: #FFFFFF">Incidencias</a>
        </li>
        <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Filtro/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
    </li>
    </ul>
</nav>
<br>
<h3>
  <center>
    SECCIÓN DE INCIDENCIAS
  </center>
</h3>
<?php if ($this->session->flashdata('alerta') == null): ?>

<?php endif ?>
<?php if ($this->session->flashdata('alerta') != null): ?>
  <div class="<?php  echo $this->session->flashdata('color'); ?>">
    <h5><?php  echo $this->session->flashdata('alerta'); ?> </h5>
  </div>
<?php endif ?>

<table class="table-bordered margen" >
 
  <thead>
   <tr >
     <td colspan="4"><B><CENTER> PALETAS DE COLORES </CENTER> </B></td>
   </tr>
   <TR>
     <td colspan="2"><B><CENTER>TIPO DE INCIDENCIA</CENTER> </B></td>
     <td colspan="2"><B><CENTER>FECHA DE LA CITA</CENTER> </B></td>
   </TR>
   <tr>
    <th>LOGRO</th>
    <th style="background-color:#2DFF41;" WIDTH="30" HEIGHT="30"></th>
    <th>FECHA POR ASIGNAR</th>
    <th style="background-color:#F0F05F;" WIDTH="30" HEIGHT="30"></th>
  </tr>
  <tr>
    <th>INCIDENCIA</th>
    <th style="background-color:#FFF639;"  WIDTH="30" HEIGHT="30"></th>
    <th >NO REQUIERE CITA</th>
    <th style="background-color:#9CA2F1;" WIDTH="30" HEIGHT="30"></th>
  </tr>
  <tr>
    <th>CONDUCTA</th>
    <th style="background-color:#FF2929;"  WIDTH="30" HEIGHT="30"></th>
    <th>FECHA PERDIDA DE LA CITA</th>
    <th style="background-color:#FF6262;" WIDTH="30" HEIGHT="30"></th>
  </tr>
  <tr>
    <th>CITA</th>
    <th style="background-color:#58ABFF"  WIDTH="30" HEIGHT="30"></th>
    <th>CITA ASIGNADA</th>
    <th style="background-color:#52D55C;" WIDTH="30" HEIGHT="30" ></th>
  </tr>
  
  <div id="tablaaa">
      <table id="table_id" class="table table-responsive table-striped table-bordered" class="display">
          <thead>
              <tr>
                  <th>FECHA DE INCIDENCIA</th>
                  <th>SALÓN</th>
                  <th>NOMBRE DEL NIÑO</th>
                  <th>TIPO DE INCIDENCIA</th>
                  <th>INCIDENCIA</th>
                  <th>OBSERVACIÓN</th>
                  <th>CITA</th>
                  <th>NOMBRE DE QUIEN REGISTRA</th>
                  <th>NOMBRE DE QUIEN REVISA</th>
                  <th>FECHA DE LA CITA</th>
                  <th>ESTADO DE LA CITA</th>
                  <th>FIRMA DEL TUTOR</th>
                  <th>ACCIONES</th>
              </tr>
          </thead>
          <tbody>
              <?php foreach ($datos as $key => $value): ?>
              <tr>
                  <td><span id="fechabas"><?php echo $value->FechaBase; ?></span></td>
                  <td><?php echo $value->Nombre_salon; ?></td>
                  <td><?php echo $value->Nombre_ninio; ?></td>
                  <?php     if($value->Nombre_tipo_inc=="CONDUCTA"){ ?>
                  <td style="background-color:#FF2929; color:#FEFEFE;" style="color: red"> <?php echo $value->Nombre_tipo_inc;?> </td>
                  <?php  } ?>
                  <?php if($value->Nombre_tipo_inc=="LOGRO"){ ?>
                  <td style="background-color:#2DFF41;"> <?php echo $value->Nombre_tipo_inc;?> </td>
                  <?php  } ?>
                  <?php if($value->Nombre_tipo_inc=="INCIDENCIA"){ ?>
                  <td style="background-color:#FFF639;"> <?php echo $value->Nombre_tipo_inc;?> </td>
                  <?php  } ?>
                  <?php if($value->Nombre_tipo_inc=="CITA"){ ?>
                  <td style="background-color:#58ABFF"> <?php echo $value->Nombre_tipo_inc;?> </td>
                  <?php  } ?>
                  <td><?php echo $value->Nombre_incidencia; ?></td>
                  <td><?php echo $value->Observacion; ?></td>
                  <td><?php echo $value->Cita; ?></td>
                  <td><?php echo $value->Nombre_docente; ?></td>
                  <td><?php echo $value->Nombre_tecnico; ?></td>
                  
                  
                  <?php if($value->EstadoCita=="ATENDIDO"){ ?>
                  <td style="background-color:#52D55C; color:#FEFEFE;"><?php echo $value->FechaBaseCita; ?></td>
                  <?php  } else { ?>
    
                  <?php if($value->Cita=="SIN CITA"){ ?>
                  <td style="background-color:#9CA2F1; color:#FEFEFE;"> NO REQUIERE CITA </td>
                  <?php  } else {
                  if($value->FechaBaseCita== NULL){
                  ?>
                  <td style="background-color:#F0F05F;"> FECHA POR ASIGNAR </td>
                  <?php  }else{ 
                  if(date('Y-m-d') > $value->FechaBaseCita){   ?>
                  <td style="background-color:#FF6262; color:#FEFEFE;"><?php echo $value->FechaBaseCita; ?></td>
                  <?php  }else{ ?>

                  <td style="background-color:#52D55C; color:#FEFEFE;"><?php echo $value->FechaBaseCita; ?></td>

                  <?php } } }} ?>
                  
                  
                  
                  
                  
                  <?php if($value->Cita=="SIN CITA"){ ?>
                  <td> NO REQUIERE CITA </td>
                  <?php  } else {
                  if($value->FechaBaseCita== NULL){
                  ?>
                  <td> FECHA POR ASIGNAR </td>
                <?php  }else{ ?>
                  
                  <td><?php echo $value->EstadoCita; ?></td>
                <?php } } ?>
                
                <td><?php echo $value->FirmaTutor; ?></td>
                
                <td>
                  <button type="button" title="Confirmacion de contraseña del tutor" id="modif" onclick="clave(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/firma1.png" width="26"></button>
                  <?php if($value->Cita != "SIN CITA"){?>
                    <button type="button" title="Agendar Cita" onclick="Altacita(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/calendar.png" width="26"></button><?php  } else { ?>
                    <?php  } ?>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>


        <!-- M O D A L    P A R A     V A L I D A R    L A    C O N T R A S E Ñ A    D E L    P A D R e -->  
        <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header" style="background:#04774D">
                <a style="color:#FFFFFF"><h4>FIRMA DEL PADRE O TUTOR</h4></a>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php echo form_open('Filtro/validartutor'); ?>
              <div class="modal-body">
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL NIÑO:</label>
                  <div class="col-md-6">
                    <input type="text" name="nnino" id="nnino" class="form-control" aria-describedby="emailHelp" disabled>
                    <input type="text"name="nom" id="nom" hidden>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL PADRE O TUTOR:</label>
                  <div class="col-md-6">
                    <input type="text" name="idtutor" id="idtutor" class="form-control" aria-describedby="emailHelp" hidden>
                    <input type="text" name="ntutor" id="ntutor" class="form-control" aria-describedby="emailHelp" disabled>
                  </div>
                </div>
                                <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">PERSONA AUTORIZADA:</label>
          <div class="col-md-6">
            <select name="autorizado" id="autorizado" class="form-control">
                <option value="">seleccione una opcion</option>
                
            </select>
          </div>
        </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label text-md-right">CONTRASEÑA DE CONFIRMACIÓN:</label>
                  <div class="col-md-6">
                    <input type="password" name="pass" id="pass" class="form-control" aria-describedby="emailHelp">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Aceptar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- M O D A L    P A R A     D A R   D E    A L T A    U N A   C I T A -->
      <?php echo form_open('Filtro/Altacita'); ?>
      <div class="modal fade" id="agregarcita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background:#04774D">
              <a style="color:#FFFFFF"> <h4> AGREGAR CITA </h4></a>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL TÉCNICO:</label>
                <div class="col-md-6">
                  <input type="text" name="idtec" id="idtec" class="form-control" aria-describedby="emailHelp" disabled>
                </div>
              </div>
              <div class="form-group row">
                <input type="text" name="id" id="id" hidden>
                <label class="col-sm-4 col-form-label text-md-right">FECHA DE CITA:</label>
                <div class="col-md-6">
                  <input type="datetime-local" name="fechabase" id="hora" class="form-control" aria-describedby="emailHelp" required>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Aceptar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </form>

    <script>$(document).ready( function () {
      $('#table_id').DataTable({
        responsive: true,
        language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
              }
            });
    } );

  </script>

  <script type="text/javascript">
    function clave(p) {

        //alert(p);
        $.ajax({
          url: "<?php echo site_url(); ?>Filtro/datofirmapadre",
          method: "POST",
          data: {
            'idinc': p
          },
          dataType: 'json',
          success: function(data) {
                console.log(data);
                $('#nom').val(data[0][0].Id_incidencia);
                $('#nnino').val(data[0][0].Nombre_ninio);
                $('#idtutor').val(data[0][0].Id_tutores);
                $('#ntutor').val(data[0][0].Nombre_tutor);
                $("#autorizado").html('');
    data[1].forEach(function(e) {
      $("#autorizado").append("<option value=''> SELECCIONE UNA OPCIÓN</option>");
        $("#autorizado").append("<option value='"+e.Idautorizado+"'>" + e.Nombre_autorizado + "</option>");
        $("#autorizado").append("<option value='"+e.Idautorizado1+"'>" + e.Nombre_autorizado1 + "</option>");
        $("#autorizado").append("<option value='"+e.Idautorizado2+"'>" + e.Nombre_autorizado2 + "</option>");
        });
                $('#modificar').modal('show');
              },
              error: function(error) {
                console.log(error);
              }
            })

      }

      function Altacita(pi) {

        //alert(pi);

        $.ajax({
          url: "<?php echo site_url(); ?>Filtro/Datoscita",
          method: "POST",
          data: {
            'idinc': pi
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $('#id').val(data[0].Id_incidencia);
            $('#idtec').val(data[0].Nombre_tecnico);
            $('#fecha').val(data[0].FechaBaseCita);
            $('#agregarcita').modal('show');
          },
          error: function(error) {
            console.log(error);
          }
        })

      }
    </script>
