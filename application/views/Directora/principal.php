<?php 	 
if($this->session->userdata('tipo_trabajador')!="DIRECTORA"){//echo "solo el admin tiene acceso aqui xD";
echo "<script>
window.location= '".site_url()."roles/roll'
</script>";


}
?>
<?php
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<nav class="navbar navbar-expand-sm" style="background-color: #04774D;">
  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Directora/" style="color: #FFFFFF">Inicio</a>
    </li>
    
    <!-- Dropdown 1 -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Catálogos
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Trabajador">Trabajadores</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Tutores/">Tutores</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_autorizados/">Autorizados</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Ninios">Niños</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Salones">Salones</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Tipos/">Tipos de Incidencias</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_catalogo_incidencias/">Incidencias</a>
      </div>
    </li>
    <ul class="navbar-nav">
      <!-- Dropdown 3 -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
          Registrar Incidencia
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias">Mostrar Todo</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias_atendidas">Incidencias Atendidas</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias_pendientes">Incidencias Pendientes</a>
        </div>
      </li>
      <!-- Dropdown 4-->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
          Citas
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas">Mostrar Todo</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas_atendidas">Citas Atendidas</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas_pendientes">Citas Pendientes</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Directora/Control" style="color: #FFFFFF">Control</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Directora/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
      </li>
    </ul>
  </nav>
  <br>
  <h3>
    <center>
      <?php echo $nombre ?>  
    </center>
  </h3>
  <?php echo $output ?>

  <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
  <?php endforeach; ?>
  <script type="text/javascript">   
    $(document).ready(function() {   
            //console.log("sdfgh");                    
            $("#field-Id_salon").change(function() {
              console.log("change option");
              $("#field-Id_salon option:selected").each(function() {
                SalaGrupott = $('#field-Id_salon').val();
                $.post("<?php echo site_url(); ?>ComboDependet/fillNombres", {
                  'field-Id_salon' : SalaGrupott
                }, function(data) {
                  console.log(data);
                  $("#field-Id_ninio").html(data);
                  $("#field-Id_ninio").html(data);
                });
              });
            });
          });
        </script>
        <script>
          function habilitar(value)
          {
            if(value==true)
            {
      // habilitamos
      document.getElementById("field-Contrasena").disabled=false;
    }else if(value==false){
      // deshabilitamos
      document.getElementById("field-Contrasena").disabled=true;
    }
  }
  $(document).ready(function() {
    $('#check').click(function() {
      $('#field-Contrasena').val('');
    });
  });
</script>

<br><br>