<?php    
if($this->session->userdata('tipo_trabajador') != "DIRECTORA")
{
 echo "<script>
 window.location= '".site_url()."roles/roll'
 </script>";
}
?>
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
  <!-- <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
  <script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
  <style>
  .sa {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #04774D;
  }

  li {
    float: left;
  }

  li a, .dropbtn {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }

  li a:hover, .dropdown:hover .dropbtn {
    background-color: #D1D5D2;
  }

  li.dropdown {
    display: inline-block;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }

  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
  }

  .dropdown-content a:hover {background-color: #f1f1f1}

  .dropdown:hover .dropdown-content {
    display: block;
  }
</style>

<nav class="navbar navbar-expand-sm" style="background-color: #04774D;">
  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Directora/" style="color: #FFFFFF">Inicio</a>
    </li>
    
    <!-- Dropdown 1 -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Catálogos
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Trabajador">Trabajadores</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Tutores/">Tutores</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_autorizados/">Autorizados</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Ninios">Niños</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Salones">Salones</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Tipos/">Tipos de Incidencias</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_catalogo_incidencias/">Incidencias</a>
      </div>
    </li>
    <ul class="navbar-nav">
    <!-- Dropdown 3 -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Registrar Incidencia
      </a>
       <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias">Mostrar Todo</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias_atendidas">Incidencias Atendidas</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias_pendientes">Incidencias Pendientes</a>
      </div>
    </li>
     <!-- Dropdown 4-->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Citas
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas">Mostrar Todo</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas_atendidas">Citas Atendidas</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas_pendientes">Citas Pendientes</a>
      </div>
    </li>
     <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Directora/Control" style="color: #FFFFFF">Control</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Directora/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
    </li>
  </ul>
</nav>
<br>
<h3>
  <center>
    <?php echo $seccion ?>  
  </center></h3>
  <div class="table-responsive">
  <table id="table2" class="table table-responsive table-striped table-bordered" class="display" > 
    <thead>
        <tr>
            <th>FECHA DE LA CITA</th>
            <th>NOMBRE DEL NIÑO</th>
            <th>TIPO DE INCIDENCIA</th>
            <th>INCIDENCIA</th>
            <th>OBSERVACIÓN DE LA INCIDENCIA</th>
            <th>NOMBRE DE QUIEN REVISA</th>
            <th>FECHA QUE FUE ATENTIDA LA CITA</th>
            <th>DESCRIPCIÓN DE LA CITA</th>
            <th>ESTADO CITA</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($datos as $key => $value): ?>  
            <tr>
              <td><?php echo $value->FechaBaseCita; ?></td>
              <td><?php echo $value->Nombre_ninio; ?></td>
              <?php     if($value->Nombre_tipo_inc=="CONDUCTA"){ ?>
              <td style="background-color:#FF4646;" style="color: red"> <?php echo $value->Nombre_tipo_inc;?> </td>
              <?php  } ?>
              <?php if($value->Nombre_tipo_inc=="LOGRO"){ ?>
              <td style="background-color:#2DFF41;"> <?php echo $value->Nombre_tipo_inc;?> </td>
              <?php  } ?>
              <?php if($value->Nombre_tipo_inc=="INCIDENCIA"){ ?>
              <td style="background-color:#FFF639;"> <?php echo $value->Nombre_tipo_inc;?> </td>
              <?php  } ?>
              <?php if($value->Nombre_tipo_inc=="CITA"){ ?>
              <td style="background-color:#58ABFF"> <?php echo $value->Nombre_tipo_inc;?> </td>
              <?php  } ?>
             <td><?php echo $value->Nombre_incidencia; ?></td>
             <td><?php echo $value->Observacion; ?></td>
             <td><?php echo $value->Nombre_tecnico; ?></td>
             <td><?php echo $value->FechaAtencionCita; ?></td>
             <td><?php echo $value->Descripcion; ?></td>
             <td><?php echo $value->EstadoCita; ?></td>
             <td>
              <!--  <button type="button" class="btn btn-primary">Agregar</button> &#128269;-->
              <button type="button" title="Revisar cita" id="mod" onclick="modificarcitas(<?php echo $value->Id_citas; ?>)"><img src="<?php echo base_url();?>/Imagenes/revisar.png" width="26"></button>
              <?php if($value->EstadoCita == "PENDIENTE"){ ?>
              <button type="button" title="Eliminar cita" id="elim" onclick="eliminarcitas(<?php echo $value->Id_citas; ?>)"><img src="<?php echo base_url();?>/Imagenes/cancel.png" width="26"></button>
              <?php } else{  ?>
              <?php } ?>
            </td>
          </tr>
        <?php endforeach ?>       

      </tbody>
    </table>

  </div>
  <!-- M O D A L    P A R A     M O D I F I C A R    U N    R E G I S T R O -->
  <?php echo form_open('Directora/modifcita'); ?>
  <div class="modal fade" id="btm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
       <div class="modal-header" style="background-color:#04774D">
        <font color="white" ><h4>REVISAR CITA</h4> </font>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <input name="fechac" id="fechacita" type="datetime-local" hidden>
       <input type="text" name="idcita" id="cita" hidden>
       <div class="form-group row">
         <label for="nickname" class="col-sm-4 col-form-label text-md-right">DESCRIPCIÓN DE LA CITA :</label>
         <div class="col-md-6">
           <textarea name="desc" id="descripcion" type="text" cols="51" rows="5" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" ></textarea>
         </div>
       </div>
     </div>
     <div class="form-group row">
       <label for="nickname" class="col-sm-4 col-form-label text-md-right">ESTADO CITA :</label>
       <div class="col-md-6">
         <select class="form-control" name="estadocita" id="estadocita">
           <option value="ATENDIDO">ATENDIDO</option>
           <option value="PENDIENTE">PENDIENTE</option>
         </select>
       </div>
     </div>
     
     <div class="modal-footer">
       <button type="submit" class="btn btn-primary">Guardar</button>
       <button type="button" class="btn btn-primary"data-dismiss="modal">Cancelar</button>
     </div>
   </div>
 </div>
</div>
</form>

<!-- M O D A L    P A R A     E L I M I N A R    U N    R E G I S T R O -->
<div>
  <form id="frmEliminarUsuario" action="" method="POST">
    <input type="hidden" id="idusuario" name="idusuario" value="">
    <input type="hidden" id="opcion" name="opcion" value="eliminar">
    <!-- Modal -->
    <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalEliminarLabel">Eliminar Usuario</h4>
          </div>
          <div class="modal-body">              
            ¿Está seguro de eliminar al usuario?<strong data-name=""></strong>
          </div>
          <div class="modal-footer">
            <button type="button" id="eliminar-usuario" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
  </form>
</div>
<script>$(document).ready( function () {
  $('#table2').DataTable({
            responsive: true,
            language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
} );


</script>
<script>
  
  
  function modificarcitas(p){
    
    $.ajax({
      url:"<?php echo site_url(); ?>Directora/modificarcitas",
      method:"POST",
      data:{'idcitas':p},
      dataType:'json',
      success:function(data)
      {
        console.log(data);
        $('#cita').val(data[0].Id_citas);
        $('#descripcion').val(data[0].Descripcion);
        $('#btm').modal('show');
      },error: function(error){
        console.log(error);
      }
    });
  }
  
  function eliminarcitas(el){
    
    var confirmacion=confirm('Seguro que deseas eliminar el registro?')
    if(confirmacion===true){
      $.ajax({
        url:"<?php echo site_url(); ?>Directora/eliminarcitas",
        method:"POST",
        data:{'idcitas':el},
        dataType:'json',
        success:function(data)
        {
          if(data == true){
            location.reload();
          }else{
            alert('Hubo un error al eliminar');
          }
        },error: function(error){
          alert('Hubo un error al eliminar');
        }
      });
      
    }
  }
</script>