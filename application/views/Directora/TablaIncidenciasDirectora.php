<?php    
if($this->session->userdata('tipo_trabajador') != "DIRECTORA")
{
 echo "<script>
 window.location= '".site_url()."roles/roll'
 </script>";
}
?>

<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
<!-- <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
<link href="<?php echo base_url();?>assets/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
<nav class="navbar navbar-expand-sm" style="background-color: #04774D;">
  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Directora/" style="color: #FFFFFF">Inicio</a>
    </li>

    <!-- Dropdown 1 -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Catálogos
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Trabajador">Trabajadores</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Tutores/">Tutores</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_autorizados/">Autorizados</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Ninios">Niños</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Salones">Salones</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Tipos/">Tipos de Incidencias</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_catalogo_incidencias/">Incidencias</a>
      </div>
    </li>
    <ul class="navbar-nav">
      <!-- Dropdown 3 -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
          Registrar Incidencia
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias">Mostrar Todo</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias_atendidas">Incidencias Atendidas</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Incidencias_pendientes">Incidencias Pendientes</a>
        </div>
      </li>
      <!-- Dropdown 4-->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
          Citas
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas">Mostrar Todo</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas_atendidas">Citas Atendidas</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Directora/CRUD_Citas_pendientes">Citas Pendientes</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Directora/Control" style="color: #FFFFFF">Control</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Directora/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
      </li>
    </ul>
  </nav>
  <br>
  <h3>
    <center>
      <?php echo $seccion ?>
    </center></h3>
    <?php if ($this->session->flashdata('alerta') == null): ?>

    <?php endif ?>
    <?php if ($this->session->flashdata('alerta') != null): ?>
      <div class="<?php  echo $this->session->flashdata('color'); ?>">
        <h5><?php  echo $this->session->flashdata('alerta'); ?> </h5>
      </div>
    <?php endif ?>
    <div id="tablaaa" class="table-responsive">
      <table id="table_id" class="table table-responsive table-striped table-bordered" class="display"> 
        <thead>
          <tr>
            <th><button type="button" title="Agregar incidencia" data-toggle="modal" data-target="#agregar"><img src="<?php echo base_url();?>/Imagenes/add.png" width="26"></button></th>
            <th>FECHA DE INCIDENCIA</th>
            <th>SALÓN</th>
            <th>NOMBRE DEL NIÑO</th>
            <th>TIPO DE INCIDENCIA</th>
            <th>INCIDENCIA</th>
            <th>OBSERVACIÓN</th>
            <th>CITA</th>
            <th>NOMBRE DE QUIEN REGISTRA</th>
            <th>FECHA Y HORA DE REVISIÓN</th>
            <th>NOMBRE DE QUIEN REVISA</th>
            <th>ESTADO DE LA INCIDENCIA</th>
            <th>FECHA DE LA CITA</th>
            <th>ESTADO DE LA CITA</th>
            <th>ESTADO DEL CORREO</th>
            <th>FIRMA DEL TUTOR</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
         <?php foreach ($datos as $key => $value): ?>
          <tr>
           <td></td>
           <td><span id="fechabas"><?php echo $value->FechaBase; ?></span></td>
           <td><?php echo $value->Nombre_salon; ?></td>
           <td><?php echo $value->Nombre_ninio; ?></td>
           <?php     if($value->Nombre_tipo_inc=="CONDUCTA"){ ?>
             <td style="background-color:#FF4646;" style="color: red"> <?php echo $value->Nombre_tipo_inc;?> </td>
           <?php  } ?>
           <?php if($value->Nombre_tipo_inc=="LOGRO"){ ?>
             <td style="background-color:#2DFF41;"> <?php echo $value->Nombre_tipo_inc;?> </td>
           <?php  } ?>
           <?php if($value->Nombre_tipo_inc=="INCIDENCIA"){ ?>
             <td style="background-color:#FFF639;"> <?php echo $value->Nombre_tipo_inc;?> </td>
           <?php  } ?>
           <?php if($value->Nombre_tipo_inc=="CITA"){ ?>
             <td style="background-color:#58ABFF"> <?php echo $value->Nombre_tipo_inc;?> </td>
           <?php  } ?>
           <td><?php echo $value->Nombre_incidencia; ?></td>
           <td><?php echo $value->Observacion; ?></td>
           <td><?php echo $value->Cita; ?></td>
           <td><?php echo $value->Nombre_docente; ?></td>
           <td><?php echo $value->FechaAtencion; ?></td>
           <td><?php echo $value->Nombre_tecnico; ?></td>
           <td><?php echo $value->EstadoIncidencia; ?></td>

           <?php if($value->EstadoCita=="ATENDIDO"){ ?>
            <td style="background-color:#52D55C; color:#FEFEFE;"><?php echo $value->FechaBaseCita; ?></td>
          <?php  } else { ?>

            <?php if($value->Cita=="SIN CITA"){ ?>
              <td style="background-color:#9CA2F1; color:#FEFEFE;"> NO REQUIERE CITA </td>
            <?php  } else {
              if($value->FechaBaseCita== NULL){
                ?>
                <td style="background-color:#F0F05F;"> FECHA POR ASIGNAR </td>
              <?php  }else{ 
                if(date('Y-m-d') > $value->FechaBaseCita){   ?>
                  <td style="background-color:#FF6262; color:#FEFEFE;"><?php echo $value->FechaBaseCita; ?></td>
                <?php  }else{ ?>

                  <td style="background-color:#52D55C; color:#FEFEFE;"><?php echo $value->FechaBaseCita; ?></td>

                <?php } } }} ?>


                <?php if($value->Cita=="SIN CITA"){ ?>
                  <td> NO REQUIERE CITA </td>
                <?php  } else {
                  if($value->FechaBaseCita== NULL){
                    ?>
                    <td> FECHA POR ASIGNAR </td>
                  <?php  }else{ ?>

                    <td><?php echo $value->EstadoCita; ?></td>
                  <?php } } ?>

                  <td><?php echo $value->EntregaIncidencia; ?></td>
                  <td><?php echo $value->FirmaTutor; ?></td>         
                  <td>
                    <button type="button" title="Editar Incidencia" id="modif" onclick="modificar(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/editar.png" width="26"></button>
                    <button type="button" title="Eliminar incidencia" onclick="eliminar(<?php echo $value->Id_incidencia; ?>)" data-toggle="modal" data-target="#"><img src="<?php echo base_url();?>/Imagenes/cancel.png" width="26"></button>
                    <?php if($value->EstadoIncidencia =="ATENDIDA"){ ?>

                     <button type="button" title="Confirmación de contraseña del tutor " id="clav" onclick="clave(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/firma1.png" width="26"></button>
                     <button type="button" title="Agendar cita" onclick="Altacita(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/calendar.png" width="26"></button>


                   <?php   } ?>
                   <button type="button"  id="btnenviar" title="Revisar incidencia " onclick="botonrevisar(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/revisar.png" width="26"></button>
                   <?php if($value->EstadoRevisadoIncidencia == "ATENDIDA"){ ?>
                     <button type="button"  id="btnenviar" title="Enviar correo " onclick="botonenviar(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/correo.png" width="26"></button>
                   <?php } ?>
                 </td>
               </tr>
             <?php endforeach ?> 
           </tbody>
         </table>
       </div>


       <!-- M O D A L    P A R A     M O D I F I C A R    U N    R E G I S T R O -->  
       <?php echo form_open('Directora/modifinc'); ?>
       <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#04774D">
              <font color="white">
                <h4>EDITAR INCIDENCIA</h4>
              </font>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">

              <div class="form-group row">
                <label for="pass" class="col-md-4 col-form-label text-md-right">SALÓN :</label>
                <input type="text" id="id" name="id" hidden>
                <div class="col-md-6">
                  <select class="form-control" name="salonD" id="salonD" class="salonM" required>
                    <option>SELECCIONE UN SALÓN</option>
                    <?php foreach ($salon as $key => $value): ?>
                      <option value="<?php echo $value->Id_salon; ?>">
                        <?php echo $value->Nombre_salon; ?>
                      </option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="pass" class="col-md-4 col-form-label text-md-right">NOMBRE DEL NIÑO :</label>
                <div class="col-md-6">
                  <select class="form-control" name="nombreninosD" id="nombreninosD" required>
                    <option value="">SELECCIONA UN NOMBRE</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="pass" class="col-md-4 col-form-label text-md-right">TIPO INCIDENCIA :</label>
                <div class="col-md-6">
                  <select class="form-control" name="tipoincidenciaD" id="tipoincidenciaD" required>
                    <option value="">SELECCIONE UN TIPO DE INCIDENCIA</option>
                    <?php foreach ($tipos as $key => $value): ?>
                      <option value="<?php echo $value->Id_catalogo_tipos; ?>">
                        <?php echo $value->Nombre_tipo_inc; ?>
                      </option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="pass" class="col-md-4 col-form-label text-md-right">INCIDENCIA :</label>
                <div class="col-md-6">
                  <select class="form-control" name="incidenciaD" id="incidenciaD" required>
                    <option value="">SELECCIONE UNA INCIDENCIA</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="pass" class="col-md-4 col-form-label text-md-right">CITA :</label>
                <div class="col-md-6">
                  <select class="form-control" name="citaD" id="citaD" required>
                    <option value="">ELIGE UNA OPCIÓN</option>
                    <option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
                    <option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIÓLOGO</option>
                    <option value="SIN CITA">SIN CITA</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">

                <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
                <div class="col-md-6">
                  <textarea name="obsrD" id="obsrD" type="text" class="form-control" cols="49" rows="5" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL TÉCNICO:</label>
                <div class="col-md-6">
                  <!-- El nombre y id eran obsr -->
                  <input type="text" name="nombretecnico" value="<?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?>" id="nombretecnico" class="form-control" aria-describedby="emailHelp" disabled>
                  <input name="Idtecnico" type="hidden" value="<?php echo $this->session->userdata('Id_trabajadores') ?>">
                </div>
              </div>

            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Modificar</button>
            </div>
          </div>
        </div>
      </div>

    </form>

    <!-- M O D A L    P A R A     A G R E G A R    U N    R E G I S T R O -->
    <div id="agregar" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header" style="background-color:#04774D">
          <font color="white" ><h4>AGREGAR UNA INCIDENCIA</h4> </font>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <?php echo form_open('Directora/guardarincidencia'); ?>
        <div class="modal-body">

         <div class="form-group row">
           <label for="pass" class="col-md-4 col-form-label text-md-right" >SALÓN :</label>
           <div class="col-md-6">
            <select class="form-control" name="salon" id="salon" required>
              <option value="">SELECCIONE UN SALÓN</option>
              <?php foreach ($salon as $key => $value): ?>
               <option value="<?php echo $value->Id_salon; ?>">
                 <?php echo $value->Nombre_salon; ?>
               </option>
             <?php endforeach ?>
           </select>
         </div>
       </div>

       <div class="form-group row">
        <label for="pass" class="col-md-4 col-form-label text-md-right" >NOMBRE DEL NIÑO :</label>
        <div class="col-md-6">
          <select class="form-control" name="nombrenino" id="nombrenino" required>
           <option value="">SELECCIONA UN NOMBRE</option>
         </select>
       </div>
     </div>

     <div class="form-group row">
      <label for="pass" class="col-md-4 col-form-label text-md-right" >TIPO INCIDENCIA :</label>
      <div class="col-md-6">
        <select class="form-control" name="tipoincidencia" id="tipoincidencia" required>
          <option value="">SELECCIONE UN TIPO DE INCIDENCIA</option>
          <?php foreach ($tipos as $key => $value): ?>
           <option value="<?php echo $value->Id_catalogo_tipos; ?>">
             <?php echo $value->Nombre_tipo_inc; ?>
           </option>
         <?php endforeach ?>
       </select>
     </div>
   </div>

   <div class="form-group row">
    <label for="pass" class="col-md-4 col-form-label text-md-right" >INCIDENCIA :</label>
    <div class="col-md-6">
     <select name="incidencia" class="form-control" id="incidencia" required>
       <option value="">SELECCIONE UNA INCIDENCIA</option>
     </select>
   </div>
 </div>
 <div class="modal-body">
  <div class="form-group row">
    <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
    <div class="col-md-6">
      <textarea rows="4" cols="50" name="obsr" id="obsr" class="form-control" aria-describedby="emailHelp" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
    </div>
  </div>
</div>

<div class="form-group row" id="Cita">
  <label for="pass" class="col-md-4 col-form-label text-md-right" >CITA :</label>
  <div class="col-md-6">
    <select class="form-control" name="cita" id="cita">
      <option value="">ELIGE UNA OPCIÓN</option>
      <option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
      <option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIÓLOGO</option>
      <option value="SIN CITA">SIN CITA</option>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="pass" class="col-md-4 col-form-label text-md-right" >NOMBRE DEL DOCENTE :</label>
  <div class="col-md-6">
    <input id="nombredocente" type="text" class="form-control" placeholder="Ingresa " name="nombredocente" value="<?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?>" disabled >
    <input name="Iddocente" type="hidden" value="<?php echo $this->session->userdata('Id_trabajadores') ?>">
  </div>
</div>
</div>


<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Agregar</button>
  <button type="button" class="btn btn-default"data-dismiss="modal">Cancelar</button>
</div>

</form>
</div>
</div>
</div>

<!-- M O D A L    P A R A     D A R   D E    A L T A    U N A   C I T A -->
<?php echo form_open('Directora/Altacita'); ?>
<div class="modal fade" id="agregarcita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background:#04774D">
        <a style="color:#FFFFFF"><h4>AGREGAR CITA</h4></a>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL TÉCNICO:</label>
          <div class="col-md-6">
            <input type="text" name="idtec" id="idtec" class="form-control" aria-describedby="emailHelp" disabled>
          </div>
        </div>
        <div class="form-group row">
          <input type="text" name="idd" id="idd" hidden>
          <label class="col-sm-4 col-form-label text-md-right">FECHA DE CITA:</label>
          <div class="col-md-6">
            <input type="datetime-local" name="fechabase" id="hora" class="form-control" aria-describedby="emailHelp" required>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- M O D A L    P A R A  E N V I A R    U N    R E G I S T R O -->
<?php echo form_open('Directora/enviarincid'); ?>
<div class="modal fade" id="botonenviarcorreo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#04774D">
        <font color="white">
          <h4>ENVIAR CORREO ELECTRÓNICO</h4>
        </font>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <input type="text" name="idE" id="idE" hidden>
          <label class="col-sm-4 col-form-label text-md-right">ASUNTO:</label>
          <div class="col-md-6">
            <textarea name="asunto" id="asunto" type="text" class="form-control" cols="49" rows="5" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">CUERPO DEL CORREO:</label>
          <div class="col-md-6">
            <textarea name="cuerpocorreo" id="cuerpocorreo" ><b></b></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">TIPO DE INCIDENCIA:</label>
          <div class="col-md-6">
            <input type="text" name="nombretipo" id="nombretipo" >
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">INCIDENCIA:</label>
          <div class="col-md-6">
            <input type="text" name="nombreinc" id="nombreinc" >
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
          <div class="col-md-6">
            <textarea name="obsrE" id="obsrE" type="text" class="form-control" cols="49" rows="5" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
          </div>
        </div>
<!--         <input type="text" name="nombretutor" id="nombretutor" >
        <input type="text" name="nombreninio" id="nombreninio" >
      -->        <input type="text" name="correo" id="correo" hidden>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Enviar correo</button>
    </div>
  </div>
</div>
</div>

</form>

<!-- M O D A L    P A R A    R E V I S A R -->
<?php echo form_open('Directora/revisar'); ?>
<div class="modal fade" id="botonrevisar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header" style="background:#04774D">
        <a style="color:#FFFFFF"><h4>REVISAR INCIDENCIA</h4></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <input type="text" name="idER" id="idER" hidden>
          <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
          <div class="col-md-6">
            <textarea name="obsrER" id="obsrER" type="text" class="form-control" cols="49" rows="5" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL TÉCNICO:</label>
          <div class="col-md-6">
            <!-- el nombre y id eran obsr -->
            <input type="text" name="tecnicoR" value="<?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?>" id="tecnicoR" class="form-control" aria-describedby="emailHelp" disabled>
            <input name="IdtecnicoR" type="hidden" value="<?php echo $this->session->userdata('Id_trabajadores') ?>">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">ESTADO DE LA INCIDENCIA:</label>
          <div class="col-md-6">
            <select name="EstadoIncR" id="EstIncR" class="form-control">
              <option value="ATENDIDA">ATENDIDA</option>
              <option value="PENDIENTE">PENDIENTE</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Revisar</button>
      </div>
    </div>
  </div>
</div>

</form>

<!-- M O D A L    P A R A     V A L I D A R    L A    C O N T R A S E Ñ A    D E L    P A D R e -->  
<div class="modal fade" id="validar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background:#04774D">
        <a style="color:#FFFFFF"><h4>FIRMA DEL PADRE O TUTOR</h4></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('Directora/validartutor'); ?>
      <div class="modal-body">
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL NIÑO:</label>
          <div class="col-md-6">
            <input type="text" name="nnino" id="nnino" class="form-control" aria-describedby="emailHelp" disabled>
            <input type="text"name="nom" id="nom" hidden>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL PADRE O TUTOR:</label>
          <div class="col-md-6">
            <input type="text" name="idtutor" id="idtutor" class="form-control" aria-describedby="emailHelp" hidden>
            <input type="text" name="ntutor" id="ntutor" class="form-control" aria-describedby="emailHelp" disabled>
          </div>
        </div>
                <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">PERSONA AUTORIZADA:</label>
          <div class="col-md-6">
            <select name="autorizado" id="autorizado" class="form-control">
                <option value="">seleccione una opcion</option>
                
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-md-right">CONTRASEÑA DE CONFIRMACIÓN:</label>
          <div class="col-md-6">
            <input type="password" name="pass" id="pass" class="form-control" aria-describedby="emailHelp">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Aceptar</button>
      </div>
    </form>
  </div>
</div>
</div>

<script>$(document).ready( function () {
  $('#table_id').DataTable({
    responsive: true,
    language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
              }
            });
  $('#nombrenino').select2();
  $('#nombreninosD').select2();
   CKEDITOR.replace( 'cuerpocorreo',{

     toolbar: [
    
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', '-', 'Undo', 'Redo' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'] }
]
   } );
} );

/*    function clave(p) {

        //alert(p);
        $.ajax({
            url: "<?php echo site_url(); ?>Filtro/datofirmapadre",
            method: "POST",
            data: {
                'idinc': p
            },
            dataType: 'json',
            success: function(data) {
                //console.log(data);
                $('#nom').val(data[0].Id_incidencia);
                $('#nnino').val(data[0].Nombre_ninio);
                $('#idtutor').val(data[0].Id_tutores);
                $('#ntutor').val(data[0].Nombre_tutor);
                $('#validar').modal('show');
            },
            error: function(error) {
                console.log(error);
            }
        })

      }*/
    </script>

<!-- <script>
  $(document).ready(function(){
    $(document).on('click', '.modificar', function(){
      var id=$(this).val();
      var first=$('#fechabas').text();
      $('#modificar').modal('show');
      $('#nom').val(first);

    });
  });

</script>  -->

<script type="text/javascript">
//// E LI MI NAR  I N C I D E N C I A 
function eliminar(hi) {

        //alert(hi); 
        var confirmacion = confirm('Seguro que desea eliminar un registro?');
        if (confirmacion === true) {
          $.ajax({
            url: "<?php echo site_url(); ?>Directora/eliminarincidencia",
            method: "POST",
            data: {
              'id': hi
            },
            dataType: 'json',
            success: function(data) {
              if (data == true) {
                        //alert("Se elimino exitosamente");
                        location.reload();

                      } else {
                        alert("hubo un error al eliminar");
                      }
                    },
                    error: function(error) {
                      alert("no se puede eliminar por que existe una cita registrada con esta incidencia");
                    }
                  })

        }
      }
//////////// M O D I F I C A R    I N C I D E N C I A 
function modificar(p) {

    //alert(p);

    $.ajax({
      url: "<?php echo site_url(); ?>Directora/modificarincidencia",
      method: "POST",
      data: {
        'idinc': p
      },
      dataType: 'json',
      success: function(data) {
        console.log(data);
        $('#id').val(data[0][0].Id_incidencia);
        $('#obsrD').val(data[0][0].Observacion);
        $('#salonD').val(data[0][0].FK_salon);
        $('#tipoincidenciaD').val(data[0][0].FK_tipos);
    // $('#incidenciaD').val(data[0][0].FK_tipos);
    $('#citaD').val(data[0][0].Cita);

    $("#nombreninosD").html('');
    data[1].forEach(function(e) {

      $("#nombreninosD").append("<option value='" + e.Id_ninio + "'>" + e.Nombre_ninio + "</option>");
    });
    $("#nombreninosD").val(data[0][0].Id_ninio);

    $("#incidenciaD").html('');
    data[2].forEach(function(e) {
      $("#incidenciaD").append("<option value='" + e.Id_catalogo_incidencias + "'>" + e.Nombre_incidencia + "</option>");
    });
    $('#incidenciaD').val(data[0][0].Id_catalogo_incidencias);


    $('#modificar').modal('show');
  },
  error: function(error) {
    console.log(error);
  }
})

  }
</script>
<!-- consulta para mandar el nombre de los ninos  A G R E G A R  I N C I D E N C I A-->
<script type="text/javascript">
  $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#salon").change(function() {
          console.log("change option");
          $("#salon option:selected").each(function() {
            FK_salon = $('#salon').val();
                // if(FK_salon!=''){

                  $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreninos",
                    method: "POST",
                    data: {
                      FK_salon: FK_salon
                    },
                    success: function(data) {
                      $('#nombrenino').html(data);
                      console.log(data);
                    }
                  })
                });
        });
      });
    </script>
    <!-- consulta para mandar el nombre de los ninos  M O D I F I C A R  I N C I D E N C I A-->
    <script type="text/javascript">
      $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#salonD").change(function() {
          console.log("change option");
          $("#salonD option:selected").each(function() {
            FK_salon = $('#salonD').val();
                // if(FK_salon!=''){
                  $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreninos",
                    method: "POST",
                    data: {
                      FK_salon: FK_salon
                    },
                    success: function(data) {
                      $('#nombreninosD').html(data);
            
                       // $('#nombreninos').html(data);
                       // console.log(data);
                     }
                   })
                });
        });
      });
    </script>             
    <!-- hace que la incidencia sea dependiente del select tipo de incidencia,  A G R E G A R  I N C I D E N C I A  -->            
    <script type="text/javascript">
      $(document).ready(function() {
                //console.log("sdfgh");                    
                $("#tipoincidencia").change(function() {
                  console.log("change option");
                  $("#tipoincidencia option:selected").each(function() {
                    FK_tipos = $('#tipoincidencia').val();
                        // if(FK_salon!=''){

                          $.ajax({
                            url: "<?php echo site_url(); ?>ComboDependet/nombreincidencias",
                            method: "POST",
                            data: {
                              FK_tipos: FK_tipos
                            },
                            success: function(data) {
                              $('#incidencia').html(data);
                              console.log(FK_tipos);
                            }


                          })

                        });
                });
              });
            </script>

            <!-- hace que la incidencia sea dependiente del select tipo de incidencia,  E D I T A R   I N C I D E N C I A  -->
            <script type="text/javascript">
              $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#tipoincidenciaD").change(function() {
          console.log("change option");
          $("#tipoincidenciaD option:selected").each(function() {
            FK_tipos = $('#tipoincidenciaD').val();
                // if(FK_salon!=''){

                  $.ajax({
                    url: "<?php echo site_url(); ?>ComboDependet/nombreincidencias",
                    method: "POST",
                    data: {
                      FK_tipos: FK_tipos
                    },
                    success: function(data) {
                      $('#incidenciaD').html(data);
                      console.log(FK_tipos);
                    }


                  })

                });
        });
      });
    </script>      
<!--     <script>
   new FroalaEditor('textarea#cuerpocorreoss',{
    toolbarButtons:[['undo','redo','bold','italic','formatUL']], 
 pluginsEnabled:["lists"]
   })
 </script>  --> 
 <!--Boton de agregar cita y clave del padre o tutor-->        
 <script type="text/javascript">
   function clave(p) {
         //alert(p);
         $.ajax({
           url: "<?php echo site_url(); ?>Directora/datofirmapadre",
           method: "POST",
           data: {
             'idinc': p
           },
           dataType: 'json',
           success: function(data) {
             console.log(data);
             $('#nom').val(data[0][0].Id_incidencia);
             $('#nnino').val(data[0][0].Nombre_ninio);
             $('#idtutor').val(data[0][0].Id_tutores);
             $('#ntutor').val(data[0][0].Nombre_tutor);
                   $("#autorizado").html('');
    data[1].forEach(function(e) {
      $("#autorizado").append("<option value=''> SELECCIONE UNA OPCIÓN</option>");
        $("#autorizado").append("<option value='"+e.Idautorizado+"'>" + e.Nombre_autorizado + "</option>");
        $("#autorizado").append("<option value='"+e.Idautorizado1+"'>" + e.Nombre_autorizado1 + "</option>");
        $("#autorizado").append("<option value='"+e.Idautorizado2+"'>" + e.Nombre_autorizado2 + "</option>");
    });
    //$('#autorizado').val(data[1][0].Nombre_autorizado);
            //$('#autorizado').val(data[0].Nombre_autorizado);
             $('#validar').modal('show');
           },
           error: function(error) {
             console.log(error);
           }
         })

       }

       function Altacita(pi) {
         //alert(pi);
         $.ajax({
           url: "<?php echo site_url(); ?>Directora/Datoscita",
           method: "POST",
           data: {
             'idinc': pi
           },
           dataType: 'json',
           success: function(data) {
             console.log(data);
             $('#idd').val(data[0].Id_incidencia);
             $('#idtec').val(data[0].Nombre_tecnico);
             $('#fecha').val(data[0].FechaBaseCita);
             $('#agregarcita').modal('show');
           },
           error: function(error) {
             console.log(error);
           }
         })

       }
    //ESTA FUNCION VA A REVISAR LA INCIDENCIA Y VA A ENVIAR EL CORREO AL PADRE O TUTOR 
    function botonrevisar(p) {

        //alert(p);


        $.ajax({
          url: "<?php echo site_url(); ?>Directora/enviarincidencia",
          method: "POST",
          data: {
            'idinc': p
          },
          dataType: 'json',
          success: function(data) {
            //console.log(data);
            $('#idER').val(data[0].Id_incidencia);
            $('#obsrER').val(data[0].Observacion);
           // $('#nombreninioR').val(data[0].Nombre_ninio);
            //$('#nombretutorR').val(data[0].Nombre_tutor);
            //$('#nombreincR').val(data[0].Nombre_incidencia);
            //$('#nombretipoR').val(data[0].Nombre_tipo_inc);
            //$('#correoR').val(data[0].email);
            $('#botonrevisar').modal('show');

          },
          error: function(error) {
            console.log(error);
          }
        })

      }
      function botonenviar(p) {

        //alert(p);


        $.ajax({
          url: "<?php echo site_url(); ?>Directora/enviarincidencia",
          method: "POST",
          data: {
            'idinc': p
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $('#idE').val(data[0].Id_incidencia);
            $('#obsrE').val(data[0].Observacion);
            $('#asunto').val('NOTIFICACIÓN DE INCIDENCIA EN LA GUARDERIA #1 DEL IMSS');
            //$('#cuerpocorreo').val('Estimado c. <b>'+ data[0].Nombre_tutor + '</b> se le informa que su hijo <b>' + data[0].Nombre_ninio+'</b>');
            CKEDITOR.instances['cuerpocorreo'].setData('Estimado c. <b>'+ data[0].Nombre_tutor + '</b> se le informa que su hijo <b>' + data[0].Nombre_ninio+'</b> tuvo la siguiente incidencia');
            $('#nombreninio').val(data[0].Nombre_ninio);
            $('#nombretutor').val(data[0].Nombre_tutor);
            $('#nombreinc').val(data[0].Nombre_incidencia);
            $('#nombretipo').val(data[0].Nombre_tipo_inc);
            $('#correo').val(data[0].email);
            $('#botonenviarcorreo').modal('show');
          },
          error: function(error) {
            console.log(error);
          }
        })

      }
    </script>

    <script type="text/javascript">   
      $(document).ready(function() {   
            //console.log("sdfgh");                    
            $("#tipoincidencia").change(function() {
              //console.log("change option");
              $("#tipoincidencia option:selected").each(function() {
                FK_tipos = $('#tipoincidencia').val();
                //console.log(FK_tipos);
                if (FK_tipos == '2') {
                  $("#Cita").hide();
                } else {
                  $("#Cita").show();
                } 
              });
            });
          });
        </script>
