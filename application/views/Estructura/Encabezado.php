                        <!DOCTYPE html>
<html lang="es" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="../assets/css/app.css" rel="stylesheet">
    <meta charset="utf-8">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script> -->
    <script src="<?php echo base_url();?>assets/js/431bootstrap.min.js"></script>


    <title> <?php echo $titulo ?> | Encabezado</title>

</head>
<body >
<nav class="navbar navbar-expand-lg container-fluid" style="background-color: #EAEDED;" style="color: black">
    <div class="container"><img src="<?php echo base_url();?>/Imagenes/imssblack.png" height="70" width="70">
        <b class="navbar-brand" style="color: black" href="#">Guarderia 001 IMSS Acapulco</b>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsibg-darkve" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><img src="<?php echo base_url();?>/Imagenes/menu.png"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsibg-darkve">
            <ul class="navbar-nav ml-auto">
                <?php if($this->session->userdata('login')){  ?>

                <h class="text-center p-1">
                    <b><a><?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?> </a></b><br> <a><?php echo $this->session->userdata('tipo_trabajador') ?> </a>
                </h>
<div class="btn-group" style="width:160px; height:34px">
  <button type="button" class="btn btn-outline-success" style="width:120px; height:35px"><a style="color: black" href="<?php echo base_url(); ?>login/cerrar_sesion">Cerrar Sesión</a></button>
  <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:30px; height:35px">
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="<?php echo base_url(); ?>Home/contrasena">Cambiar contraseña</a>
  </div>
</div>



                </div>
                <?php } else{ ?>
                <div>
                    <button type="button" class="btn btn-outline-success  ">
                        <a style="color: black" href="<?php echo base_url(); ?>Home/index">Inicio
                            <span class="sr-only">(current)</span></a>
                    </button>

                    <button type="button" class="btn btn-outline-success">
                        <a style="color: black" href="<?php echo base_url(); ?>login/iniciar_sesion">Iniciar sesión</a>
                    </button>
                </div>

                <?php } ?>
            </ul>
        </div>
    </div>
</nav>


    <?php if ($this->session->flashdata('alert') == null): ?>

    <?php endif ?>
    <?php if ($this->session->flashdata('alert') != null): ?>
      <div class="<?php  echo $this->session->flashdata('color'); ?>">
        <center><h5><?php  echo $this->session->flashdata('alert'); ?> </h5></center>
      </div>
    <?php endif ?>
 