<?php    
if($this->session->userdata('tipo_trabajador')!="ENFERMERA/NUTRIOLOGO"){//echo "solo el admin tiene acceso aqui xD";
     echo "<script>
                window.location= 'http://localhost/IncidenciasGuarderia/roles/roll'
    </script>";

//header("location:".base_url());

}
?>
<?php
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<nav class="navbar navbar-expand-sm" style="background-color: #2E8B57;">

    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Enfermera_Nutriologo/" style="color: #FFFFFF">Inicio</a>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
                Incidencias
            </a>
            <div class="dropdown-menu">

                <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Incidencias">Mostrar Todo</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Incidencias_Atendidas">Atendidas</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Incidencias_Pendientes">Pendientes</a>
            </div>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
                Citas
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Citas">Mostrar Todo</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Citas_Atendidas">Atendidas</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Citas_Pendientes">Pendientes</a>
            </div>
        </li>

    </ul>
</nav>
<br>
<h3>
<center>
    <?php echo $nombre ?>
</center>
</h3>
      <?php if ($this->session->flashdata('acceso') == null): ?>
    <?php endif ?>
    <?php if ($this->session->flashdata('acceso') != null): ?>
      <div class="alert alert-danger"><h5><?php  echo $this->session->flashdata('acceso'); ?> </h5></div>
    <?php endif ?>
<?php echo $output ?>




<?php foreach($js_files as $file): ?>
<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<br><br>