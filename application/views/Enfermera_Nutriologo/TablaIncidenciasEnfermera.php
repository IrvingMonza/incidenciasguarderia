<?php    
if($this->session->userdata('tipo_trabajador')!="ENFERMERA/NUTRIOLOGO"){//echo "solo el admin tiene acceso aqui xD";
echo "<script>
window.location= '".site_url()."roles/roll'
</script>";

//header("location:".base_url());

}
?>
<!--<link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" >-->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
<!-- <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
<nav class="navbar navbar-expand-lg" style="background-color: #2E8B57;">
  <div class="navbar navbar-expand-sm">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsibg-darkverrr" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><img src="<?php echo base_url();?>/Imagenes/menu.png"></span>
    </button>
    <!-- Links -->
    <div class="collapse navbar-collapse" id="navbarResponsibg-darkverrr">
      <ul class="navbar-nav ml-auto" >
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>Enfermera_Nutriologo/" style="color: #FFFFFF">Inicio</a>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
            Incidencias
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Incidencias">Mostrar Todo</a>
            <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Incidencias_Atendidas">Atendidas</a>
            <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Incidencias_Pendientes">Pendientes</a>
          </div>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
            Citas
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Citas">Mostrar Todo</a>
            <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Citas_Atendidas">Atendidas</a>
            <a class="dropdown-item" href="<?php echo base_url(); ?>Enfermera_Nutriologo/CRUD_Citas_Pendientes">Pendientes</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
<br>
<h3>
  <center>
    <?php echo $seccion ?>  
  </center></h3>
  <?php if ($this->session->flashdata('alerta') == null): ?>
    
  <?php endif ?>
  <?php if ($this->session->flashdata('alerta') != null): ?>
    <div class="alert alert-success"><h5><?php  echo $this->session->flashdata('alerta'); ?> </h5></div>
  <?php endif ?>
  <div id="tablaaa" class="table-responsive">
    <table id="table_id" class="table table-striped table-bordered" style="width:100%"> 
      <thead>
        <tr>

          <th>FECHA DE INCIDENCIA</th>
          <th>SALÓN</th>
          <th>NOMBRE DEL NIÑO</th>
          <th>TIPO DE INCIDENCIA</th>
          <th>INCIDENCIA</th>
          <th>OBSERVACIÓN</th>
          <th>CITA</th>
          <th>NOMBRE DE QUIEN REGISTRA</th>
          <th>FECHA DE REVISIÓN</th>
          <th>NOMBRE DE QUIEN REVISO</th>
          <th>ESTADO DE LA INCIDENCIA</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach ($datos as $key => $value): ?>
        <tr>

         <td><span id="fechabas"><?php echo $value->FechaBase; ?></span></td>
         <td><?php echo $value->Nombre_salon; ?></td>
         <td><?php echo $value->Nombre_ninio; ?></td>
         <?php     if($value->Nombre_tipo_inc=="CONDUCTA"){ ?>
         <td style="background-color:#FF4646;" style="color: red"> <?php echo $value->Nombre_tipo_inc;?> </td>
         <?php  } ?>
         <?php if($value->Nombre_tipo_inc=="LOGRO"){ ?>
         <td style="background-color:#2DFF41;"> <?php echo $value->Nombre_tipo_inc;?> </td>
         <?php  } ?>
         <?php if($value->Nombre_tipo_inc=="INCIDENCIA"){ ?>
         <td style="background-color:#FFF639;"> <?php echo $value->Nombre_tipo_inc;?> </td>
         <?php  } ?>
         <?php if($value->Nombre_tipo_inc=="CITA"){ ?>
         <td style="background-color:#58ABFF"> <?php echo $value->Nombre_tipo_inc;?> </td>
         <?php  } ?>
       <td><?php echo $value->Nombre_incidencia; ?></td>
       <td><?php echo $value->Observacion; ?></td>
       <td><?php echo $value->Cita; ?></td>
       <td><?php echo $value->Nombre_docente; ?></td>
       <td><?php echo $value->FechaAtencion; ?></td>  
       <td><?php echo $value->Nombre_tecnico; ?></td>
       <td><?php echo $value->EstadoIncidencia; ?></td>
                
       <td>
        <button type="button" title="Revisar incidencia" id="modifi" onclick="modificar(<?php echo $value->Id_incidencia; ?>)"><img src="<?php echo base_url();?>/Imagenes/revisar.png" width="26"></button>
      </td>
    </tr>
  <?php endforeach ?> 
</tbody>
</table>
</div>

<!-- M O D A L    P A R A     M O D I F I C A R    U N    R E G I S T R O -->  
<?php echo form_open('Enfermera_Nutriologo/modifinc'); ?>
<div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#04774D">
                <font color="white">
                    <h4>REVISAR INCIDENCIA</h4>
                </font>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <input type="text" name="id" id="id" hidden>
                    <label class="col-sm-4 col-form-label text-md-right">OBSERVACIÓN:</label>
                    <div class="col-md-6">
                        <textarea rows="4" cols="50" name="obsr" id="obsr" class="form-control" aria-describedby="emailHelp" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea>
                    </div>
                </div>
                <input type="text" name="nombretutor" id="nombretutor" hidden>
                <input type="text" name="nombreninio" id="nombreninio" hidden>
                <input type="text" name="nombreinc" id="nombreinc" hidden>
                <input type="text" name="nombretipo" id="nombretipo" hidden>
                <input type="text" name="correo" id="correo" hidden>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right">NOMBRE DEL TÉCNICO:</label>
                    <div class="col-md-6">
                        <input type="text" name="obsr" value="<?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?>" id="obsr" class="form-control" aria-describedby="emailHelp" disabled>
                        <input name="Idtecnico" type="hidden" value="<?php echo $this->session->userdata('Id_trabajadores') ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right">ESTADO DE LA INCIDENCIA:</label>
                    <div class="col-md-6">
                        <select class="form-control" name="EstadoInc" id="EstInc">
                            <option value="ATENDIDA">ATENDIDA</option>
                            <option value="PENDIENTE">PENDIENTE</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Revisar</button>
            </div>
        </div>
    </div>
</div>
</form>


<script>$(document).ready( function () {
  $('#table_id').DataTable({
            responsive: true,
            language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
} );
</script>

<script>
  $(document).ready(function(){
    $(document).on('click', '.modificar', function(){
      var id=$(this).val();
      var first=$('#fechabas').text();


      $('#modificar').modal('show');
      $('#nom').val(first);

    });
  });
</script> 

<script type="text/javascript">


  function modificar(p){
//alert(p);
$.ajax({
  url:"<?php echo site_url(); ?>Enfermera_Nutriologo/modificarincidencia",
  method:"POST",
  data:{'idinc':p},
  dataType:'json',
  success:function(data)
  {
    console.log(data);
    $('#id').val(data[0].Id_incidencia);
    $('#obsr').val(data[0].Observacion);
    $('#nombreninio').val(data[0].Nombre_ninio);
    $('#nombretutor').val(data[0].Nombre_tutor);
    $('#nombreinc').val(data[0].Nombre_incidencia);
    $('#nombretipo').val(data[0].Nombre_tipo_inc);
    $('#correo').val(data[0].email);
    $('#modificar').modal('show');
  },
  error: function(error) {
    console.log(error);
  }
})
}

</script>

<script type="text/javascript">   
  $(document).ready(function() {   
            //console.log("sdfgh");                    
            $("#salon").change(function() {
              console.log("change option");
              $("#salon option:selected").each(function() {
                FK_salon = $('#salon').val();
                   // if(FK_salon!=''){
                    $.ajax({
                      url:"<?php echo site_url(); ?>ComboDependet/nombreninos",
                      method:"POST",
                      data:{FK_salon:FK_salon},
                      success:function(data)
                      {
                        $('#nombrenino').html(data);
                        console.log(data);
                      }
                    })
                  });
            });
          });
        </script>
        <script type="text/javascript">   
          $(document).ready(function() {   
            //console.log("sdfgh");                    
            $("#tipoincidencia").change(function() {
              console.log("change option");
              $("#tipoincidencia option:selected").each(function() {
                FK_tipos = $('#tipoincidencia').val();
                   // if(FK_salon!=''){
                    $.ajax({
                      url:"<?php echo site_url(); ?>ComboDependet/nombreincidencias",
                      method:"POST",
                      data:{FK_tipos:FK_tipos},
                      success:function(data)
                      {
                        $('#incidencia').html(data);
                        console.log(FK_tipos);
                      }
                    })

                  });
            });
          });
        </script>