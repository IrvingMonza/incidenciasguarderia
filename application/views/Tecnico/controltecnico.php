<?php    
if($this->session->userdata('tipo_trabajador') != "TÉCNICO/EDUCADORA")
{
 echo "<script>
 window.location= '".site_url()."roles/roll'
 </script>";
}
?>
  <!-- 
   <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="css/estilos.css"> -->
  <!-- <link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" > -->
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css" >
  <!-- <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
  <script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/js/dataTables.bootstrap4.min.js"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<nav class="navbar navbar-expand-sm" style="background-color: #2E8B57;">

    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Tecnico/" style="color: #FFFFFF">Inicio</a>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
                Incidencias
            </a>
            <div class="dropdown-menu">

                <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Incidencias">Mostrar Todo</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Incidencias_Atendidas">Atendidas</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Incidencias_Pendientes">Pendientes</a>
            </div>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
                Citas
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Citas">Mostrar Todo</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Citas_Atendidas">Atendidas</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Citas_Pendientes">Pendientes</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Tecnico/Control" style="color: #FFFFFF">Control</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Tecnico/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
        </li>

    </ul>
</nav>
  <br>
  <h3>
    <center>
      <?php echo $seccion ?>  
    </center>
  </h3>
  <br>
<div class="form-group row">
    <label for="pass" class="col-md-1 col-form-label text-md-right">FECHA :</label>
    <div class="col-md-6">
        <input type="date" id="fecha1">
        ENTRE
        <input type="date" id="fecha2">
    </div>
</div>
  <div class="form-group row">
      <label for="pass" class="col-md-1 col-form-label text-md-right">SALÓN :</label>
      <div class="col-md-6">
          <select class="form-control" name="salon" id="salon" required>
              <option value="LACTANTES A">SELECCIONE UN SALÓN</option>
              <?php foreach ($salon as $key => $value): ?>
              <option value="<?php echo $value->Id_salon; ?>">
                  <?php echo $value->Nombre_salon; ?>
              </option>
              <?php endforeach ?>
          </select>
      </div>
  </div>
<br>
<div id="tablaaa">
    <table id="table_id" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE DEL NIÑO</th>
                <th>LOGRO</th>
                <th>INCIDENCIA</th>
                <th>CONDUCTA</th>

                <th>CITA</th>
            </tr>
        </thead>
        <tbody id="ninos">

        </tbody>
    </table>
</div>
<script>$(document).ready( function () {
  $('#table_id').DataTable({
            responsive: true,
            language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
} );

</script>

<script type="text/javascript">
    $(document).ready(function() {
        //console.log("sdfgh");                    
        $("#salon").change(function() {
            console.log("change option");
            $("#salon option:selected").each(function() {
                FK_salon = $('#salon').val();
                fecha1 = $('#fecha1').val();
                fecha2 = $('#fecha2').val();
                console.log(FK_salon);
                console.log(fecha1);
                console.log(fecha2);
                $.ajax({
                    url: "<?php echo site_url(); ?>Tecnico/nombres_control",
                    method: "POST",
                    data: {
                        'FK_salon': FK_salon,
                        'fecha1': fecha1,
                        'fecha2': fecha2
                    },
                    dataType: 'json',
                    success: function(data) {
                        $('#ninos').html('');
                        data.forEach(function(element) {
                            $('#ninos').append('<tr>' +
                                '<td>' + element.Id_ninio + '</td>' +
                                '<td>' + element.NOMBRE_NIÑO + '</td>' +
                                '<td>' + element.logro + '</td>' +
                                '<td>' + element.incidencia + '</td>' +
                                '<td>' + element.conducta + '</td>' +
                                '<td>' + element.cita + '</td>' +
                                '</tr>');
                            //console.log(element);
                        });
                    }
                })
            });
        });
    });
</script>