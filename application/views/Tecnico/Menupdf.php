<?php    
if($this->session->userdata('tipo_trabajador')!="TÉCNICO/EDUCADORA"){//echo "solo el admin tiene acceso aqui xD";
 echo "<script>
 window.location= '".site_url()."roles/roll'
 </script>";

//header("location:".base_url());

}
?>
<!--<link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" >-->
<link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" >
<!--<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<style>
.sa {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #04774D;
}

li {
  float: left;
}

li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
  background-color: #D1D5D2;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
<nav class="navbar navbar-expand-sm" style="background-color: #2E8B57;">

  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Tecnico/" style="color: #FFFFFF">Inicio</a>
    </li>
    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Incidencias
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Incidencias">Mostrar Todo</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Incidencias_Atendidas">Atendidas</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Incidencias_Pendientes">Pendientes</a>
      </div>
    </li>
    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown" style="color: #FFFFFF">
        Citas
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Citas">Mostrar Todo</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Citas_Atendidas">Atendidas</a>
        <a class="dropdown-item" href="<?php echo base_url(); ?>Tecnico/CRUD_Citas_Pendientes">Pendientes</a>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Tecnico/Control" style="color: #FFFFFF">Control</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url(); ?>Tecnico/CRUD_Menupdf" style="color: #FFFFFF">PDF</a>
    </li>

  </ul>
</nav>
<br>
<center><h4>INFORMACIÓN SOBRE LA ATENCIÓN DE LOS NIÑOS</h4></center>

<div id="tablaaa" class="table-responsive">
    <table id="table_id" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>SALÓN</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
           <?php foreach ($datosalones as $key => $value): ?>
            <tr>
                <td><?php echo $value->Nombre_salon; ?></td>
                <td><button type="button" title="Generar pdf del salón <?php echo $value->Nombre_salon; ?> "  id="pdff"><a target="_blank" href="<?php echo base_url('Tecnico/pdf/'.$value->Id_salon);?>"><img src="<?php echo base_url();?>/Imagenes/pdf.png" width="26"></a></button></td>
            </tr>
            
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<script>$(document).ready( function () {
  $('#table_id').DataTable({
            responsive: true,
            language: {
                url: '<?php echo base_url();?>/assets/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
} );
</script>



