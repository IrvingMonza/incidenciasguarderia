<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome2.min.css">
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div style="background-color:#04774D" class="card-header"> <img src="<?php echo base_url();?>Imagenes/logo_imss.jpg" height="50" width="50">
                        <font color="white" size="5"> CAMBIAR CONTRASEÑA </font>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>/login/ActualizarContrasena" aria-label="Register" method="POST">
                        	<!-- PARTE1 -->
                            <div class="form-group row">
                                <label for="nickname" class="col-sm-4 col-form-label text-md-right">Nombre de Usuario :</label>
                                <div class="col-md-6">
                                    <input type="text" name="idtrabajador" id="idtrabajador" value="<?php echo $this->session->userdata('Id_trabajadores')?>" hidden>
                                    <input type="text" class="form-control" id="nickname" name="usuario" aria-describedby="emailHelp" value="<?php echo $this->session->userdata('Nombre').' '.$this->session->userdata('APaterno').' '.$this->session->userdata('AMaterno')?>" disabled>
                                </div>
                            </div>
                            <!-- PARTE1 -->
                            <div class="form-group row">
                                <label for="pass" class="col-md-4 col-form-label text-md-right">Contraseña Anterior:</label>
                                <div class="col-md-6 input-group-append">
                                    <input id="pass" type="password" class="form-control" placeholder="&#128272;Ingrese Su Actual Contraseña" name="contraseña" required>
                                    <div class="input-group-append">
                                       <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                                   </div>
                               </div>
                           </div>
                           <!-- PARTE1 -->
                           <div class="form-group row">
                            <label for="pass" class="col-md-4 col-form-label text-md-right">Contraseña Anterior:</label>
                            <div class="col-md-6 input-group-append">
                                <input id="pass2" type="password" class="form-control" placeholder="&#128272;Ingrese Su Nueva Contraseña" name="nuevacontraseña" required>
                                <div class="input-group-append">
                                   <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPasswords()"> <span class="fa fa-eye-slash icon"></span> </button>
                               </div>
                           </div>
                       </div>
                       <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Cambiar Contraseña
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <a href="<?php echo base_url(); ?>/roles/rolles" style="color: white">Cancelar</a> 
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<br>
<!--  nota: Tenga mucho cuidado al cambiar su contraseña -->
</main>


<script type="text/javascript">
    function mostrarPassword() {
        var cambio = document.getElementById("pass");
        if (cambio.type == "password") {
            cambio.type = "text";
            $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        } else {
            cambio.type = "password";
            $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }
    }

    $(document).ready(function() {
        //CheckBox mostrar contraseña
        $('#ShowPassword').click(function() {
            $('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
        });
    });
</script>
<script type="text/javascript">
    function mostrarPasswords() {
        var cambio = document.getElementById("pass2");
        if (cambio.type == "password") {
            cambio.type = "text";
            $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        } else {
            cambio.type = "password";
            $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }
    }

    $(document).ready(function() {
        //CheckBox mostrar contraseña
        $('#ShowPassword').click(function() {
            $('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
        });
    });
</script>
