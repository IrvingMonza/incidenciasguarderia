<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filtro extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->principal((object)array('output' => '' , 'data' => '' , 'js_files' => array() , 'css_files' => array()));
		//$this ->load->view('Filtro/inicio');
	}

	public function principal($output = null, $data = null)
	{ 
		if ($data['nombre'] == null) {
			$data['nombre'] = "";
		} else {
			//vacio
		}
		$data['titulo'] = "Filtro";
		$this->load->view('Estructura/Encabezado', $data);
		$this->load->view('Filtro/Principal', $output);
        $this->load->view('Filtro/Inicio');
	}

	public function CRUD_Incidencias()
	{
		$this ->load->model('incidenciasfiltro');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasfiltro->datos_incidencias();
		$data['salon'] = $this->incidenciasfiltro->salon();
		$data['tipos'] = $this->incidenciasfiltro->tipo_salon();
		$data['titulo'] = "Filtro";
		$this->load->view('Estructura/Encabezado', $data);
		$this->load->view('Filtro/TablaIncidenciasFiltro', $data);
		//$this->load->view('Estructura/PiePagina');
	}
	public function guardarincidencia()
	{
		$data = $this->input->post();
		$arrayName = array(
			'FechaBase' => date('Y-m-d H:i:s'),
			'Observacion' => $data['observaciones'],
			'EstadoIncidencia' => 'PENDIENTE',
			'FK_ninio' => $data['nombrenino'],
			'FK_docente' => $data['Iddocente'],
			'FK_incidencias' => $data['incidencia'],
			'Cita' => $data['cita']
		);
		$this->db->insert('incidencias', $arrayName);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO AGREGADO EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Filtro/CRUD_Incidencias'
		</script>";
	} 

	public function modificarincidencia()
	{
		$post = $this->input->post();
		$this->load->model('incidencias');
		$fila = $this->incidencias->modificar($post['idinc']);
		echo json_encode($fila);
	}
    
    public function datofirmapadre(){
		$post = $this->input->post();
		$this->load->model('incidencias');
		$fila = $this->incidencias->datosfirma($post['idinc']);
		echo json_encode($fila);
	} 

	public function Datoscita(){
		$post = $this->input->post();
		$this->load->model('incidencias');
		$fila = $this->incidencias->citaAlta($post['idinc']);
		echo json_encode($fila);
	} 

	public function Altacita()
	{
		$data = $this->input->post();
		$this->db->delete('citas', array('FK_incidencia' => $data['id']));
		$arrayName = array(
			'FechaBaseCita' => $data['fechabase'],
			'FK_incidencia' => $data['id'],
			'EstadoCita' => 'PENDIENTE'
		);
		$this->db->insert('citas', $arrayName);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'CITA AGREGADA EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Filtro/CRUD_Incidencias'
		</script>";
	} 

	public function clavepadre()
	{
		$data = $this->input->post();
		$arrayMod = array(
			'Observacion' => $data['obsr']
		);
		$this->db->where('Id_incidencia', $data['nom']);
		$this->db->update('incidencias', $arrayMod);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Filtro/CRUD_Incidencias'
		</script>";
	}

	public function validartutor()
	{ 

        $post = $this->input->post();
       //l var_dump($post); die();
        if(isset($post) && !empty($post)){
        $idInc = $post['nom'];
        $idtutor = $post['idtutor'];
        $contraseña = $post['pass'];
        $this->load->model('user');
        $fila = $this->user->getTutor($idtutor,$contraseña, $idautorizado);
        if ($fila != null) {

        $arrayfirma = array(
		    'FirmaTutor' => 'ENTERADO'
		);
		$this->db->where('Id_incidencia', $idInc);
		$this->db->update('incidencias', $arrayfirma);
    		$this->session->set_flashdata('color', 'alert alert-success');
		    $this->session->set_flashdata('alerta', 'Usuario y contraseñas correctas');
            echo "<script>
            window.location= '".site_url()."Filtro/CRUD_Incidencias'
            </script>"; 

        } else {
        	$this->session->set_flashdata('color', 'alert alert-danger');
    		$this->session->set_flashdata('alerta', 'Usuario y/o contraseñas incorrectas');
            echo "<script>
            window.location= '".site_url()."Filtro/CRUD_Incidencias'
            </script>";
        }
    }else{
    	        redirect('Filtro/CRUD_Incidencias');
    }
    }
    
    public function CRUD_Menupdf()
	{
		$this ->load->model('incidenciasfiltro');
	    //$this ->load->model('incidencias');
		$data['datosalones'] = $this->incidenciasfiltro->datos_salones();
		$data['salon'] = $this->incidenciasfiltro->salon();
		$data['tipos'] = $this->incidenciasfiltro->tipo_salon();
       // $data['seccion'] = "SECCIÓN DE INCIDENCIAS";
		$data2['titulo'] = "Filtro";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Filtro/Menupdf', $data);
	    //$this->load->view('Estructura/PiePagina');
	}
    /////////////////////////////////////
    public function pdf($grupo)
	{
		 
		if (isset($grupo) && is_numeric($grupo)  ){
			$this ->load->model('incidenciasfiltro');
	    //$this ->load->model('incidencias');
			$datos = $this->incidenciasfiltro->datos_incidencias_pdf($grupo);
			include(APPPATH."third_party/vendor/autoload.php");
            if ($datos != null)
            {
			$mpdf = new \Mpdf\Mpdf([
			'margin_top' => '35',
			'mode' => 'utf-8', //Codepage Values OR Codepage Value
			'format' => 'A4-L']);
        //$mpdf->AddPage('L');
			$mpdf->defaultheaderline=0;
			$mpdf->defaultfooterline=0;

			$mpdf->SetHeader('      <div class="img">
				<img src="imagenes/loimss.png" width="320">
				</div>
				<h1 class="text1"><b>Información al usuario de la atención a los niños(as)</b></h1>
				<h1 class="text2" ><b>Guardería No.001</b></h1>');
			$mpdf->SetFooter('<h1 align="right" ><font face="arial" size=1>DPES/CG/009/205</font></h1>
				{PAGENO}');
			$html = '<html>
			<head>
			<style>
			body{
				font-family:Arial, Helvetica, sans-serif;
			}
			.text1{
				font-weight:bolder;
				font-size:15px;
				width:100%;
				text-align:center;

			}
			.img{
				font-weight:bolder;
				font-size:12px;
				text-align:left;
			}
			.text2{
				font-weight:bolder;
				font-size:12px;
				text-align:left;
			}
			.Nota{
				font-weight:bolder;
				font-size:9px;
				text-align:left;
			}


			table {
				border-collapse: collapse;
			}

			table, th, td {
				border: 1px solid black;
				font-weight:bolder;
				font-size:12px;
			}
			</style>

			<body>
			<table class="tabla1" style="text-align: center" WIDTH="100%">
			<thead>
			<tr>
			<th width="8%" ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Fecha</th>
			<th width="8%" ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Sala</th>
			<th width="20%"  ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Nombre del niño(a)</th>
			<th width="24%" ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Descripción de logro o incidencia</th>
			<th width="30%" COLSPAN=2 bgcolor="#D9D9D9" style="color:#000000">Asegurado usuario o Persona autorizada</th>
			</tr>
			<tr bgcolor="#D9D9D9" style="color:#000000">
			<th width="20%" style="color:#000000" >Nombre</th>
			<th width="10%" style="color:#000000">Firma</th>  
			</tr>
			</thead>
			<tbody>';

			foreach ($datos as $key => $value)
			{
				$html.= '
				<tr>
				<td>'. $value->FechaBase .'</td>
				<td>'. $value->Nombre_salon .'</td>
				<td align="left">'. $value->Nombre_ninio .'</td>
				<td align="left" >'. $value->Observacion .'</td>
				<td align="left">'. $value->Nombre_tutor.'</td>
				<td></td>
				</tr>';
			}
			$html.='
			</tbody>
			</table>
			</body>
			</html>';

//  echo $html;          
			$mpdf->WriteHTML($html);
			$mpdf->Output('Anexo3 Información al Usuario.pdf','I');
			}else{
			$html='
			<!-- Versión compilada y comprimida del CSS de Bootstrap -->
			<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
			<br><br><br>
			<center>
			    <div class="alert alert-danger">AÚN NO HAY INCIDENCIAS REGISTRADAS</div>
			</center>
			';
      echo $html;
      echo "<script>
      setTimeout(function(){
            window.close();
        },2000);
      </script>";

    }
		}else{
			echo "<script>
			alert('valor no numerico');
			window.location= '".site_url()."Filtro/CRUD_Menupdf'
			</script>";

		}
	}
}