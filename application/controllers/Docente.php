<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Docente extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->principal((object)array('output' => '' , 'data' => '', 'js_files' => array(), 'css_files' => array()));
		$this ->load->view('Docente/inicio');
	}

	public function principal($output = null,$data=null)
	{ 
		if ($data['nombre'] == null) {
			$data['nombre']="";
		} else {
			//vacio
		}
		$data['titulo'] = "Docente";
		$this->load->view('Estructura/Encabezado', $data);
		$this->load->view('Docente/Principal', $output);
	}

	public function Semaforo($value, $row)
	{
		if ($row->TipoIncidencia == 'LOGRO') {
			return '<div  style="background-color:#02FE5B;">LOGRO</div>';
		} elseif ($row->TipoIncidencia == 'INCIDENCIA') {
			return '<div  style="background-color:#FFD200;">INCIDENCIA</div>';
		} elseif ($row->TipoIncidencia == 'FOMENTO A LA SALUD') {
			return '<div  style="background-color:#DA70D6;">FOMENTO A LA SALUD</div>';
		} elseif ($row->TipoIncidencia == 'CONDUCTA') {
			return '<div  style="background-color:#FF0000;">CONDUCTA</div>';
		} elseif ($row->TipoIncidencia == 'CITA') {
			return '<div  style="background-color:#019BFC">CITA</div>';
		}
	}

	function EnvioDatos(){
		$post_array = array(
			'Fecha' => $this->input->post('Fecha'),
			'Id_salon' => $this->input->post('Id_salon'),
			'Id_ninio' => $this->input->post('Id_ninio'),
			'TipoIncidencia' => $this->input->post('TipoIncidencia'),
			'Descripcion' => $this->input->post('Descripcion'),
			'Cita' => $this->input->post('Cita'),
			'ContraseñaDocente' => $this->input->post('ContraseñaDocente'),
		    //'ContraseñaTecnico' => 'SIN ASIGNAR',
			'EstadoCita' => 'PENDIENTE',
			'EstadoIncidencia' => 'PENDIENTE'
		);
         //Se inserta el array con los datos en la tabla incidencias
		return $this->db->insert('incidencias', $post_array);
	}

	public function CRUD_Incidencias()
	{
		$this ->load->model('incidencias');
		$data['datos'] = $this->incidencias->datos_incidencias();
		$data['salon'] = $this->incidencias->salon();
		$data['tipos'] = $this->incidencias->tipo_salon();
		$data2['titulo'] = "Docente";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Docente/TablaIncidenciasDocente', $data);
	}

	public function guardarincidencia(){
		$data = $this->input->post();
		if (!empty($data['cita']))
		{
			$arrayName = array(
				'FechaBase' => date('Y-m-d H:i:s'),
				'Observacion' => $data['obsr'],
				'EstadoIncidencia' => 'PENDIENTE',
				'FK_ninio' => $data['nombrenino'],
				'FK_docente' => $data['Iddocente'],
				'FK_incidencias' => $data['incidencia'],
				'Cita' => $data['cita'],
			);
		} else {
			$arrayName = array(
				'FechaBase' => date('Y-m-d H:i:s'),
				'Observacion' => $data['obsr'],
				'EstadoIncidencia' => 'PENDIENTE',
				'EstadoRevisadoIncidencia' => 'PENDIENTE',
				'FK_ninio' => $data['nombrenino'],
				'FK_docente' => $data['Iddocente'],
				'FK_incidencias' => $data['incidencia'],
				'Cita' => 'SIN CITA',
			);
		}
		$this->db->insert('incidencias', $arrayName);
		$this->session->set_flashdata('alerta', 'REGISTRO AGREGADO EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Docente/CRUD_Incidencias'
		</script>";
	} 

	public function eliminarincidencia()
	{
		$g = $this->input->post();
		$el = $this->db->delete('incidencias', array('Id_incidencia' => $g['id']));
		echo json_encode($el);
	} 

	public function modificarincidencia()
	{
		$post = $this->input->post();
		$this->load->model('incidencias');
		$fila = $this->incidencias->modificar($post['idinc']);
		echo json_encode($fila);
	} 	

	public function modifinc()
	{
		$data = $this->input->post();
		$arrayMod = array(
		'Observacion' => $data['obsr'],
		'Cita' => $data['citaM'],
		'FK_ninio' => $data['nombreninosM'],
		'FK_incidencias' => $data['incidenciaM']
            
		);
        
		$this->db->where('Id_incidencia', $data['nom']);
		$this->db->update('incidencias', $arrayMod);
		$this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Docente/CRUD_Incidencias'
		</script>";
	}
}