<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ComboDependet extends CI_Controller
{
	public function fillNombres()
	{
		$fieldSalaGrupo = $this->input->post('salon');
		if ($fieldSalaGrupo) {
			$this->load->model('dependen');
			$nombres = $this->dependen->getNombre($fieldSalaGrupo);
			return $nombres;  
		}
	}

	function nombreninos()
	{
		if ($this->input->post('FK_salon')) {
			$this->load->model('dependen');
			echo $this->dependen->getNombre($this->input->post('FK_salon'));
		}
	}

	function nombreincidencias()
	{
		if ($this->input->post('FK_tipos')) {
			$this->load->model('dependen');
			echo $this->dependen->getNombreincidencia($this->input->post('FK_tipos'));
		}
	}	
}