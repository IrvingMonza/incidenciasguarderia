<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class roles extends CI_Controller
{
    public function roll()
    {
        /*if ($this->session->userdata('tipo_trabajador') == "ADMINISTRADOR") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Administrador/');
        } else*/if ($this->session->userdata('tipo_trabajador') == "DOCENTE") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Docente/');
        } elseif ($this->session->userdata('tipo_trabajador') == "TÉCNICO/EDUCADORA") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Tecnico/');
        } elseif ($this->session->userdata('tipo_trabajador') == "FILTRO") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Filtro/');
        } elseif ($this->session->userdata('tipo_trabajador') == "DIRECTORA") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Directora/');
        } elseif($this->session->userdata('tipo_trabajador') == "ENFERMERA/NUTRIOLOGO") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Enfermera_Nutriologo/');
        } else {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url());
        }
    }

    public function rolles()
    {
        /*if ($this->session->userdata('tipo_trabajador') == "ADMINISTRADOR") {
            header("location:".base_url().'Administrador/');
        } else*/if ($this->session->userdata('tipo_trabajador') == "DOCENTE") {
            header("location:".base_url().'Docente/');
        } elseif ($this->session->userdata('tipo_trabajador') == "TÉCNICO/EDUCADORA") {
            header("location:".base_url().'Tecnico/');
        } elseif ($this->session->userdata('tipo_trabajador') == "FILTRO") {
            header("location:".base_url().'Filtro/');
        } elseif ($this->session->userdata('tipo_trabajador') == "DIRECTORA") {
            header("location:".base_url().'Directora/');
        } elseif($this->session->userdata('tipo_trabajador') == "ENFERMERA/NUTRIOLOGO") {
            header("location:".base_url().'Enfermera_Nutriologo/');
        } else {
            header("location:".base_url());
        }
    }
    public function rollesesion()
    {
        /*if ($this->session->userdata('tipo_trabajador') == "ADMINISTRADOR") {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url().'Administrador/');
        } else*/if ($this->session->userdata('tipo_trabajador') == "DOCENTE") {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url().'Docente/');
        } elseif ($this->session->userdata('tipo_trabajador') == "TÉCNICO/EDUCADORA") {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url().'Tecnico/');
        } elseif ($this->session->userdata('tipo_trabajador') == "FILTRO") {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url().'Filtro/');
        } elseif ($this->session->userdata('tipo_trabajador') == "DIRECTORA") {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url().'Directora/');
        } elseif($this->session->userdata('tipo_trabajador') == "ENFERMERA/NUTRIOLOGO") {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url().'Enfermera_Nutriologo/');
        } else {
            $this->session->set_flashdata('acceso', 'USTED YA INICIO SESIÓN');
            header("location:".base_url());
        }
    }

    public function rolescon()
    {
        /*if ($this->session->userdata('tipo_trabajador') == "ADMINISTRADOR") {
            $this->session->set_flashdata('acceso', 'NO TIENES ACCESO A LA PAGINA QUE INTENTASTE INGRESAR');
            header("location:".base_url().'Administrador/');
        } else*/if ($this->session->userdata('tipo_trabajador') == "DOCENTE") {
          $this->session->set_flashdata('color', 'alert alert-success');
          $this->session->set_flashdata('alert', 'Contraseña Cambiada Exitosamente!');
          header("location:".base_url().'Docente/');
      } elseif ($this->session->userdata('tipo_trabajador') == "TÉCNICO/EDUCADORA") {
          $this->session->set_flashdata('color', 'alert alert-success');
          $this->session->set_flashdata('alert', 'Contraseña Cambiada Exitosamente!');
          header("location:".base_url().'Tecnico/');
      } elseif ($this->session->userdata('tipo_trabajador') == "FILTRO") {
          $this->session->set_flashdata('color', 'alert alert-success');
          $this->session->set_flashdata('alert', 'Contraseña Cambiada Exitosamente!');
          header("location:".base_url().'Filtro/');
      } elseif ($this->session->userdata('tipo_trabajador') == "DIRECTORA") {
          $this->session->set_flashdata('color', 'alert alert-success');
          $this->session->set_flashdata('alert', 'Contraseña Cambiada Exitosamente!');
          header("location:".base_url().'Directora/');
      } elseif($this->session->userdata('tipo_trabajador') == "ENFERMERA/NUTRIOLOGO") {
          $this->session->set_flashdata('color', 'alert alert-success');
          $this->session->set_flashdata('alert', 'Contraseña Cambiada Exitosamente!');
          header("location:".base_url().'Enfermera_Nutriologo/');
      } else {
          $this->session->set_flashdata('color', 'alert alert-success');
          $this->session->set_flashdata('alert', 'Contraseña Cambiada Exitosamente!');
          header("location:".base_url());
      }
  }

}