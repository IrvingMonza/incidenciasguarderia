<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tecnico extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->principal((object)array('output' => '', 'data' => '', 'js_files' => array(), 'css_files' => array()));
		$this ->load->view('Tecnico/inicio');
	}

	public function principal($output = null, $data = null)
	{ 
		if ($data['nombre'] == null) {
			$data['nombre'] = "";
		} else {
			//vacio
		}
		$data['titulo'] = "Tecnico";
		$this->load->view('Estructura/Encabezado',$data);
		$this->load->view('Tecnico/PrincipalTecn',$output);
	    //$this->load->view('Estructura/PiePagina');
	}


	public function CRUD_Incidencias()
	{
	$this ->load->model('incidenciasTecnico');
	//$this ->load->model('incidencias');
	$data['datos'] = $this->incidenciasTecnico->datos_incidencias();
	$data['salon'] = $this->incidenciasTecnico->salon();
	$data['tipos'] = $this->incidenciasTecnico->tipo_salon();
	$data['seccion'] = "SECCIÓN DE INCIDENCIAS";
	$data2['titulo'] = "Tecnico";
	$this->load->view('Estructura/Encabezado', $data2);
	$this->load->view('Tecnico/TablaIncidenciasTecnico', $data);
	//$this->load->view('Estructura/PiePagina');
	}
    
    public function CRUD_Menupdf()
	{
		$this ->load->model('incidenciasTecnico');
	    //$this ->load->model('incidencias');
		$data['datosalones'] = $this->incidenciasTecnico->datos_salones();
		$data['salon'] = $this->incidenciasTecnico->salon();
		$data['tipos'] = $this->incidenciasTecnico->tipo_salon();
       // $data['seccion'] = "SECCIÓN DE INCIDENCIAS";
		$data2['titulo'] = "Tecnico";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Tecnico/Menupdf', $data);
	    //$this->load->view('Estructura/PiePagina');
	}
 ///////// DISENO DEL DE PDF CON LAS FUNCIONES DEL MDPF PARA EL LOCAL   ////////////
  public function pdf($grupo)
  {
    if (isset($grupo) && is_numeric($grupo) ){
      $this ->load->model('incidenciasTecnico');
    //$this ->load->model('incidencias');
      $datos = $this->incidenciasTecnico->datos_incidencias_pdf($grupo);
       
      if ($datos != null)
      {
      include(APPPATH."third_party/vendor/autoload.php");

      $mpdf = new \Mpdf\Mpdf([
      'margin_top' => '35',
      'mode' => 'utf-8', //Codepage Values OR Codepage Value
      'format' => 'A4-L']);
      //$mpdf->AddPage('L');
      $mpdf->defaultheaderline=0;
      $mpdf->defaultfooterline=0;

      $mpdf->SetHeader(' <div class="img">
        <img src="imagenes/loimss.png" width="320">
        </div>
        <h1 class="text1"><b>Reporte de logros e incidencias diarias por sala de atención</b></h1>
       
        <table width="100%" border="0" width="100%" >
            <thead>
                <tr>
                    <th width="78%" style="text-align: left;" ><b>Guarderia No.001</b></th>
                    <th width="22%" style="text-align: left;" ><b>Sala: '.$datos[0]->Nombre_salon.'</b></th>
                </tr>
            </thead>
        </table>
        
     ');
      $mpdf->SetFooter('<h1 align="right">
        <font face="arial" size=1>DPES/CG/014/006</font>
        </h1>
        <h1 class="Nota">NOTA: Este formato será de uso exclusivo para el reporte de logros e incidencias de los niños(as).</h1>{PAGENO}');

      $html = '
      <html>

      <head>
      <style>
      body {
        font-family: Arial, Helvetica, sans-serif;
      }

      .text1 {
        font-weight: bolder;
        font-size: 15px;
        width: 100%;
        text-align: center;

      }

      .img {
        font-weight: bolder;
        font-size: 12px;
        text-align: left;
      }

      .text2 {
        font-weight: bolder;
        font-size: 12px;
        text-align: left;
      }

      .Nota {
        font-weight: bolder;
        font-size: 9px;
        text-align: left;
      }


      table {
        border-collapse: collapse;
      }

      table,
      th,
      td {
        
        font-weight: bolder;
        font-size: 12px;
      }
      </style>
      </head>

      <body>
      
      <table class="tabla1" style="text-align: center" width="100%" border="1">
      <thead>
      <tr>
      <th width="7%" bgcolor="#F2F2F2" style="color:#000000">Fecha</th>
      <th width="7%" bgcolor="#F2F2F2" style="color:#000000">Hora</th>
      <th width="23%" bgcolor="#F2F2F2" style="color:#000000">Nombre del niño(a)</th>
      <th width="25%" bgcolor="#F2F2F2" style="color:#000000">Descripción de logro o incidencia</th>
      <th width="19%" bgcolor="#F2F2F2" style="color:#000000">Nombre y firma de la persona que reporta</th>
      <th width="19%" bgcolor="#F2F2F2" style="color:#000000">Nombre y firma del Jefe de servicio</th>
      </tr>

     
      </thead>
      <tbody>';

      foreach ($datos as $key => $value)
      {
          $arraymod = explode(' ',$value->FechaBase);
        $html.= '
        <tr>
        <td>'. $arraymod[0] .'</td>
        <td>'. $arraymod[1] .'</td>
        <td align="left">'. $value->Nombre_ninio .'</td>
        <td align="left">'. $value->Observacion .'</td>
        <td align="left">'. $value->Nombre_docente.'</td>
        <td align="left">'.$value->Nombre_tecnico .'</td>
        </tr>';
      }
      $html.='
      </tbody>
      </table>
      </body>
      
      

      </html>';



    // echo $html;
      $mpdf->WriteHTML($html);
      $mpdf->Output('uygt.pdf','I');
    }else{
      $html='
      <!-- Versión compilada y comprimida del CSS de Bootstrap -->
      <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
      <br><br><br>
      <center>
          <div class="alert alert-danger">AÚN NO HAY INCIDENCIAS REGISTRADAS</div>
      </center>
      ';
      echo $html;
      echo "<script>
      setTimeout(function(){
            window.close();
        },2000);
      </script>";

    }
  }else{      echo "<script>
      alert('valor no numerico');
      window.location = '".site_url()."Tecnico/CRUD_Menupdf'
      </script>";}
  }
 ///////// DISENO DEL DE PDF CON LAS FUNCIONES DEL MDPF PARA EL S E R V I D O R   ////////////   
   /* public function pdf($grupo)
    {
    if (isset($grupo) && is_numeric($grupo) ){
    $this ->load->model('incidenciasTecnico');
    //$this ->load->model('incidencias');
    $datos = $this->incidenciasTecnico->datos_incidencias_pdf($grupo);
    include(APPPATH."third_party/mpdf600/mpdf.php");

    $mpdf->defaultheaderline=0;
    $mpdf->defaultfooterline=0;

    $mpdf = new mpdf('c','A4-L',0,'',10,10,35,10);//IZQUIERDA, DERECHA, ARRIBA, ABAJO
    $mpdf->SetHeader(' <div class="img">
        <img src="imagenes/loimss.png" width="320">
    </div>
    <h1 class="text1"><b>Información sobre la atencion de los niños</b></h1>
    <h1 class="text2"><b>Guarderia No.001</b></h1>');
    $mpdf->SetFooter('<h1 align="right">
        <font face="arial" size=1> 320-009-293</font>
    </h1>
    <h1 class="Nota">Nota: El lenguaje empleado en el presente documento no busca generar distinción alguna entre hombre y mujeres, por lo que las referencias o alusiones en la redacción a un género representa a ambos sexos.</h1>{PAGENO}');

    $html = '
    <html>

    <head>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            .text1 {
                font-weight: bolder;
                font-size: 15px;
                width: 100%;
                text-align: center;

            }

            .img {
                font-weight: bolder;
                font-size: 12px;
                text-align: left;
            }

            .text2 {
                font-weight: bolder;
                font-size: 12px;
                text-align: left;
            }

            .Nota {
                font-weight: bolder;
                font-size: 9px;
                text-align: left;
            }


            table {
                border-collapse: collapse;
            }

            table,
            th,
            td {
                border: 1px solid black;
                font-weight: bolder;
                font-size: 12px;
            }
        </style>
    </head>

    <body>
        <table class="tabla1" style="text-align: center" WIDTH="100%">
            <thead>
                <tr>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Fecha</th>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Sala</th>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Nombre del niño</th>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Descripción del logro o incidencia</th>
                    <th COLSPAN=2 bgcolor="#4485BB" style="color:#FFFFFF">Trabajador usuario o Persona autorizada</th>
                </tr>
                <tr bgcolor="#4485BB" style="color:#FFFFFF">
                    <th style="color:#FFFFFF">Nombre</th>
                    <th style="color:#FFFFFF">Firma</th>
                </tr>
            </thead>
            <tbody>';

                foreach ($datos as $key => $value)
                {
                $html.= '
                <tr>
                    <td>'. $value->FechaBase .'</td>
                    <td>'. $value->Nombre_salon .'</td>
                    <td align="left">'. $value->Nombre_ninio .'</td>
                    <td align="left">'. $value->Observacion .'</td>
                    <td align="left">'. $value->Nombre_docente.'<br>'.$value->Nombre_tecnico .'</td>
                    <td></td>
                </tr>';
                }
                $html.='
            </tbody>
        </table>
    </body>

    </html>';
    // echo $html;
    $mpdf->WriteHTML($html);
    $mpdf->Output('uygt.pdf','I');
    }else{
    echo "<script>
        alert('valor no numerico');
        window.location = '".site_url()."Tecnico/CRUD_Menupdf'
    </script>";

    }
    }*/
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    
   public function CRUD_Incidencias_Atendidas()
	{
		$this->load->model('incidenciasTecnico');
		$data['datos'] = $this->incidenciasTecnico->CRUD_Incidencias_Revisadas();
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS ATENDIDAS";
		$data2['titulo'] = "Tecnico";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Tecnico/TablaIncidenciasTecnico', $data);
		}

	public function CRUD_Incidencias_Pendientes()
	{
		$this->load->model('incidenciasTecnico');
		$data['datos'] = $this->incidenciasTecnico->CRUD_Incidencias_Pendientes();
        $data['seccion'] = "SECCIÓN DE INCIDENCIAS PENDIENTES";
		$data2['titulo'] = "Tecnico";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Tecnico/TablaIncidenciasTecnico', $data);
      //  header("Location: ");
	}

	public function CRUD_Citas()
	{
		$this ->load->model('citas');
		$data['datos'] = $this->citas->datos_citas();
		$data2['titulo'] = "Tecnico";
    $data['seccion'] = "SECCIÓN DE CITAS";
		$this->load->view('Estructura/Encabezado', $data2);
		$this ->load->view('Tecnico/Tablacitas', $data);
        //var_dump($data);
	   }
        
    public function CRUD_Citas_Atendidas()
	{
	    $this ->load->model('citas');
        $data['datos'] = $this->citas->CRUD_Citas_Atendidas();
	    $data2['titulo'] = "Tecnico";
      $data['seccion'] = "SECCIÓN DE CITAS ATENDIDAS";
	    $this->load->view('Estructura/Encabezado', $data2);
	    $this ->load->view('Tecnico/Tablacitas', $data);
        //var_dump($data);
	}

    public function CRUD_Citas_Pendientes()
	{
	    $this ->load->model('citas');
        $data['datos'] = $this->citas->CRUD_Citas_Pendientes();
	    $data2['titulo'] = "Tecnico";
      $data['seccion'] = "SECCIÓN DE CITAS PENDIENTES";
	    $this->load->view('Estructura/Encabezado', $data2);
	    $this ->load->view('Tecnico/Tablacitas', $data);
        //var_dump($data);
	}

	public function Control()
	{
		$this ->load->model('incidenciasTecnico');
		$data['salon'] = $this->incidenciasTecnico->salon();
		//$data['datos'] = $this->incidenciasadministrador->datos_control();
		$data['titulo'] = "Tecnico";
		$data['seccion'] = "SECCIÓN DE CONTROL DE INCIDENCIAS";
		$this->load->view('Estructura/Encabezado', $data);
		$this->load->view('Tecnico/Controltecnico', $data);
		//$this->load->view('Estructura/PiePagina');
		//var_dump($data['datos']);
	}

	public function nombres_control()
	{
        $post = $this->input->post();
        $this->load->model('incidenciasTecnico');
	    $nomb = $this->incidenciasTecnico->datos_control($post['FK_salon'], $post['fecha1'], $post['fecha2']);
	    echo json_encode($nomb);
	}
    
    public function modificarcitas()
    {
        $post =	$this->input->post();
        $this->load->model('citas');
        $filacita = $this->citas->modificar($post['idcitas']);
        echo json_encode($filacita);
        //var_dump($filacita);
    }    
    
    public function modifcita()
    {    
        $data = $this->input->post();
        $arraycita = array(
            'FechaAtencionCita' => date('Y-m-d H:i:s'),
            'Descripcion' => $data['desc'],
            'EstadoCita'=> $data['estadocita']
        );
        $this->db->where('Id_citas', $data['idcita']);
        $this->db->update('citas', $arraycita);
        $this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO CORRECTAMENTE');
        echo "<script>
             window.location = '".site_url()."Tecnico/CRUD_Citas'
             </script>";
    }

    public function eliminarcitas()
    {
        $delet = $this->input->post();
        $d = $this->db->delete('citas',array('Id_citas' => $delet['iditas']));
        echo json_encode($d);
    }

    
    public function guardarincidencia()
    {
        $data = $this->input->post();
        $arrayName = array(
	        'FechaBase' => date('Y-m-d H:i:s'),
	        'Observacion' => $data['obsr'],
	        'EstadoIncidencia' => 'PENDIENTE',
	        'FK_ninio' => $data['nombrenino'],
	        'FK_docente' => $data['Iddocente'],
	        'FK_incidencias' => $data['incidencia'],
	        'Cita' => $data['cita']
	    );
        $this->db->insert('incidencias',$arrayName);
        $this->session->set_flashdata('alerta', 'REGISTRO AGREGADO EXITOSAMENTE');
        echo "<script>
                window.location= '".site_url()."Tecnico/CRUD_Incidencias'
            </script>";
    } 

    public function eliminarincidencia()
    {
        $g = $this->input->post();
        $el = $this->db->delete('incidencias', array('Id_incidencia' => $g['idel']));
        echo json_encode($el);
    } 

    public function modificarincidencia()
    {
        $post = $this->input->post();
        $this->load->model('incidenciasTecnico');
        $fila = $this->incidenciasTecnico->modificar($post['idinc']);
        //var_dump($fila); die();
        echo json_encode($fila);    
    } 

  //////////////////////////////funcion que envia correo median te el servidor ////////////////////////
/*    public function modifinc()
    {
      $data = $this->input->post();
      $arrayMod = array(
        'Observacion' => $data['obsr'],
        'FK_tecnico'  => $data['Idtecnico'],
        'EstadoIncidencia' => $data['EstadoInc'],
        'FechaAtencion' => date('Y-m-d H:i:s')
      );
      $this->db->where('Id_incidencia',$data['id']);
      $this->db->update('incidencias',$arrayMod);

      $asunto = "NOTIFICACIÓN DE INCIDENCIA EN LA GUARDERIA #1 DEL IMSS";
      $mensaje = "C. <u><b> ".$data['nombretutor']."</b></u>  DE ACUERDO A LAS NORMAS DE LA INSTITUCION, MEDIANTE ESTE CORREO SE LE INFORMA QUE SU HIJO <u><b> ".$data['nombreninio']." </b></u> TUVO LA SIGUIENTE INCIDENCIA: <br><br>
      TIPO DE INCIDENCIA: ".$data['nombretipo']." <br><br>
      INCIDENCIA: ".$data['nombreinc']." <br><br>
      OBSERVACION: ".$data['obsr']." ";
      $destino = array($data['correo']);
      $respuestacorreo = correo($destino, $asunto, $mensaje);

      $datas = json_decode($respuestacorreo);
      switch ($datas[0]) {
        case 1:
        $arrayEstatus = array(
          'EntregaIncidencia' => "ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['id']);
        $this->db->update('incidencias', $arrayEstatus);
        $this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO Y CORREO ENVIADO CORRECTAMENTE');
        redirect('Tecnico/CRUD_Incidencias');
        break;

        default:
        $arrayEstatus = array(
          'EntregaIncidencia' => "NO ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['id']);
        $this->db->update('incidencias', $arrayEstatus);
        $this->session->set_flashdata('alerta', 'ERROR AL MODIFICAR REGISTRO Y CORREO NO ENVIADO');
        redirect('Tecnico/CRUD_Incidencias');
        break;
      }

    }*/
    
    ////////////////////////////////ENVIO DE CORREO DE MANERA LOCAL!!!///////////////////////////////////////////
public function modifinc()
  {
    $data = $this->input->post();
    $arrayMod = array(
      'Observacion' => $data['obsr'],
      'FK_tecnico'  => $data['Idtecnico'],
      'EstadoIncidencia' => $data['EstadoInc'],
      'FechaAtencion' => date('Y-m-d H:i:s')
    );
    $this->db->where('Id_incidencia', $data['id']);
    $this->db->update('incidencias', $arrayMod);
/*    $this->load->library('mail/PHPMailer');
    $this->load->library('mail/SMTP');
    $this->load->library('mail/Exception');
      // Crear una nueva  instancia de PHPMailer habilitando el tratamiento de excepciones
    $mail = new PHPMailer(true);
    try {
      $remitente = "guarderia001aca@gmail.com";
      $mail->isSMTP();
      //$mail->SMTPDebug = 1;
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = "ssl";
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; 
      $mail->Username = $remitente;
      $mail->Password = '#!98&.$sw@D';
      $mail->SMTPOptions = array(
        'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
        )
      );
      $mail->setFrom($remitente, "NOTIFICACIÓN DE INCIDENCIA");
      $mail->addAddress("monzalvo971@gmail.com");
      //Enviar una copia del correo 
      //$mail->addCC('eliza.ld90s@gmail.com');
      //Enviar una copia oculta del correo
      //$mail->addBCC('copia_oculta@outlook.com');
      $mail->isHTML(true); 
      $mail->CharSet = "UTF-8";
      $mail->Subject = "NOTIFICACIÓN DE INCIDENCIA EN LA GUARDERIA #1 DEL IMSS";
      $mail->Body    = "C. <u><b> ".$data['nombretutor']."</b></u>  DE ACUERDO A LAS NORMAS DE LA INSTITUCION, MEDIANTE ESTE CORREO SE LE INFORMA QUE SU HIJO <u><b> ".$data['nombreninio']." </b></u> TUVO LA SIGUIENTE INCIDENCIA: <br><br>
      TIPO DE INCIDENCIA: ".$data['nombretipo']." <br><br>
      INCIDENCIA: ".$data['nombreinc']." <br><br>
      OBSERVACION: ".$data['obsr']." ";
            //$mail->send();
      if ($mail->Send()) {
        $arrayEstatus = array(
          'EntregaIncidencia' => "ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['id']);
        $this->db->update('incidencias', $arrayEstatus);
      } else {
                //sE DEBE ACTUALIZAR EL REGISTRO PERO SE NECESITA EL ID DE LA INCIDENCIA PARA PODER MODIFICAR.
        $arrayEstatus = array(
          'EntregaIncidencia' => "NO ENVIADO"
        );
        $this->db->where('Id_incidencia',$data['id']);
        $this->db->update('incidencias',$arrayEstatus);
      }
    } catch (Exception $e) {
            //echo "3";
            // return $mail->ErrorInfo;
    }*/
    $this->session->set_flashdata('alerta', 'REGISTRO REVISADO EXITOSAMENTE');
    echo "<script>
                   window.location= '".site_url()."Tecnico/CRUD_Incidencias'
         </script>";
  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
}