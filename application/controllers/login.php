<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class login extends CI_Controller
{


  public function __construct()
  {
    parent::__construct();
  }

  public function validacion()
  { 
    $post = $this->input->post();
    $usuario1 = $post['usuario'];
    //$contraseña = $post['contraseña'];
    $contraseña = md5($post['contraseña']);
    $this->load->model('user');
    $fila = $this->user->getUser($usuario1, $contraseña);
    if ($fila != null) {
      $this->session->set_userdata('login', true);
      $arraydata = array(
        'Id_trabajadores'  => $fila->Id_trabajadores,
        'tipo_trabajador'  => $fila->TipoTrabajador,
        'Nombre' => $fila->Nombre_trabajador,
        'APaterno' => $fila->ApellidoPaterno_trabajador,
        'AMaterno' => $fila->ApellidoMaterno_trabajador,
        'email' => $fila->email
      );
      $this->session->set_userdata($arraydata);
      header("location:".base_url().'roles/rolles');
    } else {
      $this->session->set_flashdata('color', 'alert alert-danger');
      $this->session->set_flashdata('alert', 'Usuario y contraseña incorrecta');
      echo "<script>
      window.location.href= '".site_url()."login/iniciar_sesion'
      </script>";
    }
  }
  public function ActualizarContrasena()
  {
    $post = $this->input->post();
    $contraseña = md5($post['contraseña']);
    $esnuevacontraseña = md5($post['nuevacontraseña']);
    $this->load->model('user');
    $validar = $this->user->Nuevacontrasena($contraseña,$post['idtrabajador']);
    if($validar == true) {
      $arrayContra = array(
        'contrasena' => $esnuevacontraseña            
      );
      $this->db->where('Id_trabajadores', $post['idtrabajador']);
      $this->db->update('trabajadores', $arrayContra);
      echo "<script>
      window.location.href= '".site_url()."roles/rolescon'
      </script>";
    } else {

      $this->session->set_flashdata('color', 'alert alert-danger');
      $this->session->set_flashdata('alert', 'Contraseña Incorrecta, Ingrese Su Contraseña Actual');
      echo "<script>
      window.location.href= '".site_url()."Home/contrasena'
      </script>";
    }
    
  }

  public function iniciar_sesion()
  {
    $data['titulo'] = "iniciar sesion";
    $this->load->view('Estructura/Encabezado', $data);
    $this->load->view('Login/login');
		//$this->load->view('Estructura/PiePagina');
  }

  public function cerrar_sesion()
  {
    $this->session->sess_destroy();
    $this->session->set_userdata('login', false);
    header("location:".base_url());
  }
}