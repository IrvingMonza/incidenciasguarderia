
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Enfermera_Nutriologo extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->principal((object)array('output' => '' , 'data' => '', 'js_files' => array(), 'css_files' => array()));
		$this ->load->view('Enfermera_Nutriologo/inicio');
	}

	public function principal($output = null,$data=null)
	{ 
		if ($data['nombre'] == null) {
			$data['nombre'] = "";
		} else {
			//vacio
		}
		$data['titulo'] = "Enfermero/nutriologo";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Enfermera_Nutriologo/Principal', $output);
	    //$this->load->view('Estructura/PiePagina');
	}

	public function CRUD_Incidencias()
	{
		$this->load->model('incidenciasenfermera');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasenfermera->datos_incidencias();
		$data['salon'] = $this->incidenciasenfermera->salon();
		$data['tipos'] = $this->incidenciasenfermera->tipo_salon();
		$data2['titulo'] = "Enfermera_Nutriologo";
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Enfermera_Nutriologo/TablaIncidenciasEnfermera', $data);
	    //$this->load->view('Estructura/PiePagina');
	}
	
	public function CRUD_Incidencias_Atendidas()
	{
		$this->load->model('incidenciasenfermera');
		$data['datos'] = $this->incidenciasenfermera->CRUD_Incidencias_Revisadas();
		$data2['titulo'] = "Enfermera_Nutriologo";
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS ATENDIDAS";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Enfermera_Nutriologo/TablaIncidenciasEnfermera', $data);
	}

	public function CRUD_Incidencias_Pendientes()
	{
		$this->load->model('incidenciasenfermera');
		$data['datos'] = $this->incidenciasenfermera->CRUD_Incidencias_Pendientes();
		$data2['titulo'] = "Enfermera_Nutriologo";
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS PENDIENTES";
		$this->load->view('Estructura/Encabezado', $data2);
		$this ->load->view('Enfermera_Nutriologo/TablaIncidenciasEnfermera', $data);
	}

	public function CRUD_Citas()
	{
		$this ->load->model('citasEN');
		$data['datos'] = $this->citasEN->datos_citas();
		$data2['titulo'] = "Enfermera_Nutriologo";
		$data['seccion'] = "SECCIÓN DE CITAS";
		$this->load->view('Estructura/Encabezado', $data2);
		$this ->load->view('Enfermera_Nutriologo/Tablacitas', $data);
        //var_dump($data);
	}

	public function CRUD_Citas_Atendidas()
	{
		$this ->load->model('citasEN');
		$data['datos'] = $this->citasEN->CRUD_Citas_Atendidas();
		$data2['titulo'] = "Enfermera_Nutriologo";
		$data['seccion'] = "SECCIÓN DE CITAS ATENDIDAS";
		$this->load->view('Estructura/Encabezado', $data2);
		$this ->load->view('Enfermera_Nutriologo/Tablacitas', $data);
        //var_dump($data);
	}

	public function CRUD_Citas_Pendientes()
	{
		$this ->load->model('citasEN');
		$data['datos'] = $this->citasEN->CRUD_Citas_Pendientes();
		$data2['titulo'] = "Enfermera_Nutriologo";
		$data['seccion'] = "SECCIÓN DE CITAS PENDIENTES";
		$this->load->view('Estructura/Encabezado',$data2);
		$this ->load->view('Enfermera_Nutriologo/Tablacitas',$data);
        //var_dump($data);
	}
	
	
	public function modificarcitas()
	{
		$post =	$this->input->post();
		$this->load->model('citasEN');
		$filacita = $this->citasEN->modificar($post['idcitas']);
		echo json_encode($filacita);
        //var_dump($filacita);
	}
	
	
	public function modifcita()
	{	
		$data = $this->input->post();
		$arraycita = array(
		    'FechaAtencionCita' => date('Y-m-d H:i:s'),
			'Descripcion' => $data['desc'],
			'EstadoCita'=> $data['estadocita']
		);
		$this->db->where('Id_citas', $data['idcita']);
		$this->db->update('citas', $arraycita);
		$this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO EXITOSAMENTE');
		echo "<script>
		               window.location = '".site_url()."Enfermera_Nutriologo/CRUD_Citas'
		     </script>";
	}

	public function eliminarcitas()
	{
		$delet = $this->input->post();
		$d = $this->db->delete('citas',array('Id_citas' => $delet['iditas']));
		echo json_encode($d);
	}

	public function modificarincidencia()
	{
		$post = $this->input->post();
		$this->load->model('incidenciasenfermera');
		$fila = $this->incidenciasenfermera->modificar($post['idinc']);
		echo json_encode($fila);
	}
	
	/*public function modifinc()
    {
      $data = $this->input->post();
      $arrayMod = array(
        'Observacion' => $data['obsr'],
        'FK_tecnico'  => $data['Idtecnico'],
        'EstadoIncidencia' => $data['EstadoInc'],
        'FechaAtencion' => date('Y-m-d H:i:s')
      );
      $this->db->where('Id_incidencia',$data['id']);
      $this->db->update('incidencias',$arrayMod);

      $asunto = "NOTIFICACIÓN DE INCIDENCIA EN LA GUARDERIA #1 DEL IMSS";
      $mensaje = "C. <u><b> ".$data['nombretutor']."</b></u>  DE ACUERDO A LAS NORMAS DE LA INSTITUCION, MEDIANTE ESTE CORREO SE LE INFORMA QUE SU HIJO <u><b> ".$data['nombreninio']." </b></u> TUVO LA SIGUIENTE INCIDENCIA: <br><br>
      TIPO DE INCIDENCIA: ".$data['nombretipo']." <br><br>
      INCIDENCIA: ".$data['nombreinc']." <br><br>
      OBSERVACION: ".$data['obsr']." ";
      $destino = array($data['correo']);
      $respuestacorreo = correo($destino, $asunto, $mensaje);

      $datas = json_decode($respuestacorreo);
      switch ($datas[0]) {
        case 1:
        $arrayEstatus = array(
          'EntregaIncidencia' => "ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['id']);
        $this->db->update('incidencias', $arrayEstatus);
        $this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO Y CORREO ENVIADO CORRECTAMENTE');
        redirect('Tecnico/CRUD_Incidencias');
        break;

        default:
        $arrayEstatus = array(
          'EntregaIncidencia' => "NO ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['id']);
        $this->db->update('incidencias', $arrayEstatus);
        $this->session->set_flashdata('alerta', 'ERROR AL MODIFICAR REGISTRO Y CORREO NO ENVIADO');
        redirect('Tecnico/CRUD_Incidencias');
        break;
      }
    }*/
	
	public function modifinc()
	{
		$data = $this->input->post();
		$arrayMod = array(
			'Observacion' => $data['obsr'],
			'FK_tecnico'  => $data['Idtecnico'],
			'EstadoIncidencia' => $data['EstadoInc'],
			'FechaAtencion' => date('Y-m-d H:i:s')
		);
		$this->db->where('Id_incidencia',$data['id']);
		$this->db->update('incidencias',$arrayMod);
		
	    $this->session->set_flashdata('alerta', 'INCIDENCIA REVISADA EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Enfermera_Nutriologo/CRUD_Incidencias'
		</script>";
	}
}