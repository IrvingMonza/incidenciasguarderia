<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller
{
	public function index()
	{
      $data['titulo'] = "pagina principal";
	  $this->load->view('Estructura/Encabezado', $data);
	  $this ->load->view('Estructura/index.html');
	  $this->load->view('Estructura/PiePagina');
	}

	public function contrasena()
	{
		$data['titulo'] = "Cambiar contraseña";
		$this->load->view('Estructura/Encabezado', $data);
		$this->load->View('contrasena');
	}
}
