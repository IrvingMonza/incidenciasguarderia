<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Directora extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->principal((object)array('output' => '', 'data' => '', 'js_files' => array(), 'css_files' => array()));
		$this ->load->view('Directora/inicio');
	}

	public function principal($output = null, $data = null)
	{ 
		if ($data['nombre'] == null) {
			$data['nombre']="";
		} else {
			//vacio
		}
		$data['titulo'] = "Directora";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Directora/principal', $output);
	    //$this->load->view('Estructura/PiePagina');
	}

	public function CRUD_Trabajador()
	{
		$data['nombre'] = "SECCIÓN DE TRABAJADORES";		
		//Se elige el lenguaje de la funcion
		$this->grocery_crud->set_language('spanish');	
		// Se escoje el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
		//Es la tabla con la que va a trabajar la funcion
		$this->grocery_crud->set_table('trabajadores');
		// este sera para saber que columnas va a mostrar el crud
		$this->grocery_crud->columns('Nombre_trabajador', 'ApellidoPaterno_trabajador', 'ApellidoMaterno_trabajador', 'email', 'Genero', 'TipoTrabajador');
		//Se eligen los atributos que va a ser obligatorios
		$this->grocery_crud->required_fields('Nombre_trabajador', 'Genero', 'TipoTrabajador');
		//Se le asignan nuevos nombre a los campos para que no se muestran tal cual de la base de datos 	
		$this->grocery_crud->display_as('Nombre_trabajador', 'NOMBRE')
		->display_as('ApellidoPaterno_trabajador', 'APELLIDO PATERNO')
		->display_as('ApellidoMaterno_trabajador', 'APELLIDO MATERNO')
		->display_as('Genero', 'GÉNERO')
		->display_as('email', 'CORREO')
		->display_as('TipoTrabajador', 'TIPO DE TRABAJADOR')
		->display_as('Contrasena', 'CONTRASEÑA');
		//Se especifica de que tipo seran los campos
		//Este codigo es para que el campo de contraseña se muestre encriptado
		$this->grocery_crud->field_type ('Contrasena', 'password');  
		//Se especifican los campos que necesiten reglas especificas
		// verifica que el campo correo sea valido y que no este vacio
		$this->grocery_crud->set_rules('email', 'CORREO', 'required|valid_email');
		//Verifica que los campos nombre, apellido parterno y apellido materno sea de letras y que no este vacio  
		$this->grocery_crud->set_rules('Nombre_trabajador', 'NOMBRE', 'required|regex_match[/^[\p{L} ,.]*$/u]');  
		$this->grocery_crud->set_rules('ApellidoPaterno_trabajador', 'APELLIDO PATERNO', 'required|regex_match[/^[\p{L} ,.]*$/u]');  
		$this->grocery_crud->set_rules('ApellidoMaterno_trabajador', 'APELLIDO MATERNO', 'required|regex_match[/^[\p{L} ,.]*$/u]');  
		//Se utiliza la funcion callback_add_field para darle atributos especificos a los campos, en este caso valida que los campos cambien su tipo de letra a mayusculas
		$this->grocery_crud->callback_add_field('TipoTrabajador', function()
		{
			return '
			<select  class="form-control" name="TipoTrabajador" id="field-TipoTrabajador">
			<option value="">TIPO DE TRABAJADOR</option>
			<option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
			<option value="DOCENTE">DOCENTE</option>
			<option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIOLOGO</option>
			<option value="FILTRO">FILTRO</option>
			</select>'; 
		});
		$this->grocery_crud->callback_add_field('Nombre_trabajador', function()
		{
			return '<input type="text" value="" name="Nombre_trabajador" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">'; 
		});
		$this->grocery_crud->callback_add_field('ApellidoPaterno_trabajador', function()
		{
			return '<input type="text" value="" name="ApellidoPaterno_trabajador" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">'; 
		});
		$this->grocery_crud->callback_add_field('ApellidoMaterno_trabajador', function()
		{
			return '<input type="text" value="" name="ApellidoMaterno_trabajador" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">'; 
		});
		$this->grocery_crud->callback_add_field('Genero', function()
		{
			return '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" /> FEMENINO 
			<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" /> MASCULINO';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre_trabajador', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre_trabajador" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">'; 
		});
		$this->grocery_crud->callback_edit_field('ApellidoPaterno_trabajador', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoPaterno_trabajador" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_edit_field('ApellidoMaterno_trabajador', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoMaterno_trabajador" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_edit_field('Genero', function($value, $primary_key)
		{
			if ($value == 'FEMENINO') {
				return '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" checked /> FEMENINO 
				<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" /> MASCULINO';	
			} else {
				return '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" /> FEMENINO 
				<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" checked/> MASCULINO';
			}

		});
			    $this->grocery_crud->callback_edit_field('Contrasena', function($value, $primary_key)
		{
//return '<input type="checkbox" id="field-Genero" name="Genero" value="FEMENINO"/>';
			return '
			<input type="checkbox" id="check" onchange="habilitar(this.checked);">
			<input type="text" value ="Habilitar edición" id="field-Contrasena" name="Contrasena" disabled>';	
		});
		//$this->grocery_crud->callback_after_insert('Contrasena',array($this,'encrypt'));
        $this->grocery_crud->callback_before_insert(array($this,'encrypt'));
        $this->grocery_crud->callback_before_update(array($this,'encryptupdate'));
		$this->grocery_crud->callback_edit_field('TipoTrabajador', function($value, $primary_key)
		{
			/*if ($value == "ADMINISTRADOR") {
				return '<select  class="form-control" name="TipoTrabajador" id="field-TipoTrabajador">
				<option value="'.$value.'">'.$value.'</option>
				<option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
				<option value="DOCENTE">DOCENTE</option>
				<option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIOLOGO</option>
				<option value="FILTRO">FILTRO</option>
				</select>';
			} else*/if ($value == "TÉCNICO/EDUCADORA") {
				return '<select  class="form-control" name="TipoTrabajador" id="field-TipoTrabajador">
				<option value="'.$value.'">'.$value.'</option>
				
				<option value="DOCENTE">DOCENTE</option>
				<option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIOLOGO</option>
				<option value="FILTRO">FILTRO</option>
				</select>';
			} elseif ($value == "DOCENTE") {
				return '<select  class="form-control" name="TipoTrabajador" id="field-TipoTrabajador">
				<option value="'.$value.'">'.$value.'</option>
				
				<option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
				<option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIOLOGO</option>
				<option value="FILTRO">FILTRO</option>
				</select>';
			} elseif ($value == "ENFERMERA/NUTRIOLOGO") {
				return '<select  class="form-control" name="TipoTrabajador" id="field-TipoTrabajador">
				<option value="'.$value.'">'.$value.'</option>
				
				<option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
				<option value="DOCENTE">DOCENTE</option>
				<option value="FILTRO">FILTRO</option>
				</select>';
			} else {
				return '<select  class="form-control" name="TipoTrabajador" id="field-TipoTrabajador">
				<option value="'.$value.'">'.$value.'</option>
				
				<option value="TÉCNICO/EDUCADORA">TÉCNICO/EDUCADORA</option>
				<option value="DOCENTE">DOCENTE</option>
				<option value="ENFERMERA/NUTRIOLOGO">ENFERMERA/NUTRIOLOGO</option>
				</select>';
			}
		});
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
	}
	public function encrypt($post)
	{
		$pass = $this->input->post('Contrasena');
		$post['Contrasena'] = md5($pass);
		return $post;
	}
		public function encryptupdate($post)
	{
		//$pass = $this->input->post('Contrasena');
		if(isset($post['Contrasena']) && !empty($post['Contrasena'])){
			$post['Contrasena'] = md5($post['Contrasena']);
		return $post;
		}
		//var_dump($post); die();
		return $post;
	}

	public function CRUD_Ninios()
	{
		$data['nombre'] = "SECCIÓN DE NIÑOS";
		//Se elige el lenguaje de la funcion
		$this->grocery_crud->set_language('spanish');
		//Se elige el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
		//Se elige con que tabla con la que va a trabajar el CRUD
		$this->grocery_crud->set_table('ninios');		
		//Se escojen las tablas que se van a mostrar en el CRUD
		$this->grocery_crud->columns('Nombre_ninio', 'ApellidoPaterno_ninio', 'ApellidoMaterno_ninio', 'Edad', 'FechaNacimiento', 'Genero', 'FK_salon', 'FK_Tutores','autorizado1','autorizado2','autorizado3');
		//Se especifica que campos va a ser obligartorios
		$this->grocery_crud->required_fields('FechaNacimiento', 'Sexo', 'salon');
 		//Se le asignan nuevos nombre a los campos para que no se muestran tal cual de la base de datos 	
		$this->grocery_crud->display_as('Nombre_ninio', 'NOMBRE')
		->display_as('ApellidoPaterno_ninio', 'APELLIDO PATERNO')
		->display_as('ApellidoMaterno_ninio', 'APELLIDO MATERNO')
		->display_as('Edad', 'EDAD')
		->display_as('FechaNacimiento', 'FECHA DE NACIMIENTO')
		->display_as('Genero', 'GENERO')
		->display_as('email', 'CORREO')
		->display_as('FK_salon', 'SALÓN')
		->display_as('FK_Tutores', 'PADRE O TUTOR')
        ->display_as('autorizado1', 'PERSONA AUTORIZADA 1')
        ->display_as('autorizado2', 'PERSONA AUTORIZADA 2')
        ->display_as('autorizado3', 'PERSONA AUTORIZADA 3');
		//Se especfica de que tipo seran los campos
    	//Se especifican los campos que necesiten reglas especificas
		//valida que los campos sean de solo letras y que no esten vacios
		$this->grocery_crud->set_rules('Nombre_ninio', 'NOMBRE', 'required|regex_match[/^[\p{L} ,.]*$/u]' ); 
		$this->grocery_crud->set_rules('ApellidoPaterno_ninio', 'APELLIDO PATERNO', 'required|regex_match[/^[\p{L} ,.]*$/u]');
		$this->grocery_crud->set_rules('ApellidoMaterno_ninio', 'APELLIDO MATERNO', 'required|regex_match[/^[\p{L} ,.]*$/u]');
		//Verifica que el capo edad sea numerico, que no este vacio y que su longitud no sea mayor a 2 digitos
		$this->grocery_crud->set_rules('Edad', 'EDAD', 'required|numeric');
		$this->grocery_crud->set_rules('Edad', 'EDAD', 'required|max_length[2]');
		//crea una relacion, el primer parametro es el campo en el que se mostrara la relacion, el segundo es la tabla de la base de datos con la que tendra relacion y el tercero es el atributo que desea mostrar
		$this->grocery_crud->set_relation('FK_salon', 'salones', 'Nombre_salon');
		$this->grocery_crud->set_relation('FK_Tutores', 'tutores', '{Nombre_tutor} {ApellidoPaterno_tutor} {ApellidoMaterno_tutor}');
        $this->grocery_crud->set_relation('autorizado1','autorizados','{Nombre_autorizado} {ApellidoPaterno_autorizado} {ApellidoMaterno_autorizado}');
        $this->grocery_crud->set_relation('autorizado2','autorizados','{Nombre_autorizado} {ApellidoPaterno_autorizado} {ApellidoMaterno_autorizado}');
        $this->grocery_crud->set_relation('autorizado3','autorizados','{Nombre_autorizado} {ApellidoPaterno_autorizado} {ApellidoMaterno_autorizado}');
		//Se utiliza la funcion callback_add_field para darle atributos especificos a los campos
		//callback_add_field para que en la opcion agregar el campo sexo se muestre como raio button
		$this->grocery_crud->callback_add_field('Genero', function()
		{
			return  '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" /> FEMENINO
			<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" /> MASCULINO';
		});
		//callback_edit_field para que en la opcion editar el campo sexo se muestre como raio button
		$this->grocery_crud->callback_edit_field('Genero', function()
		{
			return  '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" /> FEMENINO
			<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" /> MASCULINO';
		});
		//En este caso valida que los campos cambien su tipo de letra a mayusculas
		$this->grocery_crud->callback_add_field('Nombre_ninio',	function()
		{
			return '<input type="text" value="" name="Nombre_ninio" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_add_field('ApellidoPaterno_ninio', function()
		{
			return '<input type="text" value="" name="ApellidoPaterno_ninio" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_add_field('ApellidoMaterno_ninio', function()
		{
			return '<input type="text" value="" name="ApellidoMaterno_ninio" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_add_field('Edad', function()
		{
			return '<input id="field-Edad" name="Edad" type="text" value="" class="numeric form-control" style=" width:100px" >';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre_ninio', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre_ninio" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_edit_field('ApellidoPaterno_ninio', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoPaterno_ninio" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_edit_field('ApellidoMaterno_ninio', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoMaterno_ninio" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_edit_field('Genero', function($value, $primary_key)
		{
			if ($value == 'FEMENINO') {
				return '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" checked /> FEMENINO 
				<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" /> MASCULINO';	
			} else {
				return '<input type="radio" id="field-Genero" name="Genero" value="FEMENINO" /> FEMENINO 
				<input type="radio" id="field-Genero" name="Genero" value="MASCULINO" checked/> MASCULINO';
			}

		});
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
	}

	public function CRUD_Salones()
	{
		$data['nombre'] = "SECCIÓN DE SALONES";
	    //Se elige el lenguaje de la funcion
		$this->grocery_crud->set_language('spanish');
		// Se elige el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
		//Se coloca la tabla con la que va a trabajar el CRUD
		$this->grocery_crud->set_table('salones');
		//Se colocan los campos que seran obligatorios
		$this->grocery_crud->required_fields('Nombre');
        //Se le asignan nuevos nombre a los campos para que no se muestran tal cual de la base de datos
		$this->grocery_crud->display_as('Nombre_salon', 'NOMBRE DEL SALÓN')
		->display_as('CapacidadMaxima', 'CAPACIDAD MÁXIMA');
        //Se asignan reglas especificas
        //En este caso que el campo capacidadmaxima solo permita numero y su longitud no sea mayor a 2 digitos
		$this->grocery_crud->set_rules('CapacidadMaxima', 'CAPACIDAD MAXIMA', 'required|max_length[2]');
        //Se utiliza la funcion callback_add_field para darle atributos especificos a los campos
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('Nombre_salon', function()
		{
			return '<input type="text" value="" name="Nombre_salon" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        $this->grocery_crud->callback_add_field('CapacidadMaxima',	function()
		{
			return '<input name="CapacidadMaxima" type="text" value="" style=" width:205px" >';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->callback_edit_field('CapacidadMaxima',	function($value, $primary_key)
		{
			return '<input id="field-Edad" name="CapacidadMaxima" type="text" value="'.$value.'" class="numeric form-control" style=" width:205px" >';
		});
		$this->grocery_crud->callback_edit_field('Descripcion', function($value, $primary_key)
		{
			return '<textarea type="text" id="Descripcion" name="Descripcion" cols="40" rows="5" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">'.$value.'</textarea>';
		});  
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
	}

	public function CRUD_Tutores()
	{
		$data['nombre'] = "SECCIÓN DE TUTORES";
        //Se elige el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
        //se elige la tabla con la que vas a trabajar
		$this->grocery_crud->set_table('tutores');
		$this->grocery_crud->columns('Nombre_tutor', 'ApellidoPaterno_tutor', 'ApellidoMaterno_tutor', 'email', 'Contrasena');
		$this->grocery_crud->add_fields('Nombre_tutor', 'ApellidoPaterno_tutor', 'ApellidoMaterno_tutor', 'email', 'Contrasena');
		$this->grocery_crud->edit_fields('Nombre_tutor', 'ApellidoPaterno_tutor', 'ApellidoMaterno_tutor', 'email', 'Contrasena');
		$this->grocery_crud->required_fields('Nombre_tutor', 'ApellidoPaterno_tutor', 'ApellidoMaterno_tutor', 'Contrasena');
		$this->grocery_crud->field_type('Contrasena', 'password');
		//Verifica que el correo sea correcto y que el campo no este vacio
		$this->grocery_crud->set_rules('email', 'CORREO', 'required|valid_email');
		$this->grocery_crud->display_as('Nombre_tutor', 'NOMBRE')
		->display_as('ApellidoPaterno_tutor', 'APELLIDO PATERNO')
		->display_as('ApellidoMaterno_tutor', 'APELLIDO MATERNO')
		->display_as('email', 'CORREO')
		->display_as('Contrasena', 'CONTRASEÑA');
        //Se utiliza la funcion callback_add_field para darle atributos especificos a los campos
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('Nombre_tutor',	function()
		{
			return '<input type="text" value="" name="Nombre_tutor" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre_tutor', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre_tutor" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('ApellidoPaterno', function()
		{
			return '<input type="text" value="" name="ApellidoPaterno" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('ApellidoPaterno',	function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoPaterno" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('ApellidoMaterno', function()
		{
			return '<input type="text" value="" name="ApellidoMaterno" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('ApellidoMaterno',	function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoMaterno" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
	}
    
    	public function CRUD_autorizados()
	{
		$data['nombre'] = "SECCIÓN DE PERSONAS AUTORIZADAS POR LOS TUTORES";
        //Se elige el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
        //se elige la tabla con la que vas a trabajar
		$this->grocery_crud->set_table('autorizados');
		$this->grocery_crud->columns('Nombre_autorizado', 'ApellidoPaterno_autorizado', 'ApellidoMaterno_autorizado', 'Contrasena');
		$this->grocery_crud->add_fields('Nombre_autorizado', 'ApellidoPaterno_autorizado', 'ApellidoMaterno_autorizado', 'Contrasena');
		$this->grocery_crud->edit_fields('Nombre_autorizado', 'ApellidoPaterno_autorizado', 'ApellidoMaterno_autorizado', 'Contrasena');
		$this->grocery_crud->required_fields('Nombre_autorizado', 'ApellidoPaterno_autorizado', 'ApellidoMaterno_autorizado', 'Contrasena');
		$this->grocery_crud->field_type('Contrasena', 'password');
		//Verifica que el correo sea correcto y que el campo no este vacio
		$this->grocery_crud->display_as('Nombre_autorizado', 'NOMBRE')
		->display_as('ApellidoPaterno_autorizado', 'APELLIDO PATERNO')
		->display_as('ApellidoMaterno_autorizado', 'APELLIDO MATERNO')
		->display_as('Contrasena', 'CONTRASEÑA');
        //Se utiliza la funcion callback_add_field para darle atributos especificos a los campos
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('Nombre_autorizado',	function()
		{
			return '<input type="text" value="" name="Nombre_autorizado" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre_autorizado', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre_autorizado" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('ApellidoPaterno_autorizado', function()
		{
			return '<input type="text" value="" name="ApellidoPaterno_autorizado" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('ApellidoPaterno_autorizado',	function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoPaterno_autorizado" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('ApellidoMaterno_autorizado', function()
		{
			return '<input type="text" value="" name="ApellidoMaterno_autorizado" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('ApellidoMaterno_autorizado',	function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="ApellidoMaterno_autorizado" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
            
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
        
	}
    
    

	public function CRUD_Tipos()
	{
		$data['nombre'] = "SECCIÓN DE TIPOS DE INCIDENCIAS";
		//Se elige el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
		//se elige la tabla con la que vas a trabajar
		$this->grocery_crud->set_table('catalogo_tipos');
		$this->grocery_crud->columns('Nombre_tipo_inc');
		$this->grocery_crud->required_fields('Nombre_tipo_inc');
		$this->grocery_crud->display_as('Nombre_tipo_inc', 'NOMBRE');
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('Nombre_tipo_inc', function()
		{
			return '<input type="text" value="" name="Nombre_tipo_inc" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre_tipo_inc', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre_tipo_inc" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
	}

	public function CRUD_catalogo_incidencias(){
		$data['nombre'] = "SECCIÓN DE NOMBRE DE INCIDENCIAS";
        //Se elige el tema del CRUD
		$this->grocery_crud->set_theme('flexigrid');
		//se elige la tabla con la que vas a trabajar
		$this->grocery_crud->set_table('catalogo_incidencias');
		$this->grocery_crud->columns('Nombre_incidencia', 'FK_tipos');
		$this->grocery_crud->required_fields('Nombre_incidencia', 'FK_tipos');
		$this->grocery_crud->display_as('Nombre_incidencia', 'NOMBRE')
		->display_as('FK_tipos', 'TIPO DE INCIDENCIA');
        //En este caso que el campo nombre cambie sus letras a mayusculas
		$this->grocery_crud->callback_add_field('Nombre_incidencia', function()
		{
			return '<input type="text" value="" name="Nombre_incidencia" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
        //Se utiliza la funcion callback_edit_field para que los campos de "editar" cambien su tipo de letra a mayusculas y las function($value, $primary_key) manda a llamar el dato de la llave primaria que esta registrado en "add" 
		$this->grocery_crud->callback_edit_field('Nombre_incidencia', function($value, $primary_key)
		{
			return '<input type="text" value="'.$value.'" name="Nombre_incidencia" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">';
		});
		$this->grocery_crud->set_relation('FK_tipos', 'catalogo_tipos', 'Nombre_tipo_inc');
		$output = $this->grocery_crud->render();
		$this->principal($output, $data);
	}

	public function CRUD_Incidencias()
	{
		$this ->load->model('incidenciasdirectora');
		$data['datos'] = $this->incidenciasdirectora->dir_incidencias();
		$data['salon'] = $this->incidenciasdirectora->salon();
		$data['tipos'] = $this->incidenciasdirectora->tipo_salon();
		$data2['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS";
		$this->load->view('Estructura/Encabezado', $data2);
		$this ->load->view('Directora/TablaIncidenciasDirectora', $data);
	}
	public function CRUD_Incidencias_pendientes()
	{
		$this ->load->model('incidenciasdirectora');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasdirectora->dir_incidencias_pendientes();
		$data['salon'] = $this->incidenciasdirectora->salon();
		$data['tipos'] = $this->incidenciasdirectora->tipo_salon();
		$data['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS PENDIENTES";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Directora/TablaIncidenciasDirectora', $data);
		//$this->load->view('Estructura/PiePagina');
	}
	public function CRUD_Incidencias_atendidas()
	{
		$this ->load->model('incidenciasdirectora');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasdirectora->dir_incidencias_atendidas();
		$data['salon'] = $this->incidenciasdirectora->salon();
		$data['tipos'] = $this->incidenciasdirectora->tipo_salon();
		$data['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE INCIDENCIAS ATENDIDAS";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Directora/TablaIncidenciasDirectora', $data);
		//$this->load->view('Estructura/PiePagina');
	}
    //pdf
	public function CRUD_Menupdf()
	{
		$this ->load->model('incidenciasdirectora');
	    //$this ->load->model('incidencias');
		$data['datosalones'] = $this->incidenciasdirectora->datos_salones();
		$data['salon'] = $this->incidenciasdirectora->salon();
		$data['tipos'] = $this->incidenciasdirectora->tipo_salon();
       // $data['seccion'] = "SECCIÓN DE INCIDENCIAS";
		$data2['titulo'] = "Directora";
		$this->load->view('Estructura/Encabezado', $data2);
		$this->load->view('Directora/Menupdf', $data);
	    //$this->load->view('Estructura/PiePagina');
	}

  //////// ESTRUCTURA DEL PDF CON FUNCIONES MPDF L O C A L /////////////////////// 
    public function pdf($grupo)
    {
    if (isset($grupo) && is_numeric($grupo) ){
    $this ->load->model('incidenciasdirectora');
    //$this ->load->model('incidencias');
    $datos = $this->incidenciasdirectora->datos_incidencias_pdf($grupo);
    if ($datos != null)
    {
   /* include(APPPATH."third_party/vendor/autoload.php");

    $mpdf = new \Mpdf\Mpdf([
    'margin_top' => '45',
    'mode' => 'utf-8', //Codepage Values OR Codepage Value
    'format' => 'A4-L']);
    //$mpdf->AddPage('L');
    $mpdf->defaultheaderline=0;
    $mpdf->defaultfooterline=0;*/
        	 include(APPPATH."third_party/mpdf600/mpdf.php");

	 $mpdf->defaultheaderline=0;
	 $mpdf->defaultfooterline=0;

	 $mpdf = new mpdf('c','A4-L',0,'',10,10,35,10);//IZQUIERDA, DERECHA, ARRIBA, ABAJO

    $mpdf->SetHeader(' <div class="img">
        <img src="imagenes/loimss.png" width="320">
    </div>
    <div style="display: inline;"> 
    <h1 class="text1"><b>Información sobre la atencion de los niños</b></h1>
    <h1 class="text2"><b>Guarderia No.001</b></h1>
</div>
    ');
    $mpdf->SetFooter('<h1 align="right">
        <font face="arial" size=1> 320-009-293</font>
    </h1>
    <h1 class="Nota">Nota: El lenguaje empleado en el presente documento no busca generar distinción alguna entre hombre y mujeres, por lo que las referencias o alusiones en la redacción a un género representa a ambos sexos.</h1>{PAGENO}');

    $html = '
    <html>

    <head>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            .text1 {
                font-weight: bolder;
                font-size: 15px;
                width: 100%;
                text-align: center;

            }

            .img {
                font-weight: bolder;
                font-size: 12px;
                text-align: left;
            }

            .text2 {
                font-weight: bolder;
                font-size: 12px;
                text-align: left;
            }

            .Nota {
                font-weight: bolder;
                font-size: 9px;
                text-align: left;
            }


            table {
                border-collapse: collapse;
            }

            table,
            th,
            td {
                border: 1px solid black;
                font-weight: bolder;
                font-size: 12px;
            }
        </style>
    </head>

    <body>
        <table class="tabla1" style="text-align: center" WIDTH="100%">
            <thead>
                <tr>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Fecha</th>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Sala</th>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Nombre del niño</th>
                    <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Descripción del logro o incidencia</th>
                    <th COLSPAN=2 bgcolor="#4485BB" style="color:#FFFFFF">Trabajador usuario o Persona autorizada</th>
                </tr>
                <tr bgcolor="#4485BB" style="color:#FFFFFF">
                    <th style="color:#FFFFFF">Nombre</th>
                    <th style="color:#FFFFFF">Firma</th>
                </tr>
            </thead>
            <tbody>';

                foreach ($datos as $key => $value)
                {
                $html.= '
                <tr>
                    <td>'. $value->FechaBase .'</td>
                    <td>'. $value->Nombre_salon .'</td>
                    <td align="left">'. $value->Nombre_ninio .'</td>
                    <td align="left">'. $value->Observacion .'</td>
                    <td align="left">'. $value->Nombre_docente.'<br>'.$value->Nombre_tecnico .'</td>
                    <td></td>
                </tr>';
                }
                $html.='
            </tbody>
        </table>
    </body>

    </html>';


    // echo $html;
    $mpdf->WriteHTML($html);
    $mpdf->Output('uygt.pdf','I');
    }else{
      $html='
      <!-- Versión compilada y comprimida del CSS de Bootstrap -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<br><br><br>
<center>
      <div class="alert alert-danger">AÚN NO HAY INCIDENCIAS REGISTRADAS EL DÍA DE HOY</div>
</center>
      ';
      echo $html;
      echo "<script>
      setTimeout(function(){
            window.close();
        },2000);
      </script>";

    }
    }else{
    echo "<script>
        alert('valor no numerico');
        window.location = '".site_url()."Directora/CRUD_Menupdf'
    </script>";

    }
    }
  //////// ESTRUCTURA DEL PDF CON FUNCIONES MPDF PARA EL S E R V I D O R /////////////////////// 
   /* public function pdf($grupo)
	 {
	 if (isset($grupo) && is_numeric($grupo) ){
	 $this ->load->model('incidenciasTecnico');
	 //$this ->load->model('incidencias');
	 $datos = $this->incidenciasTecnico->datos_incidencias_pdf($grupo);
	 include(APPPATH."third_party/mpdf600/mpdf.php");

	 $mpdf->defaultheaderline=0;
	 $mpdf->defaultfooterline=0;

	 $mpdf = new mpdf('c','A4-L',0,'',10,10,35,10);//IZQUIERDA, DERECHA, ARRIBA, ABAJO
	 $mpdf->SetHeader(' <div class="img">
	     <img src="imagenes/loimss.png" width="320">
	 </div>
	 <h1 class="text1"><b>Información sobre la atencion de los niños</b></h1>
	 <h1 class="text2"><b>Guarderia No.001</b></h1>');
	 $mpdf->SetFooter('<h1 align="right">
	     <font face="arial" size=1> 320-009-293</font>
	 </h1>
	 <h1 class="Nota">Nota: El lenguaje empleado en el presente documento no busca generar distinción alguna entre hombre y mujeres, por lo que las referencias o alusiones en la redacción a un género representa a ambos sexos.</h1>{PAGENO}');

	 $html = '
	 <html>

	 <head>
	     <style>
	         body {
	             font-family: Arial, Helvetica, sans-serif;
	         }

	         .text1 {
	             font-weight: bolder;
	             font-size: 15px;
	             width: 100%;
	             text-align: center;

	         }

	         .img {
	             font-weight: bolder;
	             font-size: 12px;
	             text-align: left;
	         }

	         .text2 {
	             font-weight: bolder;
	             font-size: 12px;
	             text-align: left;
	         }

	         .Nota {
	             font-weight: bolder;
	             font-size: 9px;
	             text-align: left;
	         }


	         table {
	             border-collapse: collapse;
	         }

	         table,
	         th,
	         td {
	             border: 1px solid black;
	             font-weight: bolder;
	             font-size: 12px;
	         }
	     </style>
	 </head>
	 <body>
	     <table class="tabla1" style="text-align: center" WIDTH="100%">
	         <thead>
	             <tr>
	                 <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Fecha</th>
	                 <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Sala</th>
	                 <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Nombre del niño</th>
	                 <th ROWSPAN="2" bgcolor="#4485BB" style="color:#FFFFFF">Descripción del logro o incidencia</th>
	                 <th COLSPAN=2 bgcolor="#4485BB" style="color:#FFFFFF">Trabajador usuario o Persona autorizada</th>
	             </tr>
	             <tr bgcolor="#4485BB" style="color:#FFFFFF">
	                 <th style="color:#FFFFFF">Nombre</th>
	                 <th style="color:#FFFFFF">Firma</th>
	             </tr>
	         </thead>
	         <tbody>';

	             foreach ($datos as $key => $value)
	             {
	             $html.= '
	             <tr>
	                 <td>'. $value->FechaBase .'</td>
	                 <td>'. $value->Nombre_salon .'</td>
	                 <td align="left">'. $value->Nombre_ninio .'</td>
	                 <td align="left">'. $value->Observacion .'</td>
	                 <td align="left">'. $value->Nombre_docente.'<br>'.$value->Nombre_tecnico .'</td>
	                 <td></td>
	             </tr>';
	             }
	             $html.='
	         </tbody>
	     </table>
	 </body>
	 </html>';
	 // echo $html;
	 $mpdf->WriteHTML($html);
	 $mpdf->Output('uygt.pdf','I');
	 }else{
	 echo "<script>
	     alert('valor no numerico');
	     window.location = '".site_url()."Tecnico/CRUD_Menupdf'
	 </script>";

	 }
	 }*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
		public function Control()
	{
		$this ->load->model('incidenciasdirectora');
		$data['salon'] = $this->incidenciasdirectora->salon();
		//$data['datos'] = $this->incidenciasadministrador->datos_control();
		$data['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE CONTROL DE INCIDENCIAS";
		$this->load->view('Estructura/Encabezado', $data);
		$this->load->view('Directora/controldirectora', $data);
		//$this->load->view('Estructura/PiePagina');
		//var_dump($data['datos']);
	}

	public function nombres_control()
	{
		$post = $this->input->post();
		$this->load->model('incidenciasdirectora');
		$nomb = $this->incidenciasdirectora->datos_control($post['FK_salon'], $post['fecha1'], $post['fecha2']);
		echo json_encode($nomb);
	}

	public function CRUD_Citas()
	{
		$this ->load->model('incidenciasdirectora');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasdirectora->datos_citas();
		$data['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE CITAS";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Directora/TablaCitasDirectora', $data);
		//$this->load->view('Estructura/PiePagina');
	}

	public function CRUD_Citas_atendidas()
	{
		$this ->load->model('incidenciasdirectora');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasdirectora->datos_citas_atendidas();
		$data['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE CITAS ATENDIDAS";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Directora/TablaCitasDirectora', $data);
		//$this->load->view('Estructura/PiePagina');
	}

	public function CRUD_Citas_pendientes()
	{
		$this ->load->model('incidenciasdirectora');
	    //$this ->load->model('incidencias');
		$data['datos'] = $this->incidenciasdirectora->datos_citas_pendientes();
		$data['titulo'] = "Directora";
		$data['seccion'] = "SECCIÓN DE CITAS PENDIENTES";
		$this->load->view('Estructura/Encabezado', $data);
		$this ->load->view('Directora/TablaCitasDirectora', $data);
		//$this->load->view('Estructura/PiePagina');
	}

	public function guardarincidencia()
	{
		$data = $this->input->post();
		if (!empty($data['cita']))
		{
			$arrayName = array(
				'FechaBase' => date('Y-m-d H:i:s'),
				'Observacion' => $data['obsr'],
				'EstadoIncidencia' => 'PENDIENTE',
				'EstadoRevisadoIncidencia' => 'PENDIENTE',
				'FK_ninio' => $data['nombrenino'],
				'FK_docente' => $data['Iddocente'],
				'FK_incidencias' => $data['incidencia'],
				'Cita' => $data['cita'],
			);
		} else {
			$arrayName = array(
				'FechaBase' => date('Y-m-d H:i:s'),
				'Observacion' => $data['obsr'],
				'EstadoIncidencia' => 'PENDIENTE',
				'FK_ninio' => $data['nombrenino'],
				'FK_docente' => $data['Iddocente'],
				'FK_incidencias' => $data['incidencia'],
				'Cita' => 'SIN CITA',
			);
		}
		$this->db->insert('incidencias', $arrayName);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO AGREGADO EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Directora/CRUD_Incidencias'
		</script>";
	} 

	public function eliminarincidencia()
	{
		$g = $this->input->post();
		$el = $this->db->delete('incidencias', array('Id_incidencia' => $g['id']));
		echo json_encode($el);
	} 

	public function modificarincidencia()
	{
		$post = $this->input->post();
		$this->load->model('incidenciasdirectora');
		$fila = $this->incidenciasdirectora->modificar($post['idinc']);
		echo json_encode($fila);
	}

	public function enviarincidencia()
	{
		$post = $this->input->post();
		$this->load->model('incidenciasdirectora');
		$fila = $this->incidenciasdirectora->enviarincidencia($post['idinc']);
		echo json_encode($fila);
	}
	public function revisarincidencia()
	{
		$post = $this->input->post();
		$this->load->model('incidenciasdirectora');
		$fila = $this->incidenciasdirectora->revisarincidencia($post['idinc']);
		echo json_encode($fila);
	}
// funcion del boton  para moficar incidencia
	public function modifinc()
	{
		$data = $this->input->post();
		$arrayMod = array(
		'Observacion' => $data['obsrD'],
		'Cita' => $data['citaD'],
		'FK_ninio' => $data['nombreninosD'],
		'FK_incidencias' => $data['incidenciaD']
            
		);
        
		$this->db->where('Id_incidencia', $data['id']);
		$this->db->update('incidencias', $arrayMod);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO EXITOSAMENTE');
		echo "<script>
		window.location= '".site_url()."Directora/CRUD_Incidencias'
		</script>";
	}
//funcion del boton de agregar una cita ////////////////////////////////////////
	public function Datoscita()
	{
		$post = $this->input->post();
		$this->load->model('incidencias');
		$fila = $this->incidencias->citaAlta($post['idinc']);
		echo json_encode($fila);
	} 

	public function Altacita()
	{
		$data = $this->input->post();
		$this->db->delete('citas',array('FK_incidencia' => $data['idd']));
		$arrayName = array(
			'FechaBaseCita' => $data['fechabase'],
			'FK_incidencia' => $data['idd'],
			'EstadoCita' => 'PENDIENTE'
		);
		$this->db->insert('citas', $arrayName);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'CITA AGREGADA EXITOSAMENTE');		
		echo "<script>
		window.location= '".site_url()."Directora/CRUD_Incidencias'
		</script>";
	}
    //catalogo de citas //////////////////////////////////////////////////////////
	public function modificarcitas()
	{
		$post =	$this->input->post();
		$this->load->model('citas');
		$filacita = $this->citas->modificar($post['idcitas']);
		echo json_encode($filacita);
        //var_dump($filacita);
	}

	public function modifcita()
	{
		$data = $this->input->post();
		$arraycita = array(
			'FechaAtencionCita' => date('Y-m-d H:i:s'),
			'Descripcion' => $data['desc'],
			'EstadoCita'=> $data['estadocita']
		);
		$this->db->where('Id_citas', $data['idcita']);
		$this->db->update('citas', $arraycita);
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO CORRECTAMENTE!');
		echo "<script>
		window.location = '".site_url()."Directora/CRUD_Citas'
		</script>";
	}

	public function eliminarcitas()
	{
		$delet = $this->input->post();
		$d = $this->db->delete('citas', array('Id_citas' => $delet['idcitas']));
		echo json_encode($d);
	}
    
    
    /////////////////////////////////////// EDITAR Y ENVIAR CORREO DE LA INCIDENCIA/////////////////////////////////////////////
  	
	
	/*    public function enviarincid()
    {
      $data = $this->input->post();
      $arrayMod = array(
        'Observacion' => $data['obsrE'],
        'FK_tecnico'  => $data['Idtecnico'],
        'EstadoIncidencia' => $data['EstadoInc'],
        'FechaAtencion' => date('Y-m-d H:i:s')
      );
      $this->db->where('Id_incidencia',$data['idE']);
      $this->db->update('incidencias',$arrayMod);

      $asunto = "NOTIFICACIÓN DE INCIDENCIA EN LA GUARDERIA #1 DEL IMSS";
      $mensaje = "C. <u><b> ".$data['nombretutor']."</b></u>  DE ACUERDO A LAS NORMAS DE LA INSTITUCION, MEDIANTE ESTE CORREO SE LE INFORMA QUE SU HIJO <u><b> ".$data['nombreninio']." </b></u> TUVO LA SIGUIENTE INCIDENCIA: <br><br>
      TIPO DE INCIDENCIA: ".$data['nombretipo']." <br><br>
      INCIDENCIA: ".$data['nombreinc']." <br><br>
      OBSERVACION: ".$data['obsrE']." ";
      $destino = array($data['correo']);
      $respuestacorreo = correo($destino, $asunto, $mensaje);

      $datas = json_decode($respuestacorreo);
      switch ($datas[0]) {
        case 1:
        $arrayEstatus = array(
          'EntregaIncidencia' => "ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['idE']);
        $this->db->update('incidencias', $arrayEstatus);
        $this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO Y CORREO ENVIADO CORRECTAMENTE');
        redirect('Tecnico/CRUD_Incidencias');
        break;

        default:
        $arrayEstatus = array(
          'EntregaIncidencia' => "NO ENVIADO"
        );
        $this->db->where('Id_incidencia', $data['idE']);
        $this->db->update('incidencias', $arrayEstatus);
        $this->session->set_flashdata('alerta', 'ERROR AL MODIFICAR REGISTRO Y CORREO NO ENVIADO');
        redirect('Tecnico/CRUD_Incidencias');
        break;
      }
    }*/

	public function enviarincid()
	{
		$data = $this->input->post();
		
       // var_dump($data); die();
		$this->load->library('mail/PHPMailer');
		$this->load->library('mail/SMTP');
		$this->load->library('mail/Exception');
	    // Crear una nueva  instancia de PHPMailer habilitando el tratamiento de excepciones
		$mail = new PHPMailer(true);
		try {
			$remitente = "guarderia001aca@gmail.com";
			$mail->isSMTP();
			/*$mail->SMTPDebug = 1;*/
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "ssl";
			$mail->Host = "smtp.gmail.com";
			$mail->Port = 465; 
			$mail->Username = $remitente;
			$mail->Password = '#!98&.$sw@D';
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
			$mail->setFrom($remitente, "NOTIFICACIÓN DE INCIDENCIA");
			$mail->addAddress("monzalvo971@gmail.com");
			//Enviar una copia del correo	
			//$mail->addCC('eliza.ld90s@gmail.com');
			//Enviar una copia oculta del correo
			//$mail->addBCC('copia_oculta@outlook.com');
			$mail->isHTML(true); 
			$mail->CharSet = "UTF-8";
			$mail->Subject = $data['asunto'];
			$mail->Body    = $data['cuerpocorreo']."<br><br>
			TIPO DE INCIDENCIA: ".$data['nombretipo']." <br><br>
			INCIDENCIA: ".$data['nombreinc']." <br><br>
			OBSERVACION: ".$data['obsrE']." ";
            //$mail->send();
			if ($mail->Send()) {
				$arrayEstatus = array(
					'EntregaIncidencia' => "ENVIADO"
				);
				$this->db->where('Id_incidencia', $data['idE']);
				$this->db->update('incidencias', $arrayEstatus);
			} else {
                //sE DEBE ACTUALIZAR EL REGISTRO PERO SE NECESITA EL ID DE LA INCIDENCIA PARA PODER MODIFICAR.
				$arrayEstatus = array(
					'EntregaIncidencia' => "NO ENVIADO"
				);
				$this->db->where('Id_incidencia',$data['idE']);
				$this->db->update('incidencias',$arrayEstatus);
			}
		} catch (Exception $e) {
            //echo "3";
            // return $mail->ErrorInfo;
		}
		//var_dump($mail); die();
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO MODIFICADO EXITOSAMENTE Y CORREO ENVIADO');
		echo "<script>
		               window.location= '".site_url()."Directora/CRUD_Incidencias'
		     </script>";
	}

public function revisar()
	{
		$data = $this->input->post();
		$arrayModIF = array(
			'Observacion' => $data['obsrER'],
			'FK_tecnico'  => $data['IdtecnicoR'],
			'EstadoIncidencia' => $data['EstadoIncR'],
			'EstadoRevisadoIncidencia' => $data['EstadoIncR'],
			'FechaAtencion' => date('Y-m-d H:i:s')
		);
       // var_dump($arrayModIF); die();
		$this->db->where('Id_incidencia', $data['idER']);
		$this->db->update('incidencias', $arrayModIF);

		//var_dump($arraymod); die();
		$this->session->set_flashdata('color', 'alert alert-success');
		$this->session->set_flashdata('alerta', 'REGISTRO REVISADO EXITOSAMENTE');
		echo "<script>
		               window.location= '".site_url()."Directora/CRUD_Incidencias'
		     </script>";
	}

    public function datofirmapadre(){
		$post = $this->input->post();
		$this->load->model('incidencias');
		$fila = $this->incidencias->datosfirma($post['idinc']);
		echo json_encode($fila);
	} 

	public function validartutor()
	{ 

		$post = $this->input->post();
        //var_dump($post); die();
		if(isset($post) && !empty($post)){
			$idInc = $post['nom'];
			$idtutor = $post['idtutor'];
            $idautorizado = $post['autorizado'];
			$contraseña = $post['pass'];
			$this->load->model('user');
			$fila = $this->user->getTutor($idtutor, $contraseña, $idautorizado);
            //var_dump($fila); die();
			if ($fila != null) {

				$arrayfirma = array(
					'FirmaTutor' => 'ENTERADO'
				);
				$this->db->where('Id_incidencia', $idInc);
				$this->db->update('incidencias', $arrayfirma);
				$this->session->set_flashdata('color', 'alert alert-success');
				$this->session->set_flashdata('alerta', 'Usuario y contraseñas correctas');
				echo "<script>
				window.location= '".site_url()."Directora/CRUD_Incidencias'
				</script>"; 

			} else {
				$this->session->set_flashdata('color', 'alert alert-danger');
				$this->session->set_flashdata('alerta', 'Usuario y/o contraseñas incorrectas');
				echo "<script>
				window.location= '".site_url()."Directora/CRUD_Incidencias'
				</script>";
			}
		}else{
			redirect('Filtro/CRUD_Incidencias');
		}
	}

    public function pdffiltro($grupo)
	{
		 
		if (isset($grupo) && is_numeric($grupo)  ){
			$this ->load->model('incidenciasfiltro');
	    //$this ->load->model('incidencias');
			$datos = $this->incidenciasfiltro->datos_incidencias_pdf($grupo);
			include(APPPATH."third_party/vendor/autoload.php");
            if ($datos != null)
            {
			$mpdf = new \Mpdf\Mpdf([
			'margin_top' => '35',
			'mode' => 'utf-8', //Codepage Values OR Codepage Value
			'format' => 'A4-L']);
        //$mpdf->AddPage('L');
			$mpdf->defaultheaderline=0;
			$mpdf->defaultfooterline=0;

			$mpdf->SetHeader('      <div class="img">
				<img src="imagenes/loimss.png" width="320">
				</div>
				<h1 class="text1"><b>Información al usuario de la atención a los niños(as)</b></h1>
				<h1 class="text2" ><b>Guardería No.001</b></h1>');
			$mpdf->SetFooter('<h1 align="right" ><font face="arial" size=1>DPES/CG/009/205</font></h1>
				{PAGENO}');
			$html = '<html>
			<head>
			<style>
			body{
				font-family:Arial, Helvetica, sans-serif;
			}
			.text1{
				font-weight:bolder;
				font-size:15px;
				width:100%;
				text-align:center;

			}
			.img{
				font-weight:bolder;
				font-size:12px;
				text-align:left;
			}
			.text2{
				font-weight:bolder;
				font-size:12px;
				text-align:left;
			}
			.Nota{
				font-weight:bolder;
				font-size:9px;
				text-align:left;
			}


			table {
				border-collapse: collapse;
			}

			table, th, td {
				border: 1px solid black;
				font-weight:bolder;
				font-size:12px;
			}
			</style>

			<body>
			<table class="tabla1" style="text-align: center" WIDTH="100%">
			<thead>
			<tr>
			<th width="8%" ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Fecha</th>
			<th width="8%" ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Sala</th>
			<th width="20%"  ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Nombre del niño(a)</th>
			<th width="24%" ROWSPAN="2" bgcolor="#D9D9D9" style="color:#000000">Descripción de logro o incidencia</th>
			<th width="30%" COLSPAN=2 bgcolor="#D9D9D9" style="color:#000000">Asegurado usuario o Persona autorizada</th>
			</tr>
			<tr bgcolor="#D9D9D9" style="color:#000000">
			<th width="20%" style="color:#000000" >Nombre</th>
			<th width="10%" style="color:#000000">Firma</th>  
			</tr>
			</thead>
			<tbody>';

			foreach ($datos as $key => $value)
			{
				$html.= '
				<tr>
				<td>'. $value->FechaBase .'</td>
				<td>'. $value->Nombre_salon .'</td>
				<td align="left">'. $value->Nombre_ninio .'</td>
				<td align="left" >'. $value->Observacion .'</td>
				<td align="left">'. $value->Nombre_tutor.'</td>
				<td></td>
				</tr>';
			}
			$html.='
			</tbody>
			</table>
			</body>
			</html>';

//  echo $html;          
			$mpdf->WriteHTML($html);
			$mpdf->Output('Anexo3 Información al Usuario.pdf','I');
			}else{
			$html='
			<!-- Versión compilada y comprimida del CSS de Bootstrap -->
			<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
			<br><br><br>
			<center>
			    <div class="alert alert-danger">AÚN NO HAY INCIDENCIAS REGISTRADAS</div>
			</center>
			';
      echo $html;
      echo "<script>
      setTimeout(function(){
            window.close();
        },2000);
      </script>";

    }
		}else{
			echo "<script>
			alert('valor no numerico');
			window.location= '".site_url()."Directora/CRUD_Menupdf'
			</script>";

		}
	}

  public function pdftecnico($grupo)
  {
    if (isset($grupo) && is_numeric($grupo) ){
      $this ->load->model('incidenciasTecnico');
    //$this ->load->model('incidencias');
      $datos = $this->incidenciasTecnico->datos_incidencias_pdf($grupo);
       
      if ($datos != null)
      {
      include(APPPATH."third_party/vendor/autoload.php");

      $mpdf = new \Mpdf\Mpdf([
      'margin_top' => '35',
      'mode' => 'utf-8', //Codepage Values OR Codepage Value
      'format' => 'A4-L']);
      //$mpdf->AddPage('L');
      $mpdf->defaultheaderline=0;
      $mpdf->defaultfooterline=0;

      $mpdf->SetHeader(' <div class="img">
        <img src="imagenes/loimss.png" width="320">
        </div>
        <h1 class="text1"><b>Reporte de logros e incidencias diarias por sala de atención</b></h1>
       
        <table width="100%" border="0" width="100%" >
            <thead>
                <tr>
                    <th width="78%" style="text-align: left;" ><b>Guarderia No.001</b></th>
                    <th width="22%" style="text-align: left;" ><b>Sala: '.$datos[0]->Nombre_salon.'</b></th>
                </tr>
            </thead>
        </table>
        
     ');
      $mpdf->SetFooter('<h1 align="right">
        <font face="arial" size=1>DPES/CG/014/006</font>
        </h1>
        <h1 class="Nota">NOTA: Este formato será de uso exclusivo para el reporte de logros e incidencias de los niños(as).</h1>{PAGENO}');

      $html = '
      <html>

      <head>
      <style>
      body {
        font-family: Arial, Helvetica, sans-serif;
      }

      .text1 {
        font-weight: bolder;
        font-size: 15px;
        width: 100%;
        text-align: center;

      }

      .img {
        font-weight: bolder;
        font-size: 12px;
        text-align: left;
      }

      .text2 {
        font-weight: bolder;
        font-size: 12px;
        text-align: left;
      }

      .Nota {
        font-weight: bolder;
        font-size: 9px;
        text-align: left;
      }


      table {
        border-collapse: collapse;
      }

      table,
      th,
      td {
        
        font-weight: bolder;
        font-size: 12px;
      }
      </style>
      </head>

      <body>
      
      <table class="tabla1" style="text-align: center" width="100%" border="1">
      <thead>
      <tr>
      <th width="7%" bgcolor="#F2F2F2" style="color:#000000">Fecha</th>
      <th width="7%" bgcolor="#F2F2F2" style="color:#000000">Hora</th>
      <th width="23%" bgcolor="#F2F2F2" style="color:#000000">Nombre del niño(a)</th>
      <th width="25%" bgcolor="#F2F2F2" style="color:#000000">Descripción de logro o incidencia</th>
      <th width="19%" bgcolor="#F2F2F2" style="color:#000000">Nombre y firma de la persona que reporta</th>
      <th width="19%" bgcolor="#F2F2F2" style="color:#000000">Nombre y firma del Jefe de servicio</th>
      </tr>

     
      </thead>
      <tbody>';

      foreach ($datos as $key => $value)
      {
          $arraymod = explode(' ',$value->FechaBase);
        $html.= '
        <tr>
        <td>'. $arraymod[0] .'</td>
        <td>'. $arraymod[1] .'</td>
        <td align="left">'. $value->Nombre_ninio .'</td>
        <td align="left">'. $value->Observacion .'</td>
        <td align="left">'. $value->Nombre_docente.'</td>
        <td align="left">'.$value->Nombre_tecnico .'</td>
        </tr>';
      }
      $html.='
      </tbody>
      </table>
      </body>
      
      

      </html>';



    // echo $html;
      $mpdf->WriteHTML($html);
      $mpdf->Output('uygt.pdf','I');
    }else{
      $html='
      <!-- Versión compilada y comprimida del CSS de Bootstrap -->
      <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
      <br><br><br>
      <center>
          <div class="alert alert-danger">AÚN NO HAY INCIDENCIAS REGISTRADAS</div>
      </center>
      ';
      echo $html;
      echo "<script>
      setTimeout(function(){
            window.close();
        },2000);
      </script>";

    }
  }else{      echo "<script>
      alert('valor no numerico');
      window.location = '".site_url()."Tecnico/CRUD_Menupdf'
      </script>";}
  }
	
}