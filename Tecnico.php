
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tecnico extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$this->principal((object)array('output' => '' ,'data' => '' , 'js_files' => array() , 'css_files' => array()));
		$this ->load->view('Tecnico/inicio');
	}

	public function principal($output = null,$data=null)
	{ 
		if($data['nombre']==null){$data['nombre']="";}
		else{}
			$data['titulo'] = "Tecnico";
		$this->load->view('Estructura/Encabezado',$data);
		$this ->load->view('Tecnico/PrincipalTecn',$output);
	//$this->load->view('Estructura/PiePagina');
	}


		public function CRUD_Incidencias()
		{
			$this ->load->model('incidenciasTecnico');
	//$this ->load->model('incidencias');

			$data['datos'] = $this->incidenciasTecnico->datos_incidencias();
			$data['salon'] = $this->incidenciasTecnico->salon();
			$data['tipos'] = $this->incidenciasTecnico->tipo_salon();
			$data2['titulo'] = "Tecnico";
			$this->load->view('Estructura/Encabezado',$data2);
			$this ->load->view('Tecnico/TablaIncidenciasTecnico',$data);
	//$this->load->view('Estructura/PiePagina');



		}
    
		public function CRUD_Incidencias_Atendidas()
		{
			$this->load->model('incidenciasTecnico');
			$data['datos'] = $this->incidenciasTecnico->CRUD_Incidencias_Revisadas();
			$data2['titulo'] = "Tecnico";
			$this->load->view('Estructura/Encabezado',$data2);
			$this ->load->view('Tecnico/TablaIncidenciasTecnico',$data);

		}

		public function CRUD_Incidencias_Pendientes()
		{
			$this->load->model('incidenciasTecnico');
			$data['datos'] = $this->incidenciasTecnico->CRUD_Incidencias_Pendientes();
			$data2['titulo'] = "Tecnico";
			$this->load->view('Estructura/Encabezado',$data2);
			$this ->load->view('Tecnico/TablaIncidenciasTecnico',$data);

		}

		public function CRUD_Citas()
		{
			$this ->load->model('citas');

			$data['datos'] = $this->citas->datos_citas();
			$data2['titulo'] = "Tecnico";
			$this->load->view('Estructura/Encabezado',$data2);
			$this ->load->view('Tecnico/Tablacitas',$data);
        //var_dump($data);

	}
    public function CRUD_Citas_Atendidas()
	{
		$this ->load->model('citas');

   $data['datos'] = $this->citas->CRUD_Citas_Atendidas();
		$data2['titulo'] = "Tecnico";
	$this->load->view('Estructura/Encabezado',$data2);
	$this ->load->view('Tecnico/Tablacitas',$data);
        //var_dump($data);
	}
    public function CRUD_Citas_Pendientes()
	{
		$this ->load->model('citas');

   $data['datos'] = $this->citas->CRUD_Citas_Pendientes();
		$data2['titulo'] = "Tecnico";
	$this->load->view('Estructura/Encabezado',$data2);
	$this ->load->view('Tecnico/Tablacitas',$data);
        //var_dump($data);
	}
    
    
    public function modificarcitas()
    {
    $post =	$this->input->post();
    $this->load->model('citas');
        $filacita = $this->citas->modificar($post['idcitas']);
        echo json_encode($filacita);
        //var_dump($filacita);
    }
    
    
    public function modifcita(){
        
        $data = $this->input->post();
        $arraycita = array(
            'FechaAtencion' => date('Y-m-d H:i:s'),
        'Descripcion' => $data['desc'],
            'EstadoCita'=> $data['estadocita']
        );
        
        $this->db->where('Id_citas',$data['idcita']);
        $this->db->update('citas',$arraycita);
        
        
        echo "<script>
        alert('REGISTRO MODIFICADO CORRECTAMENTE');
        window.location = 'http://localhost/IncidenciasGuarderia/Tecnico/CRUD_Citas'
        </script>";
    }
    public function eliminarcitas(){
        $delet = $this->input->post();
        $d = $this->db->delete('citas',array('Id_citas'=>$delet['iditas']));
        echo json_encode($d);
    }

    
    public function guardarincidencia(){
$data = $this->input->post();
$arrayName = array(

	'FechaBase' => date('Y-m-d H:i:s'),
	'Observacion' => $data['observaciones'],
	'EstadoIncidencia' => 'PENDIENTE',
	'FK_ninio' => $data['nombrenino'],
	'FK_docente' => $data['Iddocente'],
	'FK_incidencias' => $data['incidencia'],
	'Cita' => $data['cita'],
	 );
$this->db->insert('incidencias',$arrayName);

 echo "<script>
                alert('REGISTRO AGREGADO EXITOSAMENTE');
                window.location= 'http://localhost/IncidenciasGuarderia/Tecnico/CRUD_Incidencias'
    </script>";

} 

public function eliminarincidencia(){
$g = $this->input->post();
$el = $this->db->delete('incidencias',array('Id_incidencia' => $g['idel']));

echo json_encode($el);
 
} 

public function modificarincidencia(){


$post = $this->input->post();
$this->load->model('incidencias');
$fila = $this->incidencias->modificar($post['idinc']);
echo json_encode($fila);

} 	

	public function modifinc(){
		$data = $this->input->post();
		$arrayMod = array(
			'Observacion' => $data['obsr'],
			'FK_tecnico'  => $data['Idtecnico'],
			'EstadoIncidencia' => $data['EstadoInc'],
			'FechaAtencion' => date('Y-m-d H:i:s')
		);

		$this->db->where('Id_incidencia',$data['id']);
		$this->db->update('incidencias',$arrayMod);

		echo "<script>
		alert('REGISTRO MODIFICADO EXITOSAMENTE');
		window.location= 'http://localhost/IncidenciasGuarderia/Tecnico/CRUD_Incidencias'
		</script>";

	}


	}